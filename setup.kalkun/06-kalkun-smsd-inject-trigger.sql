-- Dans le cas où on utilise aussi gammu-smsd-inject, Kalkun n'affichera pas le message envoyé.
-- Cependant, si on créé un trigger où à l'ajout d'un message dans outbox
-- on créé aussi l'entrée correspondante dans user_oubox, alors il sera affiché
-- L'affichage se fait soit pour le username "smsd-inject", ou à défaut pour le username "kalkun".
-- Il n'est pas nécessaire de gérer cela pour les messages dans sentitems.
-- Il faut bien s'assurer que ce trigger n'est pas déclenché si c'est un message envoyé via Kalkun.
-- TODO, gérer les SMS entrants.

CREATE OR REPLACE FUNCTION fn_outbox_item_inserted()
  RETURNS TRIGGER AS 
$$
DECLARE
    var_id_user integer := 1;
BEGIN
    IF (SELECT id_outbox FROM user_outbox WHERE id_outbox = NEW."ID") IS NULL THEN
    IF NEW."CreatorID" LIKE 'Gammu%' THEN
        SELECT (id_user) INTO var_id_user FROM "user" WHERE username = 'smsd-inject';
        IF var_id_user IS NULL THEN
            INSERT INTO user_outbox VALUES (NEW."ID",1);
        ELSE
            INSERT INTO user_outbox VALUES (NEW."ID",var_id_user);
        END IF;
    END IF;
    END IF;
    RETURN NEW;
END $$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS "outbox_item_inserted" ON "outbox";
CREATE TRIGGER "outbox_item_inserted" 
    AFTER INSERT 
    ON outbox
    FOR EACH ROW
    EXECUTE PROCEDURE fn_outbox_item_inserted();


-- L'équivalent de ci-dessus pour la table "inbox"

-- CREATE OR REPLACE FUNCTION fn_inbox_item_inserted()
--   RETURNS TRIGGER AS
-- $$
-- DECLARE
--     var_id_user integer := 1;
-- BEGIN
--     IF (SELECT id_inbox FROM user_inbox WHERE id_inbox = NEW."ID") IS NULL THEN
--         SELECT (id_user) INTO var_id_user FROM "user" WHERE username = 'smsd-inject';
--         IF var_id_user IS NULL THEN
--             INSERT INTO user_inbox VALUES (NEW."ID",1);
--         ELSE
--             INSERT INTO user_inbox VALUES (NEW."ID",var_id_user);
--         END IF;
--     END IF;
--     RETURN NEW;
-- END $$ LANGUAGE 'plpgsql';
--
-- DROP TRIGGER IF EXISTS "inbox_item_inserted" ON "inbox";
-- CREATE TRIGGER "inbox_item_inserted"
--     AFTER INSERT
--     ON inbox
--     FOR EACH ROW
--     EXECUTE PROCEDURE fn_inbox_item_inserted();
