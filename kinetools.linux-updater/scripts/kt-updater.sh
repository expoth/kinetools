#!/bin/bash
# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

FTP_INCOMING_DIR=/var/ftp/incoming/
# Les scripts à lancer sont dans le répertoire où est ce fichier
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Mettre à 0 si on a besoin de débugguer
DELETE_INCOMING_FILES=1
DELETE_TMP_FILES=1

function delete_file() {
    if [ -f "$1" ]; then
        rm "$1"
    fi
}

inotifywait -m -e close_write "$FTP_INCOMING_DIR" |
    while read -r path action file; do
        case "$file" in
            kinetools-linux_*_all.deb)

                mkdir -p /tmp/incoming_monitor
                echo "[$(date "+%F T %T %z")] Fichier $file détecté. Mise à jour du paquet..."

                cp -a "$path/$file" /tmp/incoming_monitor &&

                # Suppression des fichiers temporaires dans FTP_INCOMING_DIR
                if [ $DELETE_INCOMING_FILES = 1 ]; then
                    delete_file "$FTP_INCOMING_DIR/$file"
                fi

                # Installe le paquet debian et conserve les conffile précédents
                dpkg --force-confold -i "/tmp/incoming_monitor/$file"

                # Suppression des fichiers temporaires dans /tmp/incoming_monitor
                if [ $DELETE_TMP_FILES = 1 ]; then
                    delete_file "/tmp/incoming_monitor/$file"
                fi

                echo "[$(date "+%F T %T %z")] Mise à jour du paquet... TERMINÉE"
                ;;
        esac
    done
