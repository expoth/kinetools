
CREATE USER "smsd" WITH password 'smsd' NOCREATEDB NOCREATEROLE;
CREATE DATABASE "smsd" WITH OWNER = "smsd" ENCODING = 'UTF8';
-- \connect "smsd" "smsd"
COMMENT ON DATABASE "smsd" IS 'Gammu SMSD Database';
