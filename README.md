# KineTools Suite

KineTools Suite est consitué des principaux composants suivants :
* __KineTools Windows__: Système de requête et récupération d'informations de logiciels métier kiné tels que Kine4000, Kinemax/logicmax, Véga5. Il transmet ensuite ces données vers l'un des composants ci-dessous.
_Par ex_, extraction de l'agenda, des prochains rendez-vous...
* __KineTools Linux__: Système d'envoi de SMS par une machine virtuelle sous Linux branchée à un téléphone/modem pris en charge par Gammu-smsd. Il réceptionne les informations recues par KineTools Windows, et effectue leur traitement.
* __KineTools Intranet__: Site web Intranet qui permet d'afficher l'agenda extrait par KineTools Windows et de sélectionner les créneaux libres à publier sur l'extranet. Il permet aussi de paramétrer KineTools Linux.
* __KineTools Extranet__: Site web pour permettre la prise de rendez-vous en ligne (et de lister les coordonnées et thérapeutes du cabinet)

La documentation se trouve dans le répertoire `doc`. Elle détaille pour chaque composant :
- les fonctionnalités
- le fonctionnement
- la procédure d'installation
