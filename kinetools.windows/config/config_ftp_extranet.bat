@echo off

if exist "%~d0\%~n0%~x0" (
  REM Si fichier de ce nom � la racine de ce dique, on prend celui-ci, sinon on prend les infos de ce fichier.
  call "%~d0\%~n0%~x0"
) else (
  REM CONFIG FTP EXTRANET
  set FTP_EXTRANET_HOST=ftp.server.com
  set FTP_EXTRANETDATADIR_USERNAME=username
  REM Attention � bien encoder les caract�res sp�ciaux selon : https://winscp.net/eng/docs/session_url
  set FTP_EXTRANETDATADIR_PASSWORD=password
)
