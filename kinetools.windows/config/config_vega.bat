@echo off

REM Choisir entre ODBC ou OLEDB pour les requêtes
set VEGA_DB_ENGINE=ODBC
REM set VEGA_DB_ENGINE=OLEDB

REM Paramétrage pour Vega
set VEGA_ODBC_NAME=Vega
set VEGA_ANA=C:\Vega5\outils\_ssv.wdd
set VEGA_REP=C:\Vega5\Fichiers

REM Paramétrage ODBC pour Vega
set ODBC_DSN_VEGA="DSN=%VEGA_ODBC_NAME%;uid=a;pwd=a"

REM SI ON UTILISE DES SINGLE QUOTE, IL FAUT LES DOUBLER, SINON CA VA POSER PROBLÈME AVEC POWERSHELL

REM Paramétrage OLEDB pour Vega
set OLEDB_CONN_STR_VEGA="Provider=PCSOFT.HFSQL;Data Source=%VEGA_ANA%;Initial Catalog=%VEGA_REP%\;"
