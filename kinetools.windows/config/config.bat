@echo off

REM R�pertoire principal de KineTools

REM set TOOLDIR=%USERPROFILE%\KineTools
SET "TOOLDIR=%~dp0\.."

REM Transforme le chemin relatif en chemin absolu
FOR /F "delims=" %%F IN ("%TOOLDIR%") DO SET "TOOLDIR=%%~fF"

REM Chemins pour KineTools
set SCRIPT_PATH=%TOOLDIR%\scripts
if not exist %SCRIPT_PATH% mkdir %SCRIPT_PATH%
set DATA_PATH=%TOOLDIR%\data
if not exist %DATA_PATH% mkdir %DATA_PATH%
set STDOUT_PATH=%TOOLDIR%\stdout
if not exist %STDOUT_PATH% mkdir %STDOUT_PATH%
set LOG_PATH=%TOOLDIR%\log
if not exist %LOG_PATH% mkdir %LOG_PATH%

REM Localisation des outils externes
set ODBCVIEW_PATH=%TOOLDIR%\10-ODBCView4
set WINSCP_PATH=%TOOLDIR%\11-WinSCP
set UnxUtils_PATH=%TOOLDIR%\12-UnxUtils

REM Use MD5SUM or CertUtil to compute MD5 Hash of transmitted files
set USE_MD5SUM=1

REM D�finit si on doit faire les uploads vers les serveurs FTP ou s'arr�ter juste avant.
set FTP_EXTRANET_DISABLE_UPLOAD=1
set FTP_VM_DISABLE_UPLOAD=1

REM D�finit si on veut faire une pause � la fin de l'execution d'un script principal.
set PAUSE_AT_END_OF_SCRIPT=1

REM Param�tres suppl�mentaires
call %~dp0\config_vm.bat

call %~dp0\config_kinemax.bat
call %~dp0\config_vega.bat
call %~dp0\config_kine4000.bat

call %~dp0\config_ftp_extranet.bat
call %~dp0\config_ftp_vm.bat

REM @echo on
