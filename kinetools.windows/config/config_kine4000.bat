@echo off

REM Choisir entre ODBC ou OLEDB pour les requ�tes
REM set KINE4000_DB_ENGINE=ODBC
set KINE4000_DB_ENGINE=OLEDB

REM Param�trage pour Kine4000
set KINE4000_ODBC_NAME=Kine4000
set KINE4000_BDD_HOST=localhost:4900
set KINE4000_BDD_USER=Admin
set KINE4000_NOM_BASE=Base_X4
set KINE4000_ANA=C:\X4\HF6.wdd
set KINE4000_REP=C:\X4\DONNEE\BDD\%KINE4000_NOM_BASE%

REM Param�trage ODBC pour Kine4000
set ODBC_DSN_KINE4000="DSN=%KINE4000_ODBC_NAME%;uid=a;pwd=a"

REM SI ON UTILISE DES SINGLE QUOTE, IL FAUT LES DOUBLER, SINON CA VA POSER PROBL�ME AVEC POWERSHELL

REM Param�trage OLEDB pour Kine4000 --> Besoin renseigner mot de passe pour certaines tables
REM Version passant par une version Client/Serveur de HFSQL (et pas par acc�s direct aux fichiers)
REM  = cas de Kine4000 v7
REM OLEDB_CONN_STR_Kine4000="Provider=PCSOFT.HFSQL;Data Source=%KINE4000_BDD_HOST%;Initial Catalog=%KINE4000_NOM_BASE%;User ID=%KINE4000_BDD_USER%;Password=;Extended Properties=''Password=*:MyPassword;'';"
REM set OLEDB_CONN_STR_Kine4000="Provider=PCSOFT.HFSQL;Data Source=%KINE4000_BDD_HOST%;Initial Catalog=%KINE4000_NOM_BASE%;User ID=%KINE4000_BDD_USER%;Password=;Extended Properties=''Password=*:myPassword;Language=UTF-8;'';"


REM Param�trage OLEDB pour Kine4000 --> Besoin renseigner mot de passe pour certaines tables
REM Version avec acc�s direct aux fichier (HFSQL Classic), sans passer par une version Client/Serveur de HFSQL
REM  = cas de Kine4000 v8
set OLEDB_CONN_STR_Kine4000="Provider=PCSOFT.HFSQL;Data Source=%KINE4000_ANA%;Initial Catalog=%KINE4000_REP%\;Extended Properties=''Password=*:MyPassword;'';"
