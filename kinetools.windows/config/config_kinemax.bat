@echo off

REM CHANGEZ CES MOTS DE PASSE !!
set PASSWORD_KTV=ktv
set PASSWORD_KT=kt


if exist "F:\Logicmax\*" set LOGICMAX_BASEDIR=F:\Logicmax
if exist "C:\Program Files (x86)\Logicmax\*" set LOGICMAX_BASEDIR=C:\Program Files (x86)\Logicmax
if exist "C:\Program Files\Logicmax\*" set LOGICMAX_BASEDIR=C:\Program Files\Logicmax

REM Paramétrage PostgreSQL pour Kinemax
set PGSQLDIR=%LOGICMAX_BASEDIR%\postgresql-9.3

set PGSQL_KINEMAX_SERVICE_NAME=LogicmaxPostgreSQL-9.3

set DB_HOST=localhost
set DB_USERNAME=ktv
REM PGPASSFILE EnvVar utilisee par PgSQL
set PGPASSFILE=%~dp0\pgpass.conf
set DB_PORT=9154
set DB_NAME=CABINET
