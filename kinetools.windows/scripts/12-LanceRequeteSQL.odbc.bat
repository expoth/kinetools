@REM 
@REM Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant � extraire des donn�es
@REM de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
@REM certaines t�ches de gestion. 
@REM 
@REM Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilit� au code source et des droits de copie,
@REM de modification et de redistribution accord�s par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
@REM seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les conc�dants successifs.
@REM 
@REM A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
@REM associ�s au chargement,  � l'utilisation,  � la modification et/ou au
@REM d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
@REM donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
@REM manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
@REM avertis poss�dant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
@REM logiciel � leurs besoins dans des conditions permettant d'assurer la
@REM s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
@REM � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
@REM 
@REM Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
@REM termes.
@REM 
@echo off

REM Script pour extraire les donn�es de Kine4000 ou Vega (selon le param�tre %2) et en faire un csv

call "%~dp0\..\config\config.bat"

set FICHIER_A_ENVOYER=%1
set PROGRAMME_METIER=%2

cd /D "%DATA_PATH%"

REM D'abord supprime les fichiers existants
del /q %FICHIER_A_ENVOYER% > nul 2> nul
del /q %FICHIER_A_ENVOYER%.certutil.md5 > nul 2> nul

FOR /F "tokens=* USEBACKQ" %%F IN (`call echo %%ODBC_DSN_%PROGRAMME_METIER%%%`) DO (SET ODBC_DSN=%%F)

REM Fait la requ�te � Kine4000 ou Vega via ODBC avec ODBCView4
%ODBCVIEW_PATH%\odbcview.exe -s -d \t ^
 "%ODBC_DSN%" "%SCRIPT_PATH%\490-%FICHIER_A_ENVOYER%.%PROGRAMME_METIER%.sql" "%FICHIER_A_ENVOYER%" ^
 > %STDOUT_PATH%\%FICHIER_A_ENVOYER%.%PROGRAMME_METIER%.odbc.stdout.txt ^
 2> %STDOUT_PATH%\%FICHIER_A_ENVOYER%.%PROGRAMME_METIER%.odbc.stderr.txt

REM Convertit en UTF8
ECHO Convertit le fichier en UTF-8
call %SCRIPT_PATH%\495-convertToUtf8.bat %FICHIER_A_ENVOYER%


REM Post traitement des fichiers (conversion de champs en base36)

IF /I "%FICHIER_A_ENVOYER%"=="prochains_rdv.csv" (
    ECHO Convertit la 1�re colonne du fichier en BASE36 [puisque cela n'a pas �t� fait par la requ�te]
    call %SCRIPT_PATH%\495-ColXToBase36.bat %FICHIER_A_ENVOYER% 1
)
IF /I "%FICHIER_A_ENVOYER%"=="rdv_du_jour_pour_alerte_sms.csv" (
    ECHO Convertit la 2�me colonne du fichier en BASE36 [puisque cela n'a pas �t� fait par la requ�te]
    call %SCRIPT_PATH%\495-ColXToBase36.bat %FICHIER_A_ENVOYER% 2
)
IF /I "%FICHIER_A_ENVOYER%"=="rdv_du_lendemain_pour_alerte_sms.csv" (
    ECHO Convertit la 2�me colonne du fichier en BASE36 [puisque cela n'a pas �t� fait par la requ�te]
    call %SCRIPT_PATH%\495-ColXToBase36.bat %FICHIER_A_ENVOYER% 2
)

timeout /T 1

REM pause
