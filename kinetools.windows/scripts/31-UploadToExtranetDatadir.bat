@REM 
@REM Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant � extraire des donn�es
@REM de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
@REM certaines t�ches de gestion. 
@REM 
@REM Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilit� au code source et des droits de copie,
@REM de modification et de redistribution accord�s par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
@REM seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les conc�dants successifs.
@REM 
@REM A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
@REM associ�s au chargement,  � l'utilisation,  � la modification et/ou au
@REM d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
@REM donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
@REM manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
@REM avertis poss�dant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
@REM logiciel � leurs besoins dans des conditions permettant d'assurer la
@REM s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
@REM � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
@REM 
@REM Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
@REM termes.
@REM 
@echo off

REM Ce script envoie le fichier en param�tre vers le dossier data qui est � la racine de l'extranet
REM Pour un extranet chez always data, ca sera dans /home/kine/www/data
REM Le mieux est de cr�er un utilisateur FTP qui a acc�s � ce dossier uniquement 
REM    (qui est la racine du FTP avec ce login)
REM Ainsi lors de l'upload il d�pose juste le fichier � la racine de son acc�s FTP, et le tour est jou�.

call "%~dp0\..\config\config.bat"

IF %FTP_EXTRANET_DISABLE_UPLOAD% EQU 1 (
	ECHO.
	ECHO Envoi FTP annul� [config FTP_EXTRANET_DISABLE_UPLOAD renseign�]
	ECHO.
	GOTO :EOF
)

set FICHIER_A_ENVOYER=%1

%WINSCP_PATH%\WinSCP\winscp.com /ini=nul /script=%SCRIPT_PATH%\32-UploadToExtranetDatadirWinSCP.script.txt /parameter ftp://%FTP_EXTRANETDATADIR_USERNAME%:%FTP_EXTRANETDATADIR_PASSWORD%@%FTP_EXTRANET_HOST%/ "%DATA_PATH%" "%FICHIER_A_ENVOYER%"
echo %errorlevel%

if not %ERRORLEVEL% EQU 0 (
	REM On propage l'erreur au script appelant
	echo.
	echo.
	echo Erreur dans WinSCP [ErrorLevel: %ERRORLEVEL%]
	exit /B 
)
rem pause
