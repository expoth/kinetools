# 
# Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant � extraire des donn�es
# de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
# certaines t�ches de gestion. 
# 
# Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilit� au code source et des droits de copie,
# de modification et de redistribution accord�s par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
# seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les conc�dants successifs.
# 
# A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
# associ�s au chargement,  � l'utilisation,  � la modification et/ou au
# d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
# donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
# manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
# avertis poss�dant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
# logiciel � leurs besoins dans des conditions permettant d'assurer la
# s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
# 
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
# termes.
# 

# Adapt� depuis :
# https://stackoverflow.com/q/64860

$FICHIER_A_ENVOYER = '.\' + $Args[0]
$FICHIER_A_ENVOYER_utf8 = '.\' + $Args[0] + '-utf8.csv'

if ( (get-childitem $FICHIER_A_ENVOYER).length -ne 0 ) {
    Get-Content -en string $FICHIER_A_ENVOYER | Out-File -en utf8 $FICHIER_A_ENVOYER_utf8
    #pause
    Remove-Item -Path $FICHIER_A_ENVOYER
    Rename-Item -Path $FICHIER_A_ENVOYER_utf8 -NewName $FICHIER_A_ENVOYER
} else {
	Write-Output "   Fichier vide: $FICHIER_A_ENVOYER"
}
# pause
