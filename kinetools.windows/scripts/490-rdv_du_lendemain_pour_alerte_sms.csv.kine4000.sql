-- 
-- Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant à extraire des données
-- de logiciels métiers pour kinésithérapeutes dans le but de faciliter
-- certaines tâches de gestion. 
-- 
-- Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilité au code source et des droits de copie,
-- de modification et de redistribution accordés par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
-- seule une responsabilité restreinte pèse sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les concédants successifs.
-- 
-- A cet égard  l'attention de l'utilisateur est attirée sur les risques
-- associés au chargement,  à l'utilisation,  à la modification et/ou au
-- développement et à la reproduction du logiciel par l'utilisateur étant 
-- donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
-- manipuler et qui le réserve donc à des développeurs et des professionnels
-- avertis possédant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
-- logiciel à leurs besoins dans des conditions permettant d'assurer la
-- sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
-- à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
-- 
-- Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
-- termes.
-- 

-- Rajouter la vérif que l'heure de rdv est bien renseignée !!

 SELECT RDV.CLEFPAT,
    RDV.CLEFPAT,
    '', -- patients_adr_tt_seances.fa_id,
    RDV.RDTITRE, -- Conversion à faire. 22=Mme, 31=M (cf DICO-FR.GIP)
    RDV.RDNOM, 
    '',
    RDV.RDPRENOM,
    RDV.RDTEL,
    RDV.RDTELHB, -- Vega a aussi TEL3 et TEL4
    CASE 
    when replace(replace(replace(replace(replace(RDTEL,'+33','0'),'.'),'-'),','),' ') ~ '^0[67][0-9]{8}$' then replace(replace(replace(replace(replace(RDTEL,'+33','0'),'.'),'-'),','),' ')
    else
        case 
        when replace(replace(replace(replace(replace(RDTELHB,'+33','0'),'.'),'-'),','),' ') ~ '^0[67][0-9]{8}$' then replace(replace(replace(replace(replace(RDTELHB,'+33','0'),'.'),'-'),','),' ')
        end
    end as mobile,
    to_char(RDV.RDDATE, 'YYYY-MM-DD') as RDDATE_YYYYMMDD, --RDV.RDDATE, -- Besoin de préciser le formattage en YYYY-MM-DD
	CASE WHEN cast(HDEB as integer) != 0 THEN
		concat (
				left( cast(HDEB as character(4)), 2 ) ,
				':',
				right( cast(HDEB as character(4)),2 )
		)
	END as HDEB,
    RDV.RDETAT as sea_etat, -- RDV.sea_etat, ''=Normal, 'A'=Arrivé, 'T'=Terminé, 'X'=validé en séance, 'K'=Annulé (absent), 'D'=Att désistement 
    'TODO' as sea_rappel_patient, -- RDV.sea_rappel_patient,
    'TODO' as sea_delai_rappel_pat, -- RDV.sea_delai_rappel_pat,
    PRAT, --PS.NOM, -- via SEANCES.REFPS (ou sinon via RDV.REFPS)
    'TODO', --PS.PRENOM, -- via SEANCES.REFPS (ou sinon via RDV.REFPS)
    CASE WHEN LieuRDV = 'S' THEN 1 ELSE 0 END as LieuRDV_perso -- Conversion à faire: 1=Dom 0=Cab -- N=Cab, S=Dom, C=Clinique, Y=Hôpital, R=Maison Retraite

   FROM RDV
    -- Voila la condition à utiliser si on veut des résultats
   --WHERE RDDATE > 20191201 
   -- Voila la condition à utiliser si on ne veut que celles du jour
   --WHERE RDDATE = cast(GETDATE() as date)
   
   -- Voila la condition à utiliser si on ne veut que celles du lendemain
    -- Date du lendemain, sauf si vendredi ou samedi, alors date du lundi
    -- Je considère le samedi comme non travaillé.
   WHERE RDDATE = (select case dayofweek(GETDATE()) 
        when 6 then cast(cast (cast(GETDATE() as date) as integer)+3 as date)
        when 7 then cast(cast (cast(GETDATE() as date) as integer)+2 as date)
        else cast(cast (cast(GETDATE() as date) as integer)+1 as date)
        end as lendemain
        from rdv
        limit 1
        )
   
   AND RDV.RDETAT != 'K'  --''=Normal, 'A'=Arrivé, 'T'=Terminé, 'X'=validé en séance, 'K'=Annulé (absent), 'D'=Att désistement 
   
    ORDER BY RDDATE, HDEB, RDNOM, RDPRENOM;
