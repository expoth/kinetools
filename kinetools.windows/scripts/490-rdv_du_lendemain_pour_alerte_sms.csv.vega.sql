-- 
-- Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant à extraire des données
-- de logiciels métiers pour kinésithérapeutes dans le but de faciliter
-- certaines tâches de gestion. 
-- 
-- Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilité au code source et des droits de copie,
-- de modification et de redistribution accordés par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
-- seule une responsabilité restreinte pèse sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les concédants successifs.
-- 
-- A cet égard  l'attention de l'utilisateur est attirée sur les risques
-- associés au chargement,  à l'utilisation,  à la modification et/ou au
-- développement et à la reproduction du logiciel par l'utilisateur étant 
-- donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
-- manipuler et qui le réserve donc à des développeurs et des professionnels
-- avertis possédant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
-- logiciel à leurs besoins dans des conditions permettant d'assurer la
-- sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
-- à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
-- 
-- Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
-- termes.
-- 

 SELECT BENEFIC.REFBENEF,
    cast(BENEFIC.REFBENEF as integer) as REFBENEF_int,
    '', -- patients_adr_tt_seances.fa_id,
    BENEFIC.CODECIVIL, -- Conversion à faire. 22=Mme, 31=M (cf DICO-FR.GIP)
    BENEFIC.NOMPATRO, -- C'est ici qu'il y a tjs un nom de renseigné
    BENEFIC.NOMUSUEL,
    BENEFIC.PRENOM,
    BENEFIC.TEL1,
    BENEFIC.TEL2, -- Vega a aussi TEL3 et TEL4
    CASE 
    when replace(BENEFIC.tel1,' ') ~ '^0[67][0-9]{8}$' then replace(BENEFIC.tel1,' ')
    else
        case 
        when replace(BENEFIC.tel2,' ') ~ '^0[67][0-9]{8}$' then replace(BENEFIC.tel2,' ')
        else
            case 
            when replace(BENEFIC.tel3,' ') ~ '^0[67][0-9]{8}$' then replace(BENEFIC.tel3,' ')
            else
                case 
                when replace(BENEFIC.tel4,' ') ~ '^0[67][0-9]{8}$' then replace(BENEFIC.tel4,' ')
                end
            end
        end
    end as mobile,
    to_char(RDV.DEBUTRDV, 'YYYY-MM-DD') as DEBUTRDV_YYYYMMDD, --RDV.DEBUTRDV, -- Besoin de préciser le formattage en YYYY-MM-DD
	CASE WHEN cast(RDV.DEBUTHRDV as integer) != 0 THEN
		concat (
				left( cast(RDV.DEBUTHRDV as character(4)), 2 ) ,
				':',
				right( cast(RDV.DEBUTHRDV as character(4)),2 )
		)
	END as debuthrdv,
    SEANCES.ETAT, -- (4=Facturée , 1=A facturer, 6=Annulée) Infos aussi dans RDV.STATUTRDV : 7=facturée, 6=a facturer/Prévue, 5=Annulée)
    'TODO' as sea_rappel_patient, -- RDV.sea_rappel_patient,
    'TODO' as sea_delai_rappel_pat, -- RDV.sea_delai_rappel_pat,
    PS.NOM, -- via SEANCES.REFPS (ou sinon via RDV.REFPS)
    PS.PRENOM, -- via SEANCES.REFPS (ou sinon via RDV.REFPS)
    LIEUEXE -- Conversion à faire: 1=Dom 0=Cab

   FROM SEANCESRDV, BENEFIC
    LEFT JOIN RDV ON BENEFIC.REFBENEF = RDV.REFBENEF
    LEFT JOIN SEANCES ON BENEFIC.REFBENEF = SEANCES.REFBENEF
    LEFT JOIN PS ON PS.REFPS = SEANCES.REFPS

   WHERE SEANCESRDV.IDRDV = RDV.IDRDV
   AND SEANCESRDV.NUMSEANCE = SEANCES.NUMSEANCE
   AND debuthrdv != ''
   
   -- Voila la condition à utiliser si on veut des résultats
   --AND RDV.DEBUTRDV > 20191201 
   -- Voila la condition à utiliser si on ne veut que celles du jour
   --AND RDV.DEBUTRDV = cast(GETDATE() as date)
   -- Voila la condition à utiliser si on ne veut que celles du lendemain
    -- Date du lendemain, sauf si vendredi ou samedi, alors date du lundi
    -- Je considère le samedi comme non travaillé.
   AND RDV.DEBUTRDV = (select case dayofweek(GETDATE()) 
        when 6 then cast(cast (cast(GETDATE() as date) as integer)+3 as date)
        when 7 then cast(cast (cast(GETDATE() as date) as integer)+2 as date)
        else cast(cast (cast(GETDATE() as date) as integer)+1 as date)
        end as lendemain
        from RDV
        limit 1
        )
        
   AND SEANCES.ETAT != 6 -- (4=Facturée , 1=A facturer, 6=Annulée) 
   
   Order by debutrdv, debuthrdv, NOMPATRO, Prenom
