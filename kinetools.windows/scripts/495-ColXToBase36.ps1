# 
# Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant � extraire des donn�es
# de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
# certaines t�ches de gestion. 
# 
# Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilit� au code source et des droits de copie,
# de modification et de redistribution accord�s par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
# seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les conc�dants successifs.
# 
# A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
# associ�s au chargement,  � l'utilisation,  � la modification et/ou au
# d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
# donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
# manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
# avertis poss�dant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
# logiciel � leurs besoins dans des conditions permettant d'assurer la
# s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
# 
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
# termes.
# 

# Set-ExecutionPolicy -ExecutionPolicy bypass -scope CurrentUser
# Set-ExecutionPolicy -ExecutionPolicy Default -scope CurrentUser

# convertTo-Base36 & convertFrom-base36 Recopi� depuis :
# https://ss64.com/ps/syntax-base36.html

function convertTo-Base36
{
    [CmdletBinding()]
    param ([parameter(valuefrompipeline=$true, HelpMessage="Integer number to convert")][int]$decNum="")
    $alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    do
    {
        $remainder = ($decNum % 36)
        $char = $alphabet.substring($remainder,1)
        $base36Num = "$char$base36Num"
        $decNum = ($decNum - $remainder) / 36
    }
    while ($decNum -gt 0)

    $base36Num
}

# source: adapted from Tony Marston's PHP code

# Convert from Base 36 back to Decimal:

function convertFrom-base36
{
    [CmdletBinding()]
    param ([parameter(valuefrompipeline=$true, HelpMessage="Alphadecimal string to convert")][string]$base36Num="")
    $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz"
    $inputarray = $base36Num.tolower().tochararray()
    [array]::reverse($inputarray)
    [long]$decNum=0
    $pos=0

    foreach ($c in $inputarray)
    {
        $decNum += $alphabet.IndexOf($c) * [long][Math]::Pow(36, $pos)
        $pos++
    }
    $decNum
}

$FICHIER_A_ENVOYER = '.\' + $Args[0]
$FICHIER_A_ENVOYER_B36 = '.\' + $Args[0] + '-base36.csv'
$myCOL = $Args[1]

#Write-Output "   FICHIER_A_ENVOYER: $FICHIER_A_ENVOYER"
#Write-Output "   FICHIER_A_ENVOYER_B36: $FICHIER_A_ENVOYER_B36"
#Write-Output "   myCOL: $myCOL"

if ( (get-childitem $FICHIER_A_ENVOYER).length -ne 0 ) {
	# R�cup�re la 1�re ligne du fichier
	$DataLine = (Get-Content $FICHIER_A_ENVOYER -TotalCount 1)
	# R�cup�re le nb de colonnes (= nb de d�limiteurs + 1)
	$nbColonnes = ([char[]]$DataLine -eq "`t").count + 1

	Write-Output "   Nombre de colonnes: $nbColonnes"

	# Convertit la [$myCOL]-�me colonne en base36
	Import-Csv -Path $FICHIER_A_ENVOYER -Header (1..$nbColonnes) -Delimiter "`t" | ForEach-Object {
		$_.$myCOL = convertTo-Base36 -decNum $_.$myCOL
		$_
	} | ConvertTo-Csv -NoTypeInformation -Delimiter "`t" |
	 Select-Object -Skip 1 |
	 Set-Content -path $FICHIER_A_ENVOYER_B36

	#pause

	Remove-Item -Path $FICHIER_A_ENVOYER
	Rename-Item -Path $FICHIER_A_ENVOYER_B36 -NewName $FICHIER_A_ENVOYER
} else {
	Write-Output "   Fichier vide: $FICHIER_A_ENVOYER"
}

# pause
