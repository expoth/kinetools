-- 
-- Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant � extraire des donn�es
-- de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
-- certaines t�ches de gestion. 
-- 
-- Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilit� au code source et des droits de copie,
-- de modification et de redistribution accord�s par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
-- seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les conc�dants successifs.
-- 
-- A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
-- associ�s au chargement,  � l'utilisation,  � la modification et/ou au
-- d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
-- donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
-- manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
-- avertis poss�dant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
-- logiciel � leurs besoins dans des conditions permettant d'assurer la
-- s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
-- � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
-- 
-- Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
-- termes.
-- 

-- Certains champs sont � finir

 SELECT RDV.CLEFPAT,
    '', -- patients_adr_tt_seances.fa_id,
    RDV.RDTITRE, -- D�j� sous la forme compl�te 'Monsieur'...
    RDV.RDNOM, 
    '',
    RDV.RDPRENOM,
    RDV.RDADR1,
    RDV.RDADR2,
    RDV.RDCP,
    RDV.RDVILLE,
    RDV.RDTEL,
    RDV.RDTELHB,
    '', --ASSURES.CODECIVIL,
    '', --ASSURES.NOMPATRO,
    '', --ASSURES.PRENOM,
    '', --ASSURES.TEL1,
    '', --ASSURES.TEL2, -- Vega a aussi TEL3
    ORDONNANCE, --ORDO.NUMORDO,
    '', --ORDO.TXCOTATION,
    '', --ORDO.DATEORDO,
    '', --ORDO.REFBENEF as REFBENEF_ordo,
    '', --ORDO.REFPS as REFPS_ordo,
    '', --PS_ordo.NOM AS REFPS_ordo_NOM,
    '', --PS_ordo.PRENOM REFPS_ordo_PRENOM,
    -- Il y a une table SERIES dans Vega, mais ca n'a pas l'air 
    -- d'avoir la m�me signification que dans Kinemax
    'TODO', --itv_serie.ser_id,
    'TODO', --itv_serie.ser_numero,
    'TODO', --itv_serie.ser_nb,
    'TODO', --itv_seance_serie.sse_etat,
    'TODO', --itv_seance_serie.sse_tarification,
    'TODO', --RDV.OBJETRDV, -- Ou alors faire une combinaison de SEANCES, ce qui serait plus juste (on veut afficher la cotation)
    'TODO', --itv_seance.sea_ordre_seance,
    to_char(RDV.RDDATE, 'YYYY-MM-DD') as RDDATE_YYYYMMDD, --RDV.RDDATE, -- Besoin de pr�ciser le formattage en YYYY-MM-DD
	CASE WHEN cast(HDEB as integer) != 0 THEN
		concat (
				left( cast(HDEB as character(4)), 2 ) ,
				':',
				right( cast(HDEB as character(4)),2 )
		)
	END as HDEB,
    RDV.RDETAT as sea_etat, -- RDV.sea_etat, ''=Normal, 'A'=Arriv�, 'T'=Termin�, 'X'=valid� en s�ance, 'K'=Annul� (absent), 'D'=Att d�sistement 
    'TODO' as sea_rappel_patient, -- RDV.sea_rappel_patient,
    'TODO' as sea_delai_rappel_pat, -- RDV.sea_delai_rappel_pat,
    CASE WHEN LieuRDV = 'S' THEN 1 ELSE 0 END as LieuRDV_perso, -- N=Cab, S=Dom, C=Clinique, Y=H�pital, R=Maison Retraite
    'TODO', -- itv_seance.sea_deplacement_non_facture,
    PRAT,
    PRAT, -- (en attendant d'avoir le nom, on utilise PRAT --PS_sea.NOM, -- via SEANCES.REFPS (ou sinon via RDV.REFPS)
    '', --PS_sea.PRENOM -- via SEANCES.REFPS (ou sinon via RDV.REFPS)
    'kine4000' as app --Application metier

   FROM RDV
   -- On prend tous les rdv jusqu'� 49j (=7 semaines) avant la date du jour
   WHERE RDDATE >= cast(cast (cast(GETDATE() as date) as integer)-49 as date)
   -- Voila la condition � utiliser si on veut des r�sultats
   --WHERE RDDATE > 20191201 
   -- Voila la condition � utiliser si on ne veut que celles apr�s auj
   --WHERE RDDATE >= cast(GETDATE() as date)
   
   -- L'ordre est important pour que l'affichage en PHP fonctionne bien
   -- Surtout heure, puis date, apr�s c'est moins important.
    ORDER BY HDEB, RDDATE, RDNOM, RDPRENOM;
