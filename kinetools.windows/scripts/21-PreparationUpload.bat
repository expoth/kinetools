@REM 
@REM Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant � extraire des donn�es
@REM de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
@REM certaines t�ches de gestion. 
@REM 
@REM Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilit� au code source et des droits de copie,
@REM de modification et de redistribution accord�s par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
@REM seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les conc�dants successifs.
@REM 
@REM A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
@REM associ�s au chargement,  � l'utilisation,  � la modification et/ou au
@REM d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
@REM donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
@REM manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
@REM avertis poss�dant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
@REM logiciel � leurs besoins dans des conditions permettant d'assurer la
@REM s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
@REM � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
@REM 
@REM Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
@REM termes.
@REM 
@echo off

REM Calcule le HashFile 
REM Puis envoie les fichiers par FTP en appelant un script externe

call "%~dp0\..\config\config.bat"

set FICHIER_A_ENVOYER=%1
call "%~dp0\..\config\logfilename.bat"

REM Sachant qu'on fait une pause de 6 sec entre chaque ping, 10essais = 1min, 600 = 60min
set /a NB_MAX_ESSAIS_FTP=600
set /a NB_MIN_PING_OK=8

cd /D "%DATA_PATH%"


REM : G�n�re le hash MD5 du fichier � envoyer.
IF %USE_MD5SUM% EQU 1 (
	REM echo Compute MD5 with md5sum
	goto md5sum
) else (
	REM echo Compute MD5 with certutil
	goto certutil
)

:md5sum
"%UnxUtils_PATH%\md5sum" %FICHIER_A_ENVOYER%  >  %FICHIER_A_ENVOYER%.md5sum
REM r�cup�re le 1er mot du fichier md5sum, ie le hash MD5 et le met dans le fichier certutil.md5
REM dans le but de recree un fichier ressemblant � celui cr�� par CertUtil. Il sera alors utilisable
REM par le script linux. Le MD5 hash est attendu tout seul � la 2e ligne du fichier certutil.
echo Fichier cree manuellement selon format Certutil avec calcul du Hash MD5 par md5sum> %FICHIER_A_ENVOYER%.certutil.md5
REM R�cup�re le 1er mot du fichier.
for /f "usebackq" %%a in ( "%FICHIER_A_ENVOYER%.md5sum" ) do (
	set a=%%a
)
echo %a%>> %FICHIER_A_ENVOYER%.certutil.md5
goto doFTP

:certutil
%SystemRoot%\System32\CertUtil -hashfile %FICHIER_A_ENVOYER% MD5 > %FICHIER_A_ENVOYER%.certutil.md5
goto doFTP



:doFTP
IF %FTP_VM_DISABLE_UPLOAD% EQU 1 (
	ECHO.
	ECHO Envoi FTP annul� [config FTP_VM_DISABLE_UPLOAD renseign�]
	ECHO.
	GOTO :EOF
)

REM : On ne lance l'upload FTP que si le serveur r�pond au ping.
setlocal enableextensions enabledelayedexpansion
set /a COUNT=0
set /a COUNT_SUCCESS=0

:TestFTP
set /a COUNT+=1
Echo V�rification si %VM_HOST% est accessible... Essai no. %COUNT%/%NB_MAX_ESSAIS_FTP%
PING %VM_HOST% -n 1 > nul
IF %ERRORLEVEL% EQU 0 (
	set /a COUNT_SUCCESS+=1
	Echo     Oui... No. !COUNT_SUCCESS!
	if !COUNT_SUCCESS! GEQ %NB_MIN_PING_OK% (
		REM Ici on a r�ussi � atteindre l'h�te, donc on va essayer l'envoi par FTP.
		Echo     !COUNT_SUCCESS!/%NB_MIN_PING_OK% essais de ping fructueux. On lance FTP
		echo=
		echo=
		call "%SCRIPT_PATH%\22-uploadWithWinSCP.bat" %FICHIER_A_ENVOYER%
		if ERRORLEVEL 1 (
			REM ERRORLEVEL est sup�rieur � 1 : ie. WinSCP a rencontr� un erreur
			echo.
			echo Erreur FTP ! Peut-�tre les fichiers existent d�j� sur la destination
			echo  [fichiers d�j� dans /incoming alors qu'on est logu� en tant qu'anonymous ]
			echo.
			echo ftpError: Erreur d'envoi FTP. Peut-�tre les fichiers existent d�j� sur la destination. >> %LOGFILENAME%
			exit /b
		) else (
			echo ftpDone: Envoi FTP effectu� >> %LOGFILENAME%
		)
		REM Pause
		REM Les fichiers sont supprim�s par WinSCP, donc on met en commentaire ici
		REM del %FICHIER_A_ENVOYER%
		REM del %FICHIER_A_ENVOYER%.certutil.md5
		IF %USE_MD5SUM% EQU 1 (
			del %FICHIER_A_ENVOYER%.md5sum
		)
	) ELSE (
		Timeout /t 1
		GOTO TestFTP
	)
	rem GOTO :EOF
) else (
	Echo     H�te %VM_HOST% inaccessible. Pas d'envoi FTP effectu�.
	set /a COUNT_SUCCESS=0
	REM Pour la date
	REM https://stackoverflow.com/a/23476347
	for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
	set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
	set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
	set "datestamp=%YYYY%%MM%%DD%" & set "timestamp=%HH%%Min%%Sec%"
	echo !datestamp!_!timestamp! H�te %VM_HOST% inaccessible. Pas d'envoi FTP effectu�. >> %LOGFILENAME%
	if %COUNT% LSS %NB_MAX_ESSAIS_FTP% (
		Timeout /t 6
		GOTO TestFTP
	)
)
endlocal

REM pause
