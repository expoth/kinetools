@REM 
@REM Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant � extraire des donn�es
@REM de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
@REM certaines t�ches de gestion. 
@REM 
@REM Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilit� au code source et des droits de copie,
@REM de modification et de redistribution accord�s par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
@REM seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les conc�dants successifs.
@REM 
@REM A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
@REM associ�s au chargement,  � l'utilisation,  � la modification et/ou au
@REM d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
@REM donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
@REM manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
@REM avertis poss�dant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
@REM logiciel � leurs besoins dans des conditions permettant d'assurer la
@REM s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
@REM � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
@REM 
@REM Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
@REM termes.
@REM 
@echo off

call "%~dp0\..\config\config.bat"

if not defined LOGICMAX_BASEDIR (
	echo "Installation de kinemax non trouv�e. Abandon."
	pause
	exit
)

set I_STARTED_PGSQL=0

REM Le nom du fichier doit �tre celui g�n�r� par la requ�te SQL
REM c'est � dire celui qu'on veut envoyer.
set FICHIER_A_ENVOYER=%1
set TARGET_HOST=%2

cd /D "%TOOLDIR%"


REM V�rifie si le service existe et selon le r�sultat, 
REM appeler la cible GOTO ad�quate
SC QUERY %PGSQL_KINEMAX_SERVICE_NAME% > NUL
IF ERRORLEVEL 1060 GOTO testPGSQLUp-withoutService
GOTO testPGSQLUp


REM Dans le cas o� le service %PGSQL_KINEMAX_SERVICE_NAME% existe 
REM on d�marre Postgresql � l'aide du service syst�me
:testPGSQLUp
ECHO Testing if Postgresql is up [Service %PGSQL_KINEMAX_SERVICE_NAME% is present]
REM https://stackoverflow.com/a/3325123 (check if a service is running via batch file)
for /F "tokens=3 delims=: " %%H in ('sc query %PGSQL_KINEMAX_SERVICE_NAME% ^| findstr "        STATE"') do (
  if /I "%%H" NEQ "RUNNING" (
    REM Put your code you want to execute here
    REM For example, the following line
    echo Serveur PostrgreSQL: pas d�marr�
	for /F "tokens=3 delims=: " %%G in ('sc qc %PGSQL_KINEMAX_SERVICE_NAME% ^| findstr "        START_TYPE"') do (
	    if /I "%%G" NEQ "DISABLED" (
			REM Le service n'est pas Disabled, donc on peut le d�marrer
		    GOTO launchPostgreSQL
		) else (
			REM Le service est Disabled, donc on ne peut pas le d�marrer, on bascule donc sur la m�thode sans service.
			echo Service %PGSQL_KINEMAX_SERVICE_NAME% est "DISABLED"
			echo On bascule sur la m�thode sans service
			goto testPGSQLUp-withoutService
		)
	)
  ) else (
	echo Serveur PostrgreSQL: d�marr�
	GOTO runSQLQuery
  )
)
GOTO :EOF

:launchPostgreSQL
echo Serveur PostrgreSQL: D�marrage en cours
net start %PGSQL_KINEMAX_SERVICE_NAME%
set I_STARTED_PGSQL=1
GOTO testPGSQLUp

:runSQLQuery
Echo   Extraction des donn�es de la BDD...
call %SCRIPT_PATH%\12-LanceRequeteSQL.kinemax.bat %FICHIER_A_ENVOYER%
goto stopPgSQL

:stopPgSQL
REM On n'arrete le service que si 
REM  1) on a du le d�marrer dans ce script
REM  2) il a le START_TYPE � d�marrage automatique (AUTO_START)
if %I_STARTED_PGSQL% EQU 1 (
    for /F "tokens=3 delims=: " %%H in ('sc qc %PGSQL_KINEMAX_SERVICE_NAME% ^| findstr "        START_TYPE"') do (
        if /I "%%H" NEQ "AUTO_START" (
            echo Serveur PostrgreSQL: Arret en cours
	        net stop %PGSQL_KINEMAX_SERVICE_NAME%
        )
    )
)
goto preparationFtp

REM Dans le cas o� le service %PGSQL_KINEMAX_SERVICE_NAME% n'existe pas
REM on d�marre Postgresql directement sans passer par un service syst�me
:testPGSQLUp-withoutService
ECHO Testing if Postgresql is up [Service %PGSQL_KINEMAX_SERVICE_NAME% doesn't exist]
"%PGSQLDIR%\bin\pg_ctl" status -D "%PGSQLDIR%\data"
if %ERRORLEVEL% EQU 0 (
	echo Serveur PostrgreSQL: d�marr�
	GOTO runSQLQuery-withoutService
) else (
	if %ERRORLEVEL% EQU 3 (
		echo Serveur PostrgreSQL: pas d�marr� ERRORLEVEL:3
		GOTO launchPostgreSQL-withoutService
	) else (
		echo Serveur PostrgreSQL: Autre probl�me: ERRORLEVEL:%ERRORLEVEL%
	)
	REM TIMEOUT /T 20 /NOBREAK
)
GOTO :EOF

:launchPostgreSQL-withoutService
echo Serveur PostrgreSQL: D�marrage en cours
start "PostgreSQL" /B /wait "%PGSQLDIR%\bin\pg_ctl" start -D "%PGSQLDIR%\data" -w > nul
set I_STARTED_PGSQL=1
REM TIMEOUT /T 15 /NOBREAK
GOTO testPGSQLUp-withoutService

:runSQLQuery-withoutService
Echo   Extraction des donn�es de la BDD...
call %SCRIPT_PATH%\12-LanceRequeteSQL.kinemax.bat %FICHIER_A_ENVOYER%
goto stopPgSQL-withoutService

:stopPgSQL-withoutService
if %I_STARTED_PGSQL% EQU 1 (
	echo Serveur PostrgreSQL: Arret en cours
	"%PGSQLDIR%\bin\pg_ctl" stop -D "%PGSQLDIR%\data" > nul
)
goto preparationFtp


REM D�but du traitement du r�sultat de la requ�te SQL
:preparationFtp
Echo   Pr�paration FTP et Upload FTP...
IF /I "%TARGET_HOST%"=="ExtranetDatadir" goto doExtranetDatadir
IF /I "%TARGET_HOST%"=="LocalhostIntranetDataDir" goto doLocalhostIntranetDataDir
REM : Else
call %SCRIPT_PATH%\21-PreparationUpload.bat %FICHIER_A_ENVOYER%
goto commonexit

:doExtranetDatadir
	call %SCRIPT_PATH%\31-UploadToExtranetDatadir.bat %FICHIER_A_ENVOYER%
	goto commonexit
:doLocalhostIntranetDataDir
	call %SCRIPT_PATH%\31-UploadToLocalhostIntranetDataDir.bat %FICHIER_A_ENVOYER%
	goto commonexit

:commonexit
GOTO :EOF

REM PAUSE
