@REM 
@REM Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant � extraire des donn�es
@REM de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
@REM certaines t�ches de gestion. 
@REM 
@REM Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilit� au code source et des droits de copie,
@REM de modification et de redistribution accord�s par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
@REM seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les conc�dants successifs.
@REM 
@REM A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
@REM associ�s au chargement,  � l'utilisation,  � la modification et/ou au
@REM d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
@REM donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
@REM manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
@REM avertis poss�dant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
@REM logiciel � leurs besoins dans des conditions permettant d'assurer la
@REM s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
@REM � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
@REM 
@REM Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
@REM termes.
@REM 
@echo off

call "%~dp0\..\config\config.bat"

REM Le nom du fichier doit �tre celui g�n�r� par la requ�te SQL
REM c'est � dire celui qu'on veut envoyer.
set FICHIER_A_ENVOYER=%1
set TARGET_HOST=%2
set PROGRAMME_METIER=kine4000

cd /D "%TOOLDIR%"

:runSQLQuery
Echo   Extraction des donn�es de la BDD...
if /I "%KINE4000_DB_ENGINE%"=="ODBC" call %SCRIPT_PATH%\12-LanceRequeteSQL.odbc.bat %FICHIER_A_ENVOYER% %PROGRAMME_METIER%
if /I "%KINE4000_DB_ENGINE%"=="OLEDB" call %SCRIPT_PATH%\12-LanceRequeteSQL.oledb.bat %FICHIER_A_ENVOYER% %PROGRAMME_METIER%
goto preparationFtp

REM D�but du traitement du r�sultat de la requ�te SQL
:preparationFtp
Echo   Pr�paration FTP et Upload FTP...
IF /I "%TARGET_HOST%"=="ExtranetDatadir" goto doExtranetDatadir
IF /I "%TARGET_HOST%"=="LocalhostIntranetDataDir" goto doLocalhostIntranetDataDir
REM : Else
call %SCRIPT_PATH%\21-PreparationUpload.bat %FICHIER_A_ENVOYER%
goto commonexit

:doExtranetDatadir
	call %SCRIPT_PATH%\31-UploadToExtranetDatadir.bat %FICHIER_A_ENVOYER%
	goto commonexit
:doLocalhostIntranetDataDir
	call %SCRIPT_PATH%\31-UploadToLocalhostIntranetDataDir.bat %FICHIER_A_ENVOYER%
	goto commonexit

:commonexit
GOTO :EOF

REM PAUSE
