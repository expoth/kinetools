# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

# Set-ExecutionPolicy -ExecutionPolicy bypass -scope CurrentUser
# Set-ExecutionPolicy -ExecutionPolicy Default -scope CurrentUser

#clear-host

$FICHIER_A_ENVOYER = '.\' + $Args[0]
$OLEDB_CONN_STR = $Args[1]
$SQL_QUERY_FILE = $Args[2]

#Write-Output "   FICHIER_A_ENVOYER: $FICHIER_A_ENVOYER"
#Write-Output "   OLEDB_CONN_STR: $OLEDB_CONN_STR"
#Write-Output "   SQL_QUERY_FILE: $SQL_QUERY_FILE"

#HFSQL Classic : Mdp marche avec *, mais pas si on renseigne le nom de la table (T1 par ex)
#$connStr = "Provider=PCSOFT.HFSQL;Data Source=C:\Mes Projets\Mon_Projet\Mon_Projet.wdd;Initial Catalog=C:\Mes Projets\Mon_Projet\Exe\;User ID=;Password=;Extended Properties='Password=*:myPassword;Language=UTF-8';"

#$connStr = "Provider=PCSOFT.HFSQL;Data Source=C:\Mes Projets\Mon_Projet\Mon_Projet.wdd;Initial Catalog=C:\Mes Projets\Mon_Projet\Exe\;Extended Properties='Password=T1:myPassword;Password=*:a';"
#$connStr = "Provider=PCSOFT.HFSQL;Data Source=C:\Mes Projets\Mon_Projet\Mon_Projet.wdd;Initial Catalog=C:\Mes Projets\Mon_Projet\Exe\;User ID=;Password=;"
#$connStr = "Provider=PCSOFT.HFSQL;Data Source=C:\Vega5\outils\_ssv.wdd;Initial Catalog=C:\Vega5\Fichiers\;"

#HFSQL C/S : Mdp de table fonctionne sans pb.
#$connStr = "Provider=PCSOFT.HFSQL;Data Source=localhost:4900;Initial Catalog=MaBDD;User ID=Admin;Password=;Extended Properties='Password=T1:myPassword;Language=UTF-8';"

$connStr = $OLEDB_CONN_STR
# write-host ConnStr: $connStr

$objConn = New-Object System.Data.OleDb.OleDbConnection ($connStr)

$query = [IO.File]::ReadAllText($SQL_QUERY_FILE)
#write-host $query

$sqlCommand = New-Object System.Data.OleDb.OleDbCommand($query)
$sqlCommand.Connection = $objConn
#write-host $sqlCommand.CommandText

$objConn.open()

$adapter = New-Object -TypeName System.Data.OleDb.OleDbDataAdapter($sqlCommand)
$dataset = New-Object -TypeName System.Data.DataSet
$adapter.Fill($dataset)
#$dataset.Tables[0] | export-csv $FICHIER_A_ENVOYER -NoTypeInformation -Encoding UTF8 -Delimiter "`t"
# On ne veut pas garder la ligne des en-têtes
$dataset.Tables[0] | ConvertTo-Csv -NoTypeInformation -Delimiter "`t" |
	Select-Object -Skip 1 |
	Set-Content -Encoding UTF8 -path $FICHIER_A_ENVOYER

$sqlCommand.Dispose()
$objConn.close()
$objConn.Dispose()

# pause
