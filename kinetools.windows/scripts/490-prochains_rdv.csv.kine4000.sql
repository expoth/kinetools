-- 
-- Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant à extraire des données
-- de logiciels métiers pour kinésithérapeutes dans le but de faciliter
-- certaines tâches de gestion. 
-- 
-- Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilité au code source et des droits de copie,
-- de modification et de redistribution accordés par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
-- seule une responsabilité restreinte pèse sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les concédants successifs.
-- 
-- A cet égard  l'attention de l'utilisateur est attirée sur les risques
-- associés au chargement,  à l'utilisation,  à la modification et/ou au
-- développement et à la reproduction du logiciel par l'utilisateur étant 
-- donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
-- manipuler et qui le réserve donc à des développeurs et des professionnels
-- avertis possédant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
-- logiciel à leurs besoins dans des conditions permettant d'assurer la
-- sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
-- à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
-- 
-- Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
-- termes.
-- 

-- A Améliorer encore un peu
-- S'execute bien avec "odbcview"
--
-- odbcview.exe -s -d \t -q  "DSN=Vega;uid=a;pwd=a"  my_query.sql my_output.csv
--

select CLEFPAT,
    to_char(RDDATE, 'YYYY-MM-DD') as RDDATE_YYYYMMDD, --RDDATE, -- Besoin de préciser le formattage en YYYY-MM-DD
	CASE WHEN cast(HDEB as integer) != 0 THEN
		concat (
				left( cast(HDEB as character(4)), 2 ) ,
				':',
				right( cast(HDEB as character(4)),2 )
		)
	END as HDEB
FROM RDV
--Where RDDATE > 20191201
Where RDDATE >= cast(GETDATE() as date)
AND RDETAT != 'K' --''=Normal, 'A'=Arrivé, 'T'=Terminé, 'X'=validé en séance, 'K'=Annulé (absent), 'D'=Att désistement 
ORDER BY RDV.CLEFPAT, RDDATE, HDEB;
