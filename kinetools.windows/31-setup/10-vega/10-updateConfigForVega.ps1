#
# Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
#
# Ce fichier fait partie du logiciel KineTools Suite.
#
# Ce logiciel est un programme informatique servant � extraire des donn�es
# de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
# certaines t�ches de gestion.
#
# Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA
# sur le site "http://www.cecill.info".
#
# En contrepartie de l'accessibilit� au code source et des droits de copie,
# de modification et de redistribution accord�s par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
# seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les conc�dants successifs.
#
# A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
# associ�s au chargement,  � l'utilisation,  � la modification et/ou au
# d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
# donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe �
# manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
# avertis poss�dant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
# logiciel � leurs besoins dans des conditions permettant d'assurer la
# s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement,
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
#
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez
# pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
# termes.
#

$ShortcutName = "Vega5.lnk"
$AnalyseFileNames = @("_ssv.wdd")

$DesktopPathes = @(
	[Environment]::GetFolderPath("Desktop"), #"c:\Users\$user\Desktop"
	[Environment]::GetFolderPath("DesktopDirectory"),  #"c:\Users\$user\Desktop"
	($env:public + "\Desktop"), # Existe � partir de Win7 = "c:\Users\Public\Desktop"
	"Y:\",
	"c:\Users\Public\Desktop"
)

foreach ($DesktopPath in $DesktopPathes) {
	#Write-Output "   DesktopPath cherch�: $DesktopPath"
	if ( Test-Path -LiteralPath ($DesktopPath + "\" + $ShortcutName) -PathType Leaf ) {
		$ShortcutFullPath = $DesktopPath + "\" + $ShortcutName
		#Write-Output "   Raccourci trouv� ici: $ShortcutFullPath"
		break
	}
}

if ($ShortcutFullPath) {
    Write-Output "Raccourci trouv� ici: $ShortcutFullPath"

    $Shell = New-Object -ComObject ("WScript.Shell")
    $ShortCut = $Shell.CreateShortcut($ShortcutFullPath)
    $lnkPath = $ShortCut.TargetPath
    #$lnkArgs = $ShortCut.Arguments

    # Exemple
    # C:\Vega5\Vega5.exe

    Write-Output "lnkPath $lnkPath"

    # D�termine le dossier d'installation de Vega5
    $InstallDir=Split-Path -Path "$lnkPath" -Parent
    Write-Output "InstallDir $InstallDir"

    # D�termine REPFIC
    $RepFic = "$InstallDir\Fichiers"

    # Liste des chemins o� chercher l'analyse
    $VegaPathes = @("$InstallDir\outils",
                    $InstallDir,
                    "C:\Vega5"
    )

    # D�termine Fichier ANALYSE (on prend le 1er trouv�)
    :firstLoop foreach ($aPath in $VegaPathes) {
        foreach ($AnalyseFileName in $AnalyseFileNames) {
            #Write-Output "$aPath"
            if ( Test-Path -LiteralPath $aPath ) {
                $myAnalyseFileInfo = Get-ChildItem -Path "$aPath" -Include $AnalyseFileName -Recurse
                if ($myAnalyseFileInfo) {
                    $myAnalyse = [string[]]($myAnalyseFileInfo)
                    $myAnalyseFirst = $myAnalyse[0]
                    break firstLoop
                } else {
                    Write-Output "Analyse Non trouv�e"
                }
            }
        }
    }

    Write-Output ""
    Write-Output ""
    Write-Output "REP: $RepFic"
    Write-Output "ANA: $myAnalyseFirst"
    Write-Output ""

    # Met � jour le fichier de config de Kinetools pour Vega5"
    $myConfFile = $Args[0]
    Write-Output "Met � jour le fichier de config de Kinetools pour Vega5"
    Write-Output "  (fichier: $myConfFile)"

    (Get-Content $myConfFile) -replace('^(set VEGA_REP=).*', ('$1'+$RepFic)) | Set-Content $myConfFile
    (Get-Content $myConfFile) -replace('^(set VEGA_ANA=).*', ('$1'+$myAnalyseFirst)) | Set-Content $myConfFile
} else {
    Write-Output "Raccourci vers l'installation de Vega5 non trouv�. Y a-t-il bien une installation de Vega5 sur ce syst�me ?"
}
