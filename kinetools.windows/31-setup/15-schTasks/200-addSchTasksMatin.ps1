# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

# https://stackoverflow.com/questions/2000674/powershell-create-scheduled-task-to-run-as-local-system-service

$cmd = $Args[0]
$logPath = $Args[1]
$batch = Split-Path -path "$cmd" -Leaf
$userId = $(whoami)

$taskname = "KT - $batch"
$taskdescription = "Launch $batch"

# Remove scheduled task if it already exists
$task = Get-ScheduledTask -TaskName $taskname -TaskPath "\KineTools\" -ErrorAction SilentlyContinue
if ($task) {
	Unregister-ScheduledTask -TaskName $taskname -TaskPath "\KineTools\" -Confirm:$false
}

# Create scheduled task Action
$action = New-ScheduledTaskAction -Execute "cmd" -Argument ("/c " + """$cmd SchTask > $logPath\$batch.txt 2>&1""")

# Create scheduled task Principal
# Option: N'executer que si un utilisateur a ouvert une session
# Pas besoin de lancer ce script PS1 en tant qu'admin
#$principal = New-ScheduledTaskPrincipal -UserId "$userId" -LogonType ServiceAccount

# Option: Executer même si aucun utilisateur n'a ouvert de session + Ne pas stocker le mot de passe
# Nécessite qu'il n'y ait pas de pause à la fin du bat appelé.
# Permet de ne pas afficher de fenêtre console lorsque le batch est lancé.
# Besoin de lancer ce script PS1 en tant qu'admin
$principal = New-ScheduledTaskPrincipal -UserId "$userId" -LogonType S4U

$trigger =  New-ScheduledTaskTrigger -Daily -At "07:00"
#$Trigger.Repetition = $(New-ScheduledTaskTrigger -Once -At "07:00" `
# -RepetitionInterval (New-TimeSpan -minutes 30) -RepetitionDuration (New-TimeSpan -days 1) `
# ).Repetition

$settings = New-ScheduledTaskSettingsSet `
  -ExecutionTimeLimit (New-TimeSpan -Minutes 3) `
  -RestartCount 3 `
  -RestartInterval (New-TimeSpan -Minutes 1) `
  -StartWhenAvailable `
  -DontStopOnIdleEnd `
  -WakeToRun

Register-ScheduledTask `
   -Action $action `
   -Principal $principal `
   -Trigger $trigger `
   -TaskName $taskname `
   -Description $taskdescription `
   -TaskPath "\KineTools\" `
   -Settings $settings #`
   #-User "System"
