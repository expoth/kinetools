#
# Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
#
# Ce fichier fait partie du logiciel KineTools Suite.
#
# Ce logiciel est un programme informatique servant � extraire des donn�es
# de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
# certaines t�ches de gestion.
#
# Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA
# sur le site "http://www.cecill.info".
#
# En contrepartie de l'accessibilit� au code source et des droits de copie,
# de modification et de redistribution accord�s par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
# seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les conc�dants successifs.
#
# A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
# associ�s au chargement,  � l'utilisation,  � la modification et/ou au
# d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
# donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe �
# manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
# avertis poss�dant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
# logiciel � leurs besoins dans des conditions permettant d'assurer la
# s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement,
# � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
#
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez
# pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
# termes.
#


function Get-IniFile
{
    # Copi� depuis https://stackoverflow.com/a/43697842
    param(
        [parameter(Mandatory = $true)] [string] $filePath
    )

    $anonymous = "NoSection"

    $ini = @{}
    switch -regex -file $filePath
    {
        "^\[(.+)\]$" # Section
        {
            $section = $matches[1]
            $ini[$section] = @{}
            $CommentCount = 0
        }

        "^(;.*)$" # Comment
        {
            if (!($section))
            {
                $section = $anonymous
                $ini[$section] = @{}
            }
            $value = $matches[1]
            $CommentCount = $CommentCount + 1
            $name = "Comment" + $CommentCount
            $ini[$section][$name] = $value
        }

        "(.+?)\s*=\s*(.*)" # Key
        {
            if (!($section))
            {
                $section = $anonymous
                $ini[$section] = @{}
            }
            $name,$value = $matches[1..2]
            $ini[$section][$name] = $value
        }
    }

    return $ini
}

$ShortcutName = "KINE+4000.lnk"
$AnalyseFileNames = @("hf6.wdd")

$DesktopPathes = @(
	[Environment]::GetFolderPath("Desktop"), #"c:\Users\$user\Desktop"
	[Environment]::GetFolderPath("DesktopDirectory"),  #"c:\Users\$user\Desktop"
	($env:public + "\Desktop"), # Existe � partir de Win7 = "c:\Users\Public\Desktop"
	"Y:\",
	"c:\Users\Public\Desktop"
)

foreach ($DesktopPath in $DesktopPathes) {
	#Write-Output "   DesktopPath cherch�: $DesktopPath"
	if ( Test-Path -LiteralPath ($DesktopPath + "\" + $ShortcutName) -PathType Leaf ) {
		$ShortcutFullPath = $DesktopPath + "\" + $ShortcutName
		#Write-Output "   Raccourci trouv� ici: $ShortcutFullPath"
		break
	}
}

if ($ShortcutFullPath) {
    Write-Output "Raccourci trouv� ici: $ShortcutFullPath"

    $Shell = New-Object -ComObject ("WScript.Shell")
    $ShortCut = $Shell.CreateShortcut($ShortcutFullPath)
    $lnkPath = $ShortCut.TargetPath
    $lnkArgs = $ShortCut.Arguments

    # Exemple
    # C:\X4\K4.exe /APPLIC=KINE+4000 /REPFIC=C:\X4\DONNEE\PARDOC\BASE_X4 /POSTE=POSTEXXX

    Write-Output "lnkPath $lnkPath"
    Write-Output "lnkArgs $lnkArgs"

    # D�termine REPFIC
    $K4RepFic = ""
    $K4Poste = ""
    $lnkArgs -split "\s*(?=[-/])" | foreach {
        $name, $val = $_ -split "="
        if ($name -eq "/REPFIC") {
            $K4RepFic = $val
        }
        if ($name -eq "/POSTE") {
            $K4Poste = $val
        }
    }

    # D�termine le dossier d'installation de Kine4000
    $K4InstallDir=Split-Path -Path "$lnkPath" -Parent
    Write-Output "K4InstallDir $K4InstallDir"

    # Liste des chemins o� chercher l'analyse
    $Kine4000Pathes = @("$K4RepFic\ANA",
                    "$K4InstallDir\DONNEE",
                    $K4InstallDir,
                    "C:\X4",
                    "c:\Kine4000\X4"
    )
    # D�termine Fichier ANALYSE (on prend le 1er trouv�)
    :firstLoop foreach ($Kine4000Path in $Kine4000Pathes) {
        foreach ($AnalyseFileName in $AnalyseFileNames) {
            #Write-Output "$Kine4000Path"
            if ( Test-Path -LiteralPath $Kine4000Path ) {
                $K4AnalyseFileInfo = Get-ChildItem -Path "$Kine4000Path" -Include $AnalyseFileName -Recurse
                if ($K4AnalyseFileInfo) {
                    $K4Analyse = [string[]]($K4AnalyseFileInfo)
                    $K4AnalyseFirst = $K4Analyse[0]
                    break firstLoop
                } else {
                    Write-Output "Analyse Non trouv�e"
                }
            }
        }
    }

    # D�termine l'IP o� est accessible la BDD via HFSQL
    # Se fait � l'aide du fichier DONNEE/PARDOC/Base_X4/BddConnecte.ini [ "$K4RepFic\BddConnecte.ini" ]
    # On cherche la section [ConnexionSQL] et la ligne Source=192.168.1.220
    $PathToBddConnecteIni = "$K4RepFic\BddConnecte.ini"
    if (Test-Path $PathToBddConnecteIni) {
        $BddConnecteIni = Get-IniFile $PathToBddConnecteIni
        $HFSQL_SERVER_IP = $BddConnecteIni.ConnexionSQL.Source
        $HFSQL_SERVER_USER = $BddConnecteIni.ConnexionSQL.user
    }

    # D�termine le dossier qui contient r�ellement les fichiers
    # Cela peut �tre $K4RepFic si Kine4000 n'utilise pas HFSQL C/S
    # Cela peut �tre DONNEE/BDD/Base_X4 sinon
    # (on peut trouver le nom du r�pertoire ici C:\X4\DONNEE\HFConf.ini (section [INIT] / DBRootPath=C:\X4\DONNEE\BDD)
    # Ce fichier donne aussi le port via lequel acc�der � HFSQL C/S
    # "$K4RepFic\..\..\HFConf.ini"
    $PathToHFConfIni = "$K4RepFic\..\..\HFConf.ini"
    if (Test-Path $PathToHFConfIni) { #V�rifie si le fichier existe
        $HFConfPath = Convert-path $PathToHFConfIni
        $HFConfIni = Get-IniFile $HFConfPath
        # K4DBRootPath peut �tre par ex: C:\X4\DONNEE\BDD
        $K4DBRootPath = $HFConfIni.INIT.DBRootPath
        if ($K4DBRootPath -Match "%%EXE%%") {
            # Alors chercher o� est install� MANTA manager (cf https://doc.pcsoft.fr/?3044345)
            # et remplace %%EXE%% par le chemin o� est install� manta
            $MantaSearch = Get-ChildItem -Path "$K4InstallDir" -Include "manta.exe" -Recurse
            if ($MantaSearch.count -eq 1) {
                $K4DBRootPath = $K4DBRootPath -replace "%%EXE%%", $MantaSearch[0].DirectoryName
            } else {
                $K4DBRootPath = ""
            }
        } else {
            # Remplace la lettre de lecteur, par celle du .lnk vers Kine4000 (au cas o� on a d�plac� le dossier)
            $K4DBRootPath = $K4DBRootPath -replace "^.", $K4RepFic[0]
        }
        $HFSQL_SERVER_PORT = $HFConfIni.INIT.AccessPort
    }

    # D�termine le nom de la base de donn�es (pour l'instant fix� en dur � Base_X4)
    $K4_DBNAME = "Base_X4"

    if ($K4DBRootPath) {
        $K4RepFic = $K4DBRootPath + "\" + $K4_DBNAME
    }

    Write-Output ""
    Write-Output ""
    Write-Output "Pour un acc�s par HFSQL C/S:"
    Write-Output "----------------------------"
    Write-Output "HFSQL_SERVER_IP:   $HFSQL_SERVER_IP"
    Write-Output "HFSQL_SERVER_PORT: $HFSQL_SERVER_PORT"
    Write-Output "HFSQL_SERVER_USER: $HFSQL_SERVER_USER"
    Write-Output "HFSQL_DBNAME:      $K4_DBNAME"
    Write-Output ""
    Write-Output "Pour un acc�s par HFSQL Classic:"
    Write-Output "--------------------------------"
    Write-Output "REP:   $K4RepFic"
    Write-Output "ANA:   $K4AnalyseFirst"
    Write-Output "POSTE: $K4Poste"
    Write-Output ""

    # Met � jour le fichier de config de Kinetools pour Kine4000"
    $KTK4000ConfFile = $Args[0]
    Write-Output "Met � jour le fichier de config de Kinetools pour Kine4000"
    Write-Output "  (fichier: $KTK4000ConfFile)"

    (Get-Content $KTK4000ConfFile) -replace('^(set KINE4000_REP=).*', ('$1'+$K4RepFic)) | Set-Content $KTK4000ConfFile
    (Get-Content $KTK4000ConfFile) -replace('^(set KINE4000_ANA=).*', ('$1'+$K4AnalyseFirst)) | Set-Content $KTK4000ConfFile
    if ($HFSQL_SERVER_IP -and $HFSQL_SERVER_PORT) {
        (Get-Content $KTK4000ConfFile) -replace('^(?<name>set KINE4000_BDD_HOST=).*', ('${name}'+$HFSQL_SERVER_IP+":"+$HFSQL_SERVER_PORT)) | Set-Content $KTK4000ConfFile
    }
    if ($HFSQL_SERVER_USER) {
        (Get-Content $KTK4000ConfFile) -replace('^(set KINE4000_BDD_USER=).*', ('$1'+$HFSQL_SERVER_USER)) | Set-Content $KTK4000ConfFile
    }
    if ($K4_DBNAME) {
        (Get-Content $KTK4000ConfFile) -replace('^(set KINE4000_NOM_BASE=).*', ('$1'+$K4_DBNAME)) | Set-Content $KTK4000ConfFile
    }
} else {
    Write-Output "Raccourci vers l'installation de Kine4000 non trouv�. Y a-t-il bien une installation de Kine4000 sur ce syst�me ?"
}
