=== Remarques ===
Si on utilise Kinemax il faut s'assurer que PostgreSQL tourne avant d'envoyer un fichier via FTP ! 
Ceci est pris en compte par les scripts.

Modifier la base de donnée de Kinemax "CABINET" pour que kinetools puisse lire les informations utiles sans prendre le risque d'y écrire des informations par inadvertance.
Pour cela on ajoute l'utilisateur "kt"
et des vues pour kinetools (l'utilisateur kt peut créer des tables et modifier les views du schema kt).
Ainsi que l'utilisateur ktv (qui est en lecture seule sur kt & public)
Ceci est géré par les scripts.
