-- 
-- Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant � extraire des donn�es
-- de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
-- certaines t�ches de gestion. 
-- 
-- Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilit� au code source et des droits de copie,
-- de modification et de redistribution accord�s par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
-- seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les conc�dants successifs.
-- 
-- A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
-- associ�s au chargement,  � l'utilisation,  � la modification et/ou au
-- d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
-- donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
-- manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
-- avertis poss�dant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
-- logiciel � leurs besoins dans des conditions permettant d'assurer la
-- s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
-- � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
-- 
-- Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
-- termes.
-- 

-- psql -U postgres -p9154 -d CABINET < 02-*.sql

-- Pour le schema "public"
GRANT SELECT ON ALL TABLES IN SCHEMA public TO ktv;

GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO ktv;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
   GRANT SELECT ON TABLES TO ktv;

ALTER DEFAULT PRIVILEGES IN SCHEMA public
   GRANT SELECT ON SEQUENCES TO ktv;

-- Utile pour que "ktv" ne puisse pas cr�er de table
-- Attention, il faut bien mettre PUBLIC apr�s FROM et pas ktv, sinon ca ne marche pas
-- cf https://stackoverflow.com/questions/42949453/revoke-create-on-public-schema-from-user-is-not-working-in-postgresql
REVOKE CREATE ON SCHEMA public FROM PUBLIC;




-- Pour le schema "kt"
GRANT USAGE ON SCHEMA kt TO ktv;

GRANT SELECT ON ALL TABLES IN SCHEMA kt TO ktv;

GRANT SELECT ON ALL SEQUENCES IN SCHEMA kt TO ktv;

ALTER DEFAULT PRIVILEGES IN SCHEMA kt
   GRANT SELECT ON TABLES TO ktv;

ALTER DEFAULT PRIVILEGES IN SCHEMA kt
   GRANT SELECT ON SEQUENCES TO ktv;

-- Utile pour que "kt" ne puisse pas cr�er de table
--REVOKE CREATE ON SCHEMA kt FROM PUBLIC;
