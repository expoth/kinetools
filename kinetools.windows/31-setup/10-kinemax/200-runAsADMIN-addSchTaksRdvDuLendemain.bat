@REM 
@REM Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant à extraire des données
@REM de logiciels métiers pour kinésithérapeutes dans le but de faciliter
@REM certaines tâches de gestion. 
@REM 
@REM Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilité au code source et des droits de copie,
@REM de modification et de redistribution accordés par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
@REM seule une responsabilité restreinte pèse sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les concédants successifs.
@REM 
@REM A cet égard  l'attention de l'utilisateur est attirée sur les risques
@REM associés au chargement,  à l'utilisation,  à la modification et/ou au
@REM développement et à la reproduction du logiciel par l'utilisateur étant 
@REM donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
@REM manipuler et qui le réserve donc à des développeurs et des professionnels
@REM avertis possédant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
@REM logiciel à leurs besoins dans des conditions permettant d'assurer la
@REM sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
@REM à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
@REM 
@REM Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
@REM termes.
@REM 
@echo off

call "%~dp0\..\..\config\config.bat"

set BAT_NAME=502-rdv_du_lendemain_pour_alerte_sms.csv.kinemax.bat

SET "BATCH_TO_RUN=%SCRIPT_PATH%\%BAT_NAME%"
FOR /F "delims=" %%F IN ("%BATCH_TO_RUN%") DO SET "BATCH_TO_RUN=%%~fF"

SET ThisScriptsDirectory=%~dp0
SET PowerShellScriptPath=%ThisScriptsDirectory%..\15-schTasks\200-addSchTasksSoir.ps1

PowerShell -Version 3 -NoProfile -ExecutionPolicy Bypass -Command "& '%PowerShellScriptPath%' '%BATCH_TO_RUN%' '%LOG_PATH%'";

pause
