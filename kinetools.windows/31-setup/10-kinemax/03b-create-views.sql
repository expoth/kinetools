-- 
-- Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant � extraire des donn�es
-- de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
-- certaines t�ches de gestion. 
-- 
-- Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilit� au code source et des droits de copie,
-- de modification et de redistribution accord�s par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
-- seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les conc�dants successifs.
-- 
-- A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
-- associ�s au chargement,  � l'utilisation,  � la modification et/ou au
-- d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
-- donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
-- manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
-- avertis poss�dant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
-- logiciel � leurs besoins dans des conditions permettant d'assurer la
-- s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
-- � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
-- 
-- Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
-- termes.
-- 

-- psql -U kt -p9154 -d CABINET < 03-*.sql

DROP VIEW kt.rdv_du_lendemain_pour_alerte_sms;
DROP VIEW kt.rdv_du_jour_pour_alerte_sms_test;
DROP VIEW kt.rdv_du_jour_pour_alerte_sms;
DROP VIEW kt.prochains_rdv;
DROP VIEW agenda;
DROP VIEW patients_adr_seances_sans_tt;
DROP VIEW patients_adr_tt_seances;
DROP VIEW patients_adr_tt;
DROP VIEW fichiers;

--   __   ___               
--   \ \ / (_)_____ __ _____
--    \ V /| / -_) V  V (_-<
--     \_/ |_\___|\_/\_//__/
--                          
--- Nom des patients, avec adresse et traitement et s�ances
CREATE OR REPLACE VIEW patients_adr_tt_seances AS 
 SELECT itv_dossier_patient.dp_id,
    itv_fiche_admin.fa_id,
    itv_fiche_admin.fa_ben_titre,
    itv_fiche_admin.fa_ben_nom_usuel,
    itv_fiche_admin.fa_ben_nom_famille,
    itv_fiche_admin.fa_ben_prenom,
    itv_fiche_admin.fa_adresse,
    itv_fiche_admin.fa_adr_complement,
    itv_fiche_admin.fa_cp,
    itv_fiche_admin.fa_ville,
    itv_fiche_admin.fa_ben_tel1,
    itv_fiche_admin.fa_ben_tel2,
    itv_fiche_admin.fa_ass_titre,
    itv_fiche_admin.fa_ass_nom,
    itv_fiche_admin.fa_ass_prenom,
    itv_fiche_admin.fa_ass_tel1,
    itv_fiche_admin.fa_ass_tel2,
    itv_traitement.trt_id,
    itv_traitement.trt_cotation,
    itv_traitement.trt_date_presc,
    itv_traitement.trt_dp_id,
    itv_traitement.trt_ps_id,
    info_ps_tt.ips_nom AS ps_tt_nom,
    info_ps_tt.ips_prenom AS ps_tt_prenom,
    itv_serie.ser_id,
    itv_serie.ser_numero,
    itv_serie.ser_nb,
    itv_seance_serie.sse_etat,
    itv_seance_serie.sse_tarification,
    itv_seance.sea_cotation,
    itv_seance.sea_ordre_seance,
    itv_seance.sea_date,
    itv_seance.sea_heure,
    itv_seance.sea_etat, -- 0=NonPlanifi�e 1=Planifiee 2=A_Facturer 3=Annul�e
    itv_seance.sea_rappel_patient,
    itv_seance.sea_delai_rappel_pat,
    itv_seance.sea_a_domicile,
    itv_seance.sea_deplacement_non_facture,
    itv_seance.sea_ps_exec_id,
    info_ps_exec.ips_nom AS ps_exec_nom,
    info_ps_exec.ips_prenom AS ps_exec_prenom
   FROM itv_dossier_patient
     LEFT JOIN itv_fiche_admin ON itv_dossier_patient.dp_fa_id = itv_fiche_admin.fa_id
     LEFT JOIN itv_traitement ON itv_dossier_patient.dp_id = itv_traitement.trt_dp_id
     LEFT JOIN itv_infos_ps info_ps_tt ON itv_traitement.trt_ps_id = info_ps_tt.ips_id
     LEFT JOIN itv_serie ON itv_traitement.trt_id = itv_serie.ser_trt_id
     LEFT JOIN itv_seance_serie ON itv_seance_serie.sse_ser_id = itv_serie.ser_id
     LEFT JOIN itv_seance ON itv_seance_serie.sse_sea_id = itv_seance.sea_id
     LEFT JOIN itv_infos_ps info_ps_exec ON itv_seance.sea_ps_exec_id = info_ps_exec.ips_id
  --WHERE itv_traitement.trt_id = 111111
  --itv_fiche_admin.fa_ben_nom_usuel = 'NOM'
  ORDER BY itv_fiche_admin.fa_ben_nom_usuel, itv_fiche_admin.fa_ben_prenom, itv_seance.sea_date, itv_seance.sea_ordre_seance;


-- Nom des patients, avec adresse et traitement SANS s�ances
CREATE OR REPLACE VIEW patients_adr_tt AS 
 SELECT itv_dossier_patient.dp_id,
    itv_fiche_admin.fa_id,
    itv_fiche_admin.fa_ben_titre,
    itv_fiche_admin.fa_ben_nom_usuel,
    itv_fiche_admin.fa_ben_nom_famille,
    itv_fiche_admin.fa_ben_prenom,
    itv_fiche_admin.fa_adresse,
    itv_fiche_admin.fa_adr_complement,
    itv_fiche_admin.fa_cp,
    itv_fiche_admin.fa_ville,
    itv_fiche_admin.fa_ben_tel1,
    itv_fiche_admin.fa_ben_tel2,
    itv_fiche_admin.fa_ass_titre,
    itv_fiche_admin.fa_ass_nom,
    itv_fiche_admin.fa_ass_prenom,
    itv_fiche_admin.fa_ass_tel1,
    itv_fiche_admin.fa_ass_tel2,
    itv_traitement.trt_id,
    itv_traitement.trt_cotation,
    itv_traitement.trt_date_presc,
    itv_traitement.trt_dp_id,
    itv_traitement.trt_ps_id,
    info_ps_tt.ips_nom AS ps_tt_nom,
    info_ps_tt.ips_prenom AS ps_tt_prenom,
    itv_serie.ser_id,
    itv_serie.ser_numero,
    itv_serie.ser_nb
   FROM itv_dossier_patient
     LEFT JOIN itv_fiche_admin ON itv_dossier_patient.dp_fa_id = itv_fiche_admin.fa_id
     LEFT JOIN itv_traitement ON itv_dossier_patient.dp_id = itv_traitement.trt_dp_id
     LEFT JOIN itv_infos_ps info_ps_tt ON itv_traitement.trt_ps_id = info_ps_tt.ips_id
     LEFT JOIN itv_serie ON itv_traitement.trt_id = itv_serie.ser_trt_id
  ORDER BY itv_fiche_admin.fa_ben_nom_usuel,
           itv_fiche_admin.fa_ben_prenom;

-- S�ances qui n'ont pas de traitement (cas des 1ers rdv qui n'ont pas encore de ttt affect�)
CREATE OR REPLACE VIEW patients_adr_seances_sans_tt AS 
  SELECT itv_dossier_patient.dp_id,
    itv_fiche_admin.fa_id,
    itv_fiche_admin.fa_ben_titre,
    itv_fiche_admin.fa_ben_nom_usuel,
    itv_fiche_admin.fa_ben_nom_famille,
    itv_fiche_admin.fa_ben_prenom,
    itv_fiche_admin.fa_adresse,
    itv_fiche_admin.fa_adr_complement,
    itv_fiche_admin.fa_cp,
    itv_fiche_admin.fa_ville,
    itv_fiche_admin.fa_ben_tel1,
    itv_fiche_admin.fa_ben_tel2,
    itv_fiche_admin.fa_ass_titre,
    itv_fiche_admin.fa_ass_nom,
    itv_fiche_admin.fa_ass_prenom,
    itv_fiche_admin.fa_ass_tel1,
    itv_fiche_admin.fa_ass_tel2,
    0::bigint as trt_id, --itv_traitement.trt_id,
    NULL::text as trt_cotation, --itv_traitement.trt_cotation,
    NULL::date as trt_date_presc, --itv_traitement.trt_date_presc,
    NULL::bigint as trt_dp_id, --itv_traitement.trt_dp_id,
    NULL::bigint as trt_ps_id, --itv_traitement.trt_ps_id,
    NULL::character varying(50) as ps_tt_nom,--info_ps_tt.ips_nom AS ps_tt_nom,
    NULL::character varying(50) as ps_tt_prenom,--info_ps_tt.ips_prenom AS ps_tt_prenom,
    NULL::bigint as ser_id, --itv_serie.ser_id,
    NULL::integer as ser_numero, --itv_serie.ser_numero,
    NULL::integer as ser_nb, --itv_serie.ser_nb,
    NULL::integer as sse_etat, --itv_seance_serie.sse_etat,
    NULL::integer as sse_tarification, --itv_seance_serie.sse_tarification,
    itv_seance.sea_cotation,
    'Nouv.'::character varying(15), --itv_seance.sea_ordre_seance,
    itv_seance.sea_date,
    itv_seance.sea_heure,
    itv_seance.sea_etat,
    itv_seance.sea_rappel_patient,
    itv_seance.sea_delai_rappel_pat,
    itv_seance.sea_a_domicile,
    itv_seance.sea_deplacement_non_facture,
    itv_seance.sea_ps_exec_id,
    info_ps_exec.ips_nom AS ps_exec_nom,
    info_ps_exec.ips_prenom AS ps_exec_prenom
   FROM itv_seance
     JOIN itv_dossier_patient ON itv_seance.sea_fse_id = itv_dossier_patient.dp_id
     LEFT JOIN itv_fiche_admin ON itv_dossier_patient.dp_fa_id = itv_fiche_admin.fa_id
     --LEFT JOIN itv_traitement ON itv_seance.sea_trt_id = itv_traitement.trt_id
     --LEFT JOIN itv_infos_ps info_ps_tt ON itv_traitement.trt_ps_id = info_ps_tt.ips_id
     --LEFT OUTER JOIN itv_seance_serie ON itv_seance_serie.sse_sea_id = itv_seance.sea_id
     --LEFT OUTER JOIN itv_serie ON itv_serie.ser_id = itv_seance_serie.sse_ser_id
     LEFT JOIN itv_infos_ps info_ps_exec ON itv_seance.sea_ps_exec_id = info_ps_exec.ips_id
   WHERE itv_seance.sea_trt_id = 0;


CREATE OR REPLACE VIEW agenda AS 
  SELECT * FROM patients_adr_tt_seances
  UNION
  SELECT * FROM patients_adr_seances_sans_tt
  ORDER BY sea_heure, sea_date;

-- Documents stock�s (ordo etc) avec nom des patients, du fichier et date.
CREATE OR REPLACE VIEW fichiers AS 
 SELECT itv_document.doc_id,
    itv_document.doc_donnee,
    itv_document.doc_nom_fichier,
    itv_document.doc_date,
    itv_donnees_medic.dom_id,
    itv_donnees_medic.dom_date,
    itv_donnees_medic.dom_libelle,
    dpdom.dp_id AS dp_iddom,
    fadom.fa_id AS fa_iddom,
    fadom.fa_ben_nom_usuel AS fa_ben_nom_usueldom,
    fadom.fa_ben_nom_famille AS fa_ben_nom_familledom,
    fadom.fa_ben_prenom AS fa_ben_prenom_familledom,
    itv_echange.ech_id,
    itv_echange.ech_datetime,
    itv_echange.ech_libelle,
    itv_echange.ech_dp_id,
    dpech.dp_id AS dp_idech,
    faech.fa_id AS fa_idech,
    faech.fa_ben_nom_usuel AS fa_ben_nom_usuelech,
    faech.fa_ben_nom_famille AS fa_ben_nom_familleech,
    faech.fa_ben_prenom AS fa_ben_prenom_familleech
   FROM itv_document
     LEFT JOIN itv_donnees_medic ON itv_document.doc_dom_id = itv_donnees_medic.dom_id
     LEFT JOIN itv_dossier_patient dpdom ON itv_donnees_medic.dom_dp_id = dpdom.dp_id
     LEFT JOIN itv_fiche_admin fadom ON dpdom.dp_fa_id = fadom.fa_id
     LEFT JOIN itv_echange ON itv_document.doc_ech_id = itv_echange.ech_id
     LEFT JOIN itv_dossier_patient dpech ON itv_echange.ech_dp_id = dpech.dp_id
     LEFT JOIN itv_fiche_admin faech ON dpech.dp_fa_id = faech.fa_id
LIMIT 20;

-- Les prochains rendez-vous pour un dossier patient (sans distinction du traitement sur lequel est mis le rdv)
-- On ne prend pas ceux ayant sea_etat = 3 car 3=Annul�e
CREATE OR REPLACE VIEW kt.prochains_rdv AS 
 SELECT base36_encode(agenda.dp_id) as dp_id_base64,
    agenda.sea_date,
    to_char(agenda.sea_heure::interval, 'HH24:MI'::text) AS sea_heure_hhmm
   FROM agenda
  WHERE agenda.sea_date >= CURRENT_DATE
    AND agenda.sea_etat != 3
  ORDER BY  agenda.dp_id, 
            agenda.sea_date, 
            (to_char(agenda.sea_heure::interval, 'HH24:MI'::text));

  
-- Patients qui ont un rdv ce jour pour leur envoyer un SMS de rappel
-- Si un patient refuse, alors mettre un caract�re suppl�mentaire (- ou ! ou peu ) au d�but de ses num�ros mobile
-- Ainsi il ne sortira pas dans cette liste
--
-- Message de kinemax:
-- "Veuillez ne pas oublier votre rendez-vous du [DATE_RDV] � [HEURE_RDV] avec [NOM_PS].".
-- Retrouvez vos prochains rdv sur https://mon.rdvkine.com/<dp_id>. Ceci est un SMS automatique, les r�ponses ne sont pas consult�es.
--
CREATE OR REPLACE VIEW kt.rdv_du_jour_pour_alerte_sms AS 
 SELECT agenda.dp_id,
    base36_encode(agenda.dp_id) as dp_id_base64,
    agenda.fa_id,
    agenda.fa_ben_titre,
    agenda.fa_ben_nom_usuel,
    agenda.fa_ben_nom_famille,
    agenda.fa_ben_prenom,
    agenda.fa_ben_tel1,
    agenda.fa_ben_tel2,
        CASE
            WHEN agenda.fa_ben_tel1::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text THEN regexp_replace(agenda.fa_ben_tel1::text, '(0(6|7))[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)$'::text, '\1\3\4\5\6'::text, 'g'::text)
            ELSE
            CASE
                WHEN agenda.fa_ben_tel2::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text THEN regexp_replace(agenda.fa_ben_tel2::text, '(0(6|7))[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)$'::text, '\1\3\4\5\6'::text, 'g'::text)
                ELSE ''::text
            END
        END AS mobile,
    agenda.sea_date,
    to_char(agenda.sea_heure::interval, 'HH24:MI'::text) AS sea_heure_hhmm,
    agenda.sea_etat,  -- 0=NonPlanifi�e 1=Planifiee 2=A_Facturer 3=Annul�e
    agenda.sea_rappel_patient,
    agenda.sea_delai_rappel_pat,
    agenda.ps_exec_nom,
    agenda.ps_exec_prenom
   FROM kt.agenda
  WHERE agenda.sea_date = CURRENT_DATE 
    AND agenda.sea_etat != 3
    AND (agenda.fa_ben_tel1::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text OR agenda.fa_ben_tel2::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text);
  
  
-- VERSION TEST
CREATE OR REPLACE VIEW kt.rdv_du_jour_pour_alerte_sms_test AS 
 SELECT agenda.dp_id,
    base36_encode(agenda.dp_id) as dp_id_base64,
    agenda.fa_id,
    agenda.fa_ben_titre,
    agenda.fa_ben_nom_usuel,
    agenda.fa_ben_nom_famille,
    agenda.fa_ben_prenom,
    agenda.fa_ben_tel1,
    agenda.fa_ben_tel2,
        CASE
            WHEN agenda.fa_ben_tel1::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text THEN regexp_replace(agenda.fa_ben_tel1::text, '(0(6|7))[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)$'::text, '\1\3\4\5\6'::text, 'g'::text)
            ELSE
            CASE
                WHEN agenda.fa_ben_tel2::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text THEN regexp_replace(agenda.fa_ben_tel2::text, '(0(6|7))[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)$'::text, '\1\3\4\5\6'::text, 'g'::text)
                ELSE ''::text
            END
        END AS mobile,
    agenda.sea_date,
    to_char(agenda.sea_heure::interval, 'HH24:MI'::text) AS sea_heure_hhmm,
    agenda.sea_etat,  -- 0=NonPlanifi�e 1=Planifiee 2=A_Facturer 3=Annul�e
    agenda.sea_rappel_patient,
    agenda.sea_delai_rappel_pat,
    agenda.ps_exec_nom,
    agenda.ps_exec_prenom
   FROM agenda
  WHERE agenda.sea_date >= '2019-12-01' 
    AND agenda.sea_etat != 3
    AND (agenda.fa_ben_tel1::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text OR agenda.fa_ben_tel2::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text);

-- Patients qui ont un rdv le lendemain (ou le lundi suivant si on est vendredi ou samedi)pour leur envoyer un SMS de rappel
-- Si un patient refuse, alors mettre un caract�re suppl�mentaire (- ou ! ou peu ) au d�but de ses num�ros mobile
-- Ainsi il ne sortira pas dans cette liste
--
-- Message de kinemax:
-- "Veuillez ne pas oublier votre rendez-vous du [DATE_RDV] � [HEURE_RDV] avec [NOM_PS].".
-- Retrouvez vos prochains rdv sur https://mon.rdvkine.com/<dp_id>. Ceci est un SMS automatique, les r�ponses ne sont pas consult�es.
--
CREATE OR REPLACE VIEW kt.rdv_du_lendemain_pour_alerte_sms AS 
 SELECT agenda.dp_id,
    base36_encode(agenda.dp_id) as dp_id_base64,
    agenda.fa_id,
    agenda.fa_ben_titre,
    agenda.fa_ben_nom_usuel,
    agenda.fa_ben_nom_famille,
    agenda.fa_ben_prenom,
    agenda.fa_ben_tel1,
    agenda.fa_ben_tel2,
        CASE
            WHEN agenda.fa_ben_tel1::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text THEN regexp_replace(agenda.fa_ben_tel1::text, '(0(6|7))[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)$'::text, '\1\3\4\5\6'::text, 'g'::text)
            ELSE
            CASE
                WHEN agenda.fa_ben_tel2::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text THEN regexp_replace(agenda.fa_ben_tel2::text, '(0(6|7))[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)[- \.](\d\d)$'::text, '\1\3\4\5\6'::text, 'g'::text)
                ELSE ''::text
            END
        END AS mobile,
    agenda.sea_date,
    to_char(agenda.sea_heure::interval, 'HH24:MI'::text) AS sea_heure_hhmm,
    agenda.sea_etat,  -- 0=NonPlanifi�e 1=Planifiee 2=A_Facturer 3=Annul�e
    agenda.sea_rappel_patient,
    agenda.sea_delai_rappel_pat,
    agenda.ps_exec_nom,
    agenda.ps_exec_prenom
   FROM kt.agenda
  WHERE agenda.sea_date = 
        -- Date du lendemain, sauf si vendredi ou samedi, alors date du lundi
        -- Je consid�re le samedi comme non travaill�.
        (select case when (select extract(dow from current_date) = 5) then current_date+3
        else case when (select extract(dow from current_date) = 6) then current_date+2
        else current_date +1
        end
        end as lendemain)
    AND agenda.sea_etat != 3
    AND (agenda.fa_ben_tel1::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text OR agenda.fa_ben_tel2::text ~ '^0(6|7)[- \.]\d\d[- \.]\d\d[- \.]\d\d[- \.]\d\d$'::text);
  
