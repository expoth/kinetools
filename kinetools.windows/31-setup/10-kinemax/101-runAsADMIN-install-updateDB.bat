@REM 
@REM Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant à extraire des données
@REM de logiciels métiers pour kinésithérapeutes dans le but de faciliter
@REM certaines tâches de gestion. 
@REM 
@REM Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilité au code source et des droits de copie,
@REM de modification et de redistribution accordés par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
@REM seule une responsabilité restreinte pèse sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les concédants successifs.
@REM 
@REM A cet égard  l'attention de l'utilisateur est attirée sur les risques
@REM associés au chargement,  à l'utilisation,  à la modification et/ou au
@REM développement et à la reproduction du logiciel par l'utilisateur étant 
@REM donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
@REM manipuler et qui le réserve donc à des développeurs et des professionnels
@REM avertis possédant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
@REM logiciel à leurs besoins dans des conditions permettant d'assurer la
@REM sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
@REM à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
@REM 
@REM Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
@REM termes.
@REM 
@echo off

cd /D "%~dp0"

call "%~dp0\..\..\config\config.bat"

if not defined LOGICMAX_BASEDIR (
	echo "Installation de kinemax non trouve. Abandon."
	pause
	exit
)

net stop %PGSQL_KINEMAX_SERVICE_NAME%
REM Au préalable, mettre "trust" au lieu de "md5" dans "%PGSQLDIR%"\data\pg_hba.conf
icacls "%PGSQLDIR%"\data\pg_hba.conf /save perms.txt
"%UnxUtils_PATH%\sed.exe" -i.bak -e "/host[[:space:]]\+all[[:space:]]\+all[[:space:]]\+/ s/md5/trust/" "%PGSQLDIR%"\data\pg_hba.conf
icacls "%PGSQLDIR%"\data\ /restore perms.txt
net start %PGSQL_KINEMAX_SERVICE_NAME%


COPY 01-create_user_kt.sql.in 01-create_user_kt.sql
"%UnxUtils_PATH%\sed.exe" -i -e "s/@PASSWORD_KT@/%PASSWORD_KT%/" 01-create_user_kt.sql
"%UnxUtils_PATH%\unix2dos.exe" 01-create_user_kt.sql

COPY 04-create_user_ktv.sql.in 04-create_user_ktv.sql
"%UnxUtils_PATH%\sed.exe" -i -e "s/@PASSWORD_KTV@/%PASSWORD_KTV%/" 04-create_user_ktv.sql
"%UnxUtils_PATH%\unix2dos.exe" 04-create_user_ktv.sql

REM Apply Changes to Database & Cluster

"%PGSQLDIR%"\bin\psql -U postgres       -p %DB_PORT%                < 01-create_user_kt.sql
"%PGSQLDIR%"\bin\psql -U postgres       -p %DB_PORT% -d %DB_NAME%   < 02-create_kt_privileges_and_schema.sql

"%PGSQLDIR%"\bin\psql -U kt             -p %DB_PORT% -d %DB_NAME%   < 03a-base36_encode_decode.sql
"%PGSQLDIR%"\bin\psql -U kt             -p %DB_PORT% -d %DB_NAME%   < 03b-create-views.sql

"%PGSQLDIR%"\bin\psql -U postgres       -p %DB_PORT% -d %DB_NAME%   < 04-create_user_ktv.sql
"%PGSQLDIR%"\bin\psql -U postgres       -p %DB_PORT% -d %DB_NAME%   < 05-create_ktv_privileges_and_schema.sql

COPY ..\..\config\pgpass.conf.in ..\..\config\pgpass.conf
"%UnxUtils_PATH%\sed.exe" -i -e "s/@PASSWORD_KTV@/%PASSWORD_KTV%/" ..\..\config\pgpass.conf
"%UnxUtils_PATH%\unix2dos.exe" ..\..\config\pgpass.conf

net stop %PGSQL_KINEMAX_SERVICE_NAME%
REM Remettre "md5" dans "%PGSQLDIR%"\data\pg_hba.conf
icacls "%PGSQLDIR%"\data\pg_hba.conf /save perms.txt
"%UnxUtils_PATH%\sed.exe" -i -e "/host[[:space:]]\+all[[:space:]]\+all[[:space:]]\+/ s/trust/md5/" "%PGSQLDIR%"\data\pg_hba.conf
"%UnxUtils_PATH%\unix2dos.exe" "%PGSQLDIR%"\data\pg_hba.conf
icacls "%PGSQLDIR%"\data\ /restore perms.txt
net start %PGSQL_KINEMAX_SERVICE_NAME%

del 01-create_user_kt.sql
del 04-create_user_ktv.sql

PAUSE
