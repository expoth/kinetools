@REM 
@REM Copyright ou � Fab Stz <fabstz-it@yahoo.fr>, (2020)
@REM 
@REM Ce fichier fait partie du logiciel KineTools Suite.
@REM 
@REM Ce logiciel est un programme informatique servant � extraire des donn�es
@REM de logiciels m�tiers pour kin�sith�rapeutes dans le but de faciliter
@REM certaines t�ches de gestion. 
@REM 
@REM Ce logiciel est r�gi par la licence CeCILL-C soumise au droit fran�ais et
@REM respectant les principes de diffusion des logiciels libres. Vous pouvez
@REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
@REM de la licence CeCILL-C telle que diffus�e par le CEA, le CNRS et l'INRIA 
@REM sur le site "http://www.cecill.info".
@REM 
@REM En contrepartie de l'accessibilit� au code source et des droits de copie,
@REM de modification et de redistribution accord�s par cette licence, il n'est
@REM offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
@REM seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
@REM titulaire des droits patrimoniaux et les conc�dants successifs.
@REM 
@REM A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
@REM associ�s au chargement,  � l'utilisation,  � la modification et/ou au
@REM d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
@REM donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
@REM manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
@REM avertis poss�dant  des  connaissances  informatiques approfondies.  Les
@REM utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
@REM logiciel � leurs besoins dans des conditions permettant d'assurer la
@REM s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
@REM � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 
@REM 
@REM Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
@REM pris connaissance de la licence CeCILL-C, et que vous en avez accept� les
@REM termes.
@REM 
@echo off

cd /D "%~dp0"
call "%~dp0\..\..\config\config.bat"

REM Installe silencieusement VCRedist2019
REM copy ..\..\07-packages-httpd-php\vcredist2019_x86.exe %TEMP%
REM %TEMP%\vcredist2019_x86.exe /install /quiet /norestart 
REM del %TEMP%\vcredist2019_x86.exe


REM Unzip
..\..\13-7-ZipPortable\App\7-Zip\7z.exe x -y -o..\..\20-httpd-php ..\..\07-packages-httpd-php\httpd-2.4.41-win32-VS16.zip Apache24/
..\..\13-7-ZipPortable\App\7-Zip\7z.exe x -y -o..\..\20-httpd-php\php7 ..\..\07-packages-httpd-php\php-7.3-Win32-VC15-x86-latest.zip

REM Setup APACHE
@setlocal enableextensions enabledelayedexpansion

REM FOR /F "USEBACKQ tokens=*" %%g IN (`echo %USERPROFILE% ^| "%UnxUtils_PATH%\sed.exe" -e "s_\\_/_g"`) do (SET "MY_USERPROFILE=%%g")
FOR /F "USEBACKQ tokens=*" %%g IN (`echo %TOOLDIR% ^| "%UnxUtils_PATH%\sed.exe" -e "s_\\_/_g"`) do (SET "KINETOOLS_BASEDIR=%%g")
REM Enl�ve le dernier caract�re de KINETOOLS_BASEDIR qui est un espace qu'on ne veut pas
set KINETOOLS_BASEDIR=!KINETOOLS_BASEDIR:~0,-1!

REM Adapte httpd.conf
"%UnxUtils_PATH%\sed.exe" -i.bak -e "s�\"c:/Apache24\"�\"%KINETOOLS_BASEDIR%/20-httpd-php/Apache24\"�" ..\..\20-httpd-php\Apache24\conf\httpd.conf
"%UnxUtils_PATH%\sed.exe" -i -e "s�^Listen 80$�Listen 8000�" ..\..\20-httpd-php\Apache24\conf\httpd.conf

REM Copie et adapte kinetools-intranet.conf
COPY kinetools-intranet.conf.in ..\..\20-httpd-php\Apache24\conf\kinetools-intranet.conf
"%UnxUtils_PATH%\sed.exe" -i -e "s�\"INTRANET_BASE_DIR\"�\"%KINETOOLS_BASEDIR%/20-httpd-php/intranet\"�" ..\..\20-httpd-php\Apache24\conf\kinetools-intranet.conf

REM Add line into httpd.conf to include kinetools-intranet.conf
>nul find "Include conf/kinetools-intranet.conf" ..\..\20-httpd-php\Apache24\conf\httpd.conf && (
  echo already in file. skipping...
) || (
  echo Include conf/kinetools-intranet.conf>> ..\..\20-httpd-php\Apache24\conf\httpd.conf
)


REM SETUP PHP
REM Adapte php.ini
COPY ..\..\20-httpd-php\php7\php.ini-development ..\..\20-httpd-php\php7\php.ini
"%UnxUtils_PATH%\sed" -i -e "s�^;extension_dir = \"ext\"$�extension_dir = \"%KINETOOLS_BASEDIR%/20-httpd-php/php7/ext\"�" ..\..\20-httpd-php\php7\php.ini
"%UnxUtils_PATH%\sed" -i -e "s�^;extension=curl$�extension=curl�" ..\..\20-httpd-php\php7\php.ini
"%UnxUtils_PATH%\sed" -i -e "s�^;date.timezone =$�date.timezone = Europe/Paris�" ..\..\20-httpd-php\php7\php.ini

REM Copie et adapte mod_php.conf
COPY mod_php.conf.in ..\..\20-httpd-php\Apache24\conf\mod_php.conf
"%UnxUtils_PATH%\sed.exe" -i -e "s�PHP_PATH�%KINETOOLS_BASEDIR%/20-httpd-php/php7�" ..\..\20-httpd-php\Apache24\conf\mod_php.conf

REM Adapte httpd.conf
"%UnxUtils_PATH%\sed" -i -e "s�DirectoryIndex index.html$�DirectoryIndex index.html index.php�" ..\..\20-httpd-php\Apache24\conf\httpd.conf
>nul find "Include conf/mod_php.conf" ..\..\20-httpd-php\Apache24\conf\httpd.conf && (
  echo already in file. skipping...
) || (
  echo Include conf/mod_php.conf>> ..\..\20-httpd-php\Apache24\conf\httpd.conf
)


endlocal

"%UnxUtils_PATH%\unix2dos" ..\..\20-httpd-php\Apache24\conf\httpd.conf
"%UnxUtils_PATH%\unix2dos" ..\..\20-httpd-php\Apache24\conf\kinetools-intranet.conf
"%UnxUtils_PATH%\unix2dos" ..\..\20-httpd-php\Apache24\conf\mod_php.conf
"%UnxUtils_PATH%\unix2dos" ..\..\20-httpd-php\php7\php.ini

pause
