
==== Installation gammu-smsd ====

# Installer Gammu
aptitude install gammu gammu-smsd

# D'abord installer postgresql:
aptitude install postgresql

# Définir le mot de passe pour smsd dans le fichier 05-createUser.sql
vim setup.gammu-smsd/05-createUser.sql

# Entrer dans psql & lancer les commandes SQL du fichier 05-createUser.sql
# pour créer l'utilisateur et la base de données
su -c 'psql < setup.gammu-smsd/05-createUser.sql' postgres

# Créer la structure de base de données: (1 erreur attendue sur CREATE LANGUAGE pgplsql car existe déjà) 
# et les tables pour Gammu-smsd
gunzip -c /usr/share/doc/gammu-smsd/examples/pgsql.sql.gz | psql -U smsd -h localhost smsd 

# Paramétrer le fichier /etc/gammu-smsdrc
# Je désactive la réception des SMS car ils se font tout de suite manger
# par wammu et ne restent pas sur le téléphone
cp setup.gammu-smsd/gammu-smsdrc /etc/gammu-smsdrc
chmod 600 /etc/gammu-smsdrc #Car il stocke un mot de passe
chown root:root /etc/gammu-smsdrc

# Modifier le mot de passe dans /etc/gammu-smsdrc
# Et eventuellement adapter le paramétrage
vim /etc/gammu-smsdrc

# Puis relancer le daemon gammu-smd
systemctl restart gammu-smsd.service

==== Configuration ====

Dans le fichier /etc/gammu-smsdrc :

* Besoin de renseigner le code PIN la SIM
pin = xxxx

* Besoin de renseigner le n° SMSC (à récupérer de la SIM)
smsc = +336xxxx

Numéros de centre de messagerie (SMSC)
- Orange, Breizh Mobile, Virgin Mobile, M6 Mobile : +33689004000
- Bouygues Telecom, Universal Mobile : +33660660001 ou +33660003000
- SFR, Debitel, Nrj Mobile, Coriolis : +33609001390
- Free Mobile : +33695000695

* Ajouter
; MaxRetries: How many times will SMSD try to resend message if sending fails. This is tracked per message and currently supported only with SQL backends.
; (default = 1)
MaxRetries = 5

; RetryTimeout: How long to wait before resending failed message (needs to be enabled by MaxRetries).
; (default = 600=10min)
RetryTimeout = 300

==== Utilisation de gammu-smsd ====

# L'envoi d'un message simple se fait avec la commande suivante 
# (-autolen 2000 pour qu'il y ait assez de caratères pris en compte pour l'envoi 
# d'un message et qu'il soit bien reçu en SMS multiples fusionnés)

gammu-smsd-inject TEXT <TEL> -text "le texte du SMS" -autolen 2000
