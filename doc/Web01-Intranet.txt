
==== Intranet ====

* L'intranet propose :
    - Une page affichant l'agenda extrait depuis le logiciel métier
    - Une page permettant d'injecter des SMS de confirmation de demande de rdv dans la passerelle de SMS (gammu-smsd)
    - Une page de configuration de l'envoi des SMS (texte, activation...)

* La page affichant l'agenda :
    - utilise le fichier extrait depuis le logiciel métier et qui doit être placé dans le dossier "data"
    - Permet de sélectionner les créneaux libres puis de les publier vers l'extranet
        Rem: Le fait de publier vers l'extranet remplace tous les créneaux de la semaine qui sont déjà sur l'extranet
        Donc pour enlever tous les rdv de la semaine sur l'extranet, il faut déselectionner tous les crénaux et publier.

* La page d'injection des SMS dans Gammu-SMSd utilise au choix:
    - l'envoi via Kalkun. Il sera alors demandé de se logguer à kalkun
    - l'envoi via gammu-smsd-inject. Il sera alors demandé un code

* La page pour configurer le texte des messages SMS d'alertes rendez-vous (texte par défaut, selon l'utilisateur, activer/désactiver l'envoi des SMS...)

* Prérequis
    - Serveur Web Apache & PHP (de préférence PHP > 7)
    - Besoin de l'extension php "php-curl" (pour envoyer (depuis l'intranet) les rdv disponibles vers l'extranet)
    - cf KineTools16-VM-configApacheIntranet.txt pour configurer le serveur

* Configuration:
    - Elle se fait à l'aide des fichiers dans les dossiers "config" & "config_local"
    - Copier les fichiers d'extension ".in" vers des fichiers de même nom, sans l'extension ".in" puis les adapter selon les indications ci-dessous.
    
    - alertes_sms.ini (dans le dossier "config_local")
        - S'assurer qu'il a les bonnes permissions (en l'occurence l'utilisateur du daemon httpd (ie www-data sous debian) doit pouvoir écrire dedans, pour que le fichier puisse être modifié par l'intranet)
        - Contient les modèles de messages et la configuration de l'envoi des alertes SMS par les scripts KineTools pour linux
        - Ce fichier est généré et mis à jour par la page de configuration "alertes_sms.php"
        - Il est lu par les scripts KineTools pour linux (script python: alerte_sms.py)
    - config.php
        - Renseigner les URL pour l'extranet et kalkun
        - 'secret' : Mettre un mot de passe qui doit être le même que dans la config de l'extranet. Il permet d'authentifier les échanges entre l'intranet et l'extranet
        - 'suppr_seance_avant_auj' : Permet de dire qu'on doit supprimer les séances passées lorsqu'on publie sur l'extranet
        - 'smsd-inject-config-mode' : Mettre à "true" pour configurer. 
            Aller ensuite sur la page "api/smsd-inject.php"
            Suivre les instructions sur la page
            On met alors à jour les autres paramètres 'smsd-inject... (ts, nonce, hash)' avec ce qui est indiqué
        - 'smsd-inject-agent' : Pour dire si on veut utiliser gammu-smsd-inject ou kalkun pour mettre les SMS dans la file d'attente d'envoi.
        - 'agenda_code_th_suppl' : ajoute des thérapeutes supplémentaires dans la vue agenda qui ne sont pas dans l'export du logiciel métier. C'est utile si ces thérapeutes veulent quand même publier les créneaux libres sur l'extranet
        - les autres valeurs s'auto-expliquent
    - therapeute.csv
        le contenu attendu est décrit dans la 1ère ligne, et la 2e ligne est un exemple)

