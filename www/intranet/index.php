<!DOCTYPE html>
<?php
$configs = include($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/config/config.php');
?>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Intranet - KineTools</title>
    <style>
        @import url(<?php echo dirname($_SERVER['REQUEST_URI']) ?>/css/main-intranet.css);
        form, form input[type=submit] { display:inline; color:darkblue}
    </style>
</head>

<body>
    <h1>Intranet - KineTools</h1>
    <p><a href="agenda.php">Agenda (vue thérapeute)</a></p>
    <p><a href="api/sms/smsd-inject.php?<?php echo $configs['smsd-inject-query_string']; ?>">Envoi de SMS</a></p>
    <p>&nbsp;
    <p><a href="alertes_sms.php">Paramétrage des alertes SMS</a></p>
    <p>&nbsp;
    <footer>
    </footer>
</body>

</html>
