<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */

// Lun:1 Mar:2...
function jour_de_semaine($date)
{
setlocale(LC_ALL,'fr_FR.UTF-8');

//Convert the date string into a unix timestamp.
$unixTimestamp = strtotime($date);
 
//Get the day of the week using PHP's date function.
// https://www.php.net/manual/fr/function.date.php
# 1 (pour Lundi) à 7 (pour Dimanche)
$dayOfWeek = strftime("%u", $unixTimestamp);

    //Print out the day that our date fell on.
    return $dayOfWeek;
}

// Retourne: lun. mar. mer. ...
function jour_de_semaine_lun($date)
{
setlocale(LC_ALL,'fr_FR.UTF-8');

//Convert the date string into a unix timestamp.
$unixTimestamp = strtotime($date);
 
//Get the day of the week using PHP's date function.
// https://www.php.net/manual/fr/function.date.php

$dayOfWeek = strftime("%a", $unixTimestamp);

    //Print out the day that our date fell on.
    return $dayOfWeek;
}

// Retourne: lun. mar. mer. ...
function formate($date)
{
setlocale(LC_ALL,'fr_FR.UTF-8');

//Convert the date string into a unix timestamp.
$unixTimestamp = strtotime($date);
 
//Get the day of the week using PHP's date function.
// https://www.php.net/manual/fr/function.date.php

$dayOfWeek = strftime("%a %d %b %Y", $unixTimestamp);

    //Print out the day that our date fell on.
    return $dayOfWeek;
}

function getStartAndEndDate($year, $week)
{
   return [
      (new DateTime())->setISODate($year, $week)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week, 7)->format('Y-m-d') //end date
   ];
}

function getLunVenDates($year, $week)
{
   return [
      (new DateTime())->setISODate($year, $week)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,2)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,3)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,4)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,5)->format('Y-m-d') //start date
   ];
}

function getLunSamDates($year, $week)
{
   return [
      (new DateTime())->setISODate($year, $week)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,2)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,3)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,4)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,5)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,6)->format('Y-m-d') //start date
   ];
}

function getTodayYYYYMMAA()
{
   return (new DateTime())->format('Y-m-d');
}

function getYesterdayYYYYMMAA()
{
   return date('Y-m-d', time() - 60 * 60 * 24);
}

function getYYYYWWplusDays ($days_shift = 0) {
    return date('Y-W', time() + $days_shift * 60 * 60 * 24);
}

function getYYYYWWofTimestamp ($ts) {
    return date('Y-W', $ts);
}

// Retourne maintenant au format YYYY-MM-AA_HH:MM
// en ajoutant $addition (min) si renseigné.
function getNowYYYYMMAA_HHMM($addition_minutes = 0)
{
    if ($addition_minutes != 0)
        return (new DateTime())->format('Y-m-d_H:i');
    else
        return date('Y-m-d_H:i', time() + $addition_minutes * 60);
}

// Retourne maintenant au format YYYY-MM-AA_HH:MM
function getNowHHMM()
{
   return (new DateTime())->format('H:i');
}

function tel_est_mobile($num) {
    $tmp_num = tel_sans_caract_speciaux($num);
    //echo $tmp_num;
    if ( (strlen($tmp_num) == 10 and preg_match('/^0[6|7]/',$tmp_num) ) or
         (strlen($tmp_num) == 12 and preg_match('/^\+33[6|7]/',$tmp_num) ) or 
         (strlen($tmp_num) == 13 and preg_match('/^0033[6|7]/',$tmp_num) ) ) {
        //echo "\n$num est mobile ($tmp_num)\n";
        return true;
    } else {
        //echo "\n$num est PAS mobile ($tmp_num)\n";
        return false;
    }
}

function tel_sans_caract_speciaux($str) {
    return preg_replace('/[^0-9\+]/', '', $str);
}

// Fonction récupérée ici : https://stackoverflow.com/a/48433241
// Avec ajout de addcslashes en plus
if (!function_exists('write_ini_file')) {
    /**
     * Write an ini configuration file
     * 
     * @param string $file
     * @param array  $array
     * @return bool
     */
    function write_ini_file($file, $array = []) {
        // check first argument is string
        if (!is_string($file)) {
            throw new \InvalidArgumentException('Function argument 1 must be a string.');
        }

        // check second argument is array
        if (!is_array($array)) {
            throw new \InvalidArgumentException('Function argument 2 must be an array.');
        }

        // process array
        $data = array();
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $data[] = "[$key]";
                foreach ($val as $skey => $sval) {
                    if (is_array($sval)) {
                        foreach ($sval as $_skey => $_sval) {
                            if (is_numeric($_skey)) {
                                $data[] = $skey.'[] = '.(is_numeric($_sval) ? $_sval : (ctype_upper($_sval) ? addcslashes($_sval,'"') : '"'.addcslashes($_sval,'"').'"'));
                            } else {
                                $data[] = $skey.'['.$_skey.'] = '.(is_numeric($_sval) ? $_sval : (ctype_upper($_sval) ? addcslashes($_sval,'"') : '"'.addcslashes($_sval,'"').'"'));
                            }
                        }
                    } else {
                        $data[] = $skey.' = '.(is_numeric($sval) ? $sval : (ctype_upper($sval) ? addcslashes($sval,'"') : '"'.addcslashes($sval,'"').'"'));
                    }
                }
            } else {
                $data[] = $key.' = '.(is_numeric($val) ? $val : (ctype_upper($val) ? addcslashes($val,'"') : '"'.addcslashes($val,'"').'"'));
            }
            // empty line
            $data[] = null;
        }

        // open file pointer, init flock options
        $fp = fopen($file, 'w');
        $retries = 0;
        $max_retries = 100;

        if (!$fp) {
            return false;
        }

        // loop until get lock, or reach max retries
        do {
            if ($retries > 0) {
                usleep(rand(1, 5000));
            }
            $retries += 1;
        } while (!flock($fp, LOCK_EX) && $retries <= $max_retries);

        // couldn't get the lock
        if ($retries == $max_retries) {
            return false;
        }

        // got lock, write data
        fwrite($fp, implode(PHP_EOL, $data).PHP_EOL);

        // release lock
        flock($fp, LOCK_UN);
        fclose($fp);

        return true;
    }
}

function readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    if ($file_handle == false) {
        die ("<p><br>Le fichier n'a pas pu être ouvert: ".$csvFile);
    }
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 0, "\t", "\"");
    }
    fclose($file_handle);
    return $line_of_text;
}

function isAnnulee($val,$app){
    // Seance annulée dans le logiciel métier si valeur
    //  [6 pour Véga]
    //  [3 pour Kinemax]
    //  [K pour Kine4000 : 'O'=annulé, 'K'=Annulé (absent), 'D'=Att désistement ]

    $old_config_agenda_valeur_seance_annulee = 3;

    switch (strtolower($app)) {
        case "kinemax":
            if ($val == 3) return true;
            break;
        case "kine4000":
            if ($val == 'O') return true;
            break;
        case "vega":
            if ($val == 6) return true;
            break;
        default;
            if ($val == $old_config_agenda_valeur_seance_annulee) return true;
            return false;
    }
    return false;
}

function isAbsent($val,$app){
    // Seance annulée dans le logiciel métier si valeur
    //  [TODO pour Véga]
    //  [TODO pour Kinemax]
    //  [K pour Kine4000 : 'O'=annulé, 'K'=Annulé (absent), 'D'=Att désistement ]

    switch (strtolower($app)) {
        case "kinemax":
            if ($val == '') return true;
            break;
        case "kine4000":
            if ($val == 'K') return true;
            break;
        case "vega":
            if ($val == '') return true;
            break;
        default;
            return false;
    }
    return false;
}

?>
