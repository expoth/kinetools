<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Agenda</title>
    <script>
        function checkUncheckRow(callerTag, data) {
            //console.log("callerTag: " + callerTag.checked);
            //console.log("data: " + data);
            css_selector = "[value$=\"_"+data+"\"]";
            //console.log("css_selector:" + css_selector);

            if (callerTag.checked)
                checkUncheckSelector(css_selector, true);
            else
                checkUncheckSelector(css_selector, false);
        }

        function checkUncheckColumn(callerTag, data) {
            //console.log("callerTag: " + callerTag.checked);
            //console.log("data: " + data);
            css_selector = "[value^=\""+data+"_\"]";
            //console.log("css_selector:" + css_selector);

            if (callerTag.checked)
                checkUncheckSelector(css_selector, true);
            else
                checkUncheckSelector(css_selector, false);
        }

        function checkUncheckSelector(css_selector, checked_value) {
            var inputs = document.querySelectorAll(css_selector);
            for (var i=0; i<inputs.length; i++)
                    inputs[i].checked = checked_value;
        }

        function checkUncheckAll(callerTag) {
            if (callerTag.checked)
                newValue = true;
            else
                newValue = false;

            var css_selectors = ["[name^=\"seance[\"]", "[name=\"cbCheckUncheckColumn\"]", "[name=\"cbCheckUncheckRow\"]"];

            for (var s=0; s<css_selectors.length; s++) {
                //console.log(css_selectors[s]);
                checkUncheckSelector(css_selectors[s], newValue);
            }
        }

        function submit_form() {
            document.querySelector("#my_form").submit();
            //alert("a");
        }
        
        function set_semaine(val) {
            document.querySelector("#select_semaine").value = val
            submit_form();
        }
        
    </script>
    <style>
        @import url(<?php echo dirname($_SERVER['REQUEST_URI']) ?>/css/main-intranet.css);
        form input[type="submit"] { display:inline; color:darkblue}

        .domicile { background-color: AntiqueWhite; }
        .annulee  { color: red; text-decoration: line-through; }
        .absent  { color: darkorange;}
        .absent::before { content: "⚠️"; }
        .absent::after { content: "[Abs]"; }
        .new      { color: blue; }
        details { font-family:sans-serif; font-size:80%; }
        summary { display: inline; }
        details summary::-webkit-details-marker {display: none}

        .num_seance {font-size:60%; color:grey; vertical-align:middle;}
    </style>
</head>

<body>
    <h1>Agenda</h1>

<?php 
// Affiche valeur du formulaire
//var_dump($_POST);

include_once $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/tools/call_api.php';
include_once $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/tools/tools.php';
include_once $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/tools/th_csv.php';
$configs = include($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/config/config.php');

//    ___ _                                    _      ___ _____   __
//   / __| |_  __ _ _ _ __ _ ___ _ __  ___ _ _| |_   / __/ __\ \ / /
//  | (__| ' \/ _` | '_/ _` / -_) '  \/ -_) ' \  _| | (__\__ \\ V / 
//   \___|_||_\__,_|_| \__, \___|_|_|_\___|_||_\__|  \___|___/ \_/  
//                     |___/                                        
# Il faut que le CSV soit trié par heure, puis par date (ou alors il faut enlever le break dans la boucle
$csv_file = $configs['agenda_csv_location'];

// Ouvre le fichier CSV
$csv_in_memory = readCSV($csv_file);


//    ___ _        _       _   _     __                         _       
//   / __| |_  ___(_)_ __ | |_| |_  /_/ _ _ __ _ _ __  ___ _  _| |_ ___ 
//  | (__| ' \/ _ \ \ \ / |  _| ' \/ -_) '_/ _` | '_ \/ -_) || |  _/ -_)
//   \___|_||_\___/_/_\_\  \__|_||_\___|_| \__,_| .__/\___|\_,_|\__\___|
//                                              |_|                     

// Liste les thérapeutes qui sont dans le CSV agenda
$th_csv_agenda = Array();
foreach ($csv_in_memory as $row) {
    $cur_th = isset($row[39]) ? $row[39] : '';
    if (!empty($cur_th) && $cur_th !== '') {
        if (!in_array($cur_th , $th_csv_agenda))
            $th_csv_agenda[] = $cur_th ;
    }
}

// Ajoute les thérapeutes définis dans la config.
if (isset($configs['agenda_code_th_suppl'])) {
    foreach ($configs['agenda_code_th_suppl'] as $t) {
        $th_csv_agenda[] = $t ;
    }
}

if (sizeof($th_csv_agenda) === 0) {
    $die_message = 'Aucun thérapeute à lister';
    $die_message .= '<br>⋅ Ni dans le fichier CSV (colonne $row[39] vide partout) [Vérifier l\'export]';
    $die_message .= '<br>⋅ Ni dans le ficher config.php [Vérifier la configuration]';
    $die_message .= '<br>→ Impossible de poursuivre';
    die($die_message);
}

$th_csv_agenda_u = array_unique($th_csv_agenda);
$th_csv_agenda = $th_csv_agenda_u;
unset($th_csv_agenda_u);
sort($th_csv_agenda);

if (isset($_POST['therapeute'])) {
    $therapeute = $_POST['therapeute'];
} else {
    // Définit la valeur par défaut si on n'a pas encore validé le thérapeute
    // typiquement au 1er chargement de l'agenda.
    $therapeute = $th_csv_agenda[0]; // Prend le 1er thérapeute du CSV
}



//   ___      _ _     _       __          _   _                                _       
//  |_ _|_ _ (_) |_  (_)_ _  / _|___ ___ | |_| |_  ___ _ _ __ _ _ __  ___ _  _| |_ ___ 
//   | || ' \| |  _| | | ' \|  _/ _ (_-< |  _| ' \/ -_) '_/ _` | '_ \/ -_) || |  _/ -_)
//  |___|_||_|_|\__| |_|_||_|_| \___/__/  \__|_||_\___|_| \__,_| .__/\___|\_,_|\__\___|
//                                                             |_|                     

// récupère les horaires et durée de séance du thérapeute pour l'agenda intranet
$th_config = new therapeutes();
if (isset($therapeute) && $therapeute !== '') {
    try {
        $duree_rdv = $th_config->get_duree_rdv($therapeute);
    } catch (Exception $e) {
        if ($e->getCode() == 1) {
            echo '<br><strong>'.$e->getMessage().'</strong>';
            $duree_rdv = (int)30;
            echo '<strong><br>Je la fixe à 30.<br></strong>';
        } else {
            echo '<br><strong>'.$e->getMessage().'</strong>';
            echo '<pre>';
            var_dump($e);
            echo '</pre>';
            die;
        }
    }
    try {
        $heure_debut = $th_config->get_horaire_debut_matin_min($therapeute, 'hor_agenda');
    } catch (Exception $e) {
        echo '<br><strong>'.$e->getMessage().'</strong>';
        die;
    }
    try {
        $heure_fin = $th_config->get_horaire_fin_aprem_min($therapeute, 'hor_agenda');
    } catch (Exception $e) {
        echo '<br><strong>'.$e->getMessage().'</strong>';
        die;
    }
    //var_dump($heure_debut);
    //var_dump($heure_fin);
    //var_dump($duree_rdv);
    //die;
} else {
    // Si l'agenda est vide ou encore que le champ therapeute n'est pas renseigé dans le agenda.csv
    // Alors on initialise les variables;
    if (isset($configs['agenda_code_th_suppl'])) {
        $therapeute = $configs['agenda_code_th_suppl'][0];
        $heure_debut = 0;
        $heure_fin = 24*60;
        $duree_rdv = 30;
    } else {
        die("Pas pu initiliaser les variables pour thérapeute de valeur vide: ''");
    }
}

//    ___ _        _                          _          
//   / __| |_  ___(_)_ __  ___ ___ _ __  __ _(_)_ _  ___ 
//  | (__| ' \/ _ \ \ \ / (_-</ -_) '  \/ _` | | ' \/ -_)
//   \___|_||_\___/_/_\_\ /__/\___|_|_|_\__,_|_|_||_\___|
//                                                       

// Liste les YYYY-WW pour les dates qui sont dans le CSV pour faire le menu déroulant de choix de semaine
$weeks_in_csv = Array(); // 2019-48 (YYYY-WW)
foreach ($csv_in_memory as $row) {
    if (isset($row[39]) && $row[39] !== '' && $row[39] === $therapeute) {
        $cur_date = $row[31];
        if (!empty($cur_date) && $cur_date !== '') {
            $cur_date_YYYYWW = getYYYYWWofTimestamp(strtotime($cur_date));
            if (!in_array($cur_date_YYYYWW, $weeks_in_csv))
                $weeks_in_csv[] = $cur_date_YYYYWW;
        }
    }
}
sort($weeks_in_csv);

// Ajoute les semaines à venir dans la liste des semaines
$upcoming_weeks = array();
if (isset($configs['agenda_nb_sem_futures_a_afficher'])) {
    for ($i=0 ; $i <= (int)$configs['agenda_nb_sem_futures_a_afficher']; $i++) {
        $upcoming_weeks[] = getYYYYWWplusDays( 7*$i );
    }
}

$weeks = array_merge($weeks_in_csv, $upcoming_weeks);
$weeks_u = array_unique($weeks);
$weeks = $weeks_u;
unset($weeks_u);
sort($weeks);

if (sizeof($weeks) === 0) {
    $die_message = 'Aucune semaine à lister';
    $die_message .= '<br> ⋅ Ni dans le fichier CSV (colonne $row[31] vide partout) [Vérifier l\'export]';
    $die_message .= '<br> ⋅ Ni dans le ficher config.php [Vérifier la configuration]';
    $die_message .= '<br> → Impossible de poursuivre';
    die($die_message);
}

// Définit la semaine qui sera affichée
if (isset($_POST['semaine'])) {
    $semaine = $_POST['semaine'];
} else {
    // Par défaut on choisit la semaine actuelle
    $semaine = getYYYYWWplusDays(0);
}

// Si la semaine sélectionnée est en dehors de celles dans $weeks
// On la fixe aux limites possibles dans $weeks
if ($semaine < $weeks[0]) {
    $semaine = $weeks[0];
}
if ($semaine > end($weeks)) {
    $semaine = end($weeks);
}

$i = 0;
$index_sem_courante = 0;
foreach ($weeks as $w) {
    if ($w == $semaine)
        $index_sem_courante = $i;
    $i++;
}
unset ($i);

//    _  _ _____ __  __ _                 _     
//   | || |_   _|  \/  | |     __ ___  __| |___ 
//   | __ | | | | |\/| | |__  / _/ _ \/ _` / -_)
//   |_||_| |_| |_|  |_|____| \__\___/\__,_\___|
//                                              
echo '<form id="my_form" name="agenda" action="agenda.php" method="POST">';
echo "\n";
// Affiche le lien de navigation vers la semaine précédente
echo '<p>&nbsp;<a href="#" onclick="set_semaine(\''.$weeks[max(0,($index_sem_courante-1))].'\')">«</a>&nbsp;'."\n\n";

// Construit le HTML du menu déroulant pour les thérapeutes
echo '<select name="therapeute" onChange="submit_form()">';
foreach ($th_csv_agenda as $t) {
    if ($t === $therapeute)
        echo '  <option value="'.$t.'" selected>'.$t.'</option>';
    else
        echo '  <option value="'.$t.'">'.$t.'</option>';
}
echo '</select>'."\n\n";

// Construit le HTML du menu déroulant pour les semaines
echo '<select id="select_semaine" name="semaine" onChange="submit_form()">';
foreach ($weeks as $w) {
    if ($w == $semaine) {
        echo '  <option value="'.$w.'" selected>'.$w.'</option>';
    } else {
        echo '  <option value="'.$w.'">'.$w.'</option>';
    }
}
echo '</select>'."\n\n";


// Bouton SUBMIT pour la sélection du thérapeute et de la semaine
echo '<input type="submit" name="submit_th_semaine" value="Valider">'."\n\n";


echo '<span style="float:right">'."\n";

// Affiche le bouton de publication des séances
echo '<input id="publier_semaine" type="submit" name="submit_button" value="Publier les séances de la semaine">'."\n";

// Affiche le lien de navigation vers la semaine suivante
echo '&nbsp;<a href="#" onclick="set_semaine(\''.$weeks[min(sizeof($weeks)-1,($index_sem_courante+1))].'\')" >»</a>&nbsp;'."\n";
echo '</span></p>';
echo "\n\n";

unset($index_sem_courante);

//    ___                 _      _         
//   / __| ___ _ __  __ _(_)_ _ (_)___ _ _ 
//   \__ \/ -_) '  \/ _` | | ' \| / -_) '_|
//   |___/\___|_|_|_\__,_|_|_||_|_\___|_|  
//                                         
//if (sizeof($weeks) > 0) {

    $array_YYYYMM = explode('-',$semaine);
    $YYYY = $array_YYYYMM[0];
    $WW = $array_YYYYMM [1];
    $jourLundi = getLunVenDates($YYYY, $WW)[0];
    $jourDimanche = getStartAndEndDate($YYYY, $WW)[1];
    unset ($array_YYYYMM, $YYYY, $WW);
    
    if (isset($configs['agenda_semainier_avec_samedi']) && $configs['agenda_semainier_avec_samedi'] === true) {
        $dates = getLunSamDates(explode('-',$semaine)[0],explode('-',$semaine)[1]);
    } else {
        $dates = getLunVenDates(explode('-',$semaine)[0],explode('-',$semaine)[1]);
    }

    //echo '<input type="hidden" name="th" value="'.$therapeute.'">';
    //echo '<input type="hidden" name="sem" value="'.$semaine.'">';
    echo '<table>';
    
    # TABLE HEADER
    echo '<thead class="'.((isset($configs['agenda_semainier_avec_samedi']) && $configs['agenda_semainier_avec_samedi'] === true) ? 'J6' : 'J5');
    echo '"><tr>';
    # Colonne des heures:
    echo '<th class="colheure"><input type="checkbox" checked name="cbCheckUncheckAll" onClick="checkUncheckAll(this)"></th>';
    # Nom des jours
    foreach ($dates as $d) {
        echo '<th><input type="checkbox" checked name="cbCheckUncheckColumn" onClick="checkUncheckColumn(this, \''.$d.'\')"><br>'.$d.'</th>';
    }
    echo '</tr></thead>';

    # TABLE DATA
    unset($date);
    $date = '';

    // On commence le décompte à la minute : $heure_debut%$duree_rdv
    // Ce qui permet, par ex, si on commence sa journée à 7h50, avec des rdv de 20min,
    // le décompte de l'agenda à partir de 00h10
    // On aura donc bien un 1er horaire dans l'agenda à 7h50
    $h_premier_rdv_0h = $heure_debut%$duree_rdv;
    $h_dernier_rdv_23h = min(60*24-1, (((int)(60*24/$duree_rdv))*$duree_rdv) + $h_premier_rdv_0h);

    for ($heure_rdv_min = $h_premier_rdv_0h; $heure_rdv_min <= $h_dernier_rdv_23h; $heure_rdv_min += $duree_rdv) {

        $est_affichable = (($heure_rdv_min >= $heure_debut-$duree_rdv && $heure_rdv_min < $heure_fin ) || $heure_rdv_min >= $h_dernier_rdv_23h-$duree_rdv);
        # Affiche les lignes de données
        if ($est_affichable)
            echo "\n\n<tr". (($heure_rdv_min >= 12*60 && $heure_rdv_min < 14*60) ?
                ' class="pause-dej"' : ''). '>';

        // Colonne des heures (=1er colonne):
        if ($heure_rdv_min < $heure_debut || $heure_rdv_min >= $heure_fin) {
            $heure_rdv_hhmm = "--:--";
        } else {
            $heure_rdv_hhmm = sprintf('%02d', (int)$heure_rdv_min/60) . ':' . sprintf('%02d', ($heure_rdv_min%60));
        }
        
        if ($est_affichable) {
            echo '<th>';
            if ($heure_rdv_min >= $heure_debut && $heure_rdv_min < $heure_fin ) 
                echo '<input type="checkbox" checked name="cbCheckUncheckRow" onClick="checkUncheckRow(this, \''.$heure_rdv_hhmm.'\')"> ';
            echo $heure_rdv_hhmm.'</th>';
        }

        // Prépare le contenu qui sera affiché dans chacune des cases (colonne) de la ligne en cours
        // C'est stocké dans un array $ligne qui sera utilisé dans le code produisant le HTML
        // Ici, on réinitialise les $ligne au moment de changement de créneau horaire
        // (si on est dans ceux de présente du thérapeute)
        if ($heure_rdv_min >= $heure_debut && $heure_rdv_min < $heure_fin+$duree_rdv ) {
            unset($ligne);
            unset($ligne_annulee);
            unset($ligne_absent);
            $ligne = array();
            $ligne_annulee = array();
            $ligne_absent = array();
        }

        foreach ($csv_in_memory as $row) {
            // EST ON SUR UNE DATE QUI CORRESPOND À LA SEMAINE EN COURS ?
            $date = isset($row[31]) ? $row[31] : '';
            if ($date < $jourLundi || $date > $jourDimanche) {
                continue;
            }
            
            // EST ON SUR ON HORAIRE DE SEANCE QUI CORRESPOND AU CRÉNEAU EN COURS ?
            $heure_sea_hhmm = $row[32];
            // Reformate HH:MM:SS en HH:MM le cas échéant
            $heure_sea_hhmm = implode(':', array_slice ( explode(':', $heure_sea_hhmm), 0, 2) );
            $heure_sea_min = 0; //initialise
            if (isset($heure_sea_hhmm) && $heure_sea_hhmm !== '') {
                $time = explode(':', $heure_sea_hhmm);
                $heure_sea_min = ((int)$time[0]*60) + ((isset($time[1]) ? (int)$time[1] : 0)) + ((isset($time[2]) ? (int)$time[2] : 0)/60);
                $heure_sea_min = max($heure_sea_min, $h_premier_rdv_0h);
            } else {
                $heure_sea_min = $h_premier_rdv_0h;
            }
            //echo "<!-- [$row[32] : $row[31] $row[3]] Rdv: $heure_sea_hhmm=$heure_sea_min";
            //echo ' Créneau:['.$heure_rdv_min.'-'.($heure_rdv_min+$duree_rdv)."] -->\n";

            if ($heure_sea_min < $heure_rdv_min ) {
                // Séance avant le créneau en cours...
                continue;
            }

            // Ici on peut mettre un break, car le fichier est trié par heure croissante.
            // On arrête de lire le fichier et on passe directement au créneau horaire suivant
            if ($heure_sea_min >= $heure_rdv_min + $duree_rdv) {
                // Séance après le créneau en cours
                break;
            }

            // EST-ON SUR UNE SÉANCE FAITE PAR LE THÉRAPEUTE EN COURS ?
            // Il n'y a pas de thérapeute renseigné, donc on passe à la suivante
            if ($row[39] === '') {
                continue;
            }

            // Si on n'est pas sur une ligne pour le thérapeute sélectionné, passer à la suivante
            if ($row[39] !== $therapeute) {
                continue;
            }

            // La ligne est valide, on traite son contenu
            foreach ($dates as $d) {
                //if ($date === $d && $heure_sea_min >= $heure_rdv_min && $heure_sea_min < $heure_rdv_min + $duree_rdv) {
                if ($date === $d) {
                    if ($heure_sea_min !== (int)$heure_rdv_min ||
                        $heure_sea_min < $heure_debut ||
                        $heure_sea_min >= $heure_fin ) {
                        $prepend_heure = "<small><strong>$heure_sea_hhmm </strong></small>";
                        if ($heure_sea_min >= $heure_fin && $heure_rdv_min < $heure_fin)
                            $prepend_heure .= '<span style="text-decoration: underline; color:red">APRES HEURE DE FIN</span> ';
                    } else {
                        $prepend_heure = '';
                    }

                    $tags_classes = array();
                    $is_dom = ( ucwords(strtolower($row[36])) == 1 ? true : false) ;
                    $is_annulee = isAnnulee($row[33], $row[41]);
                    $is_absent = isAbsent($row[33], $row[41]);
                    $is_new = ( $row[30] == 'Nouv.' ? true : false);
                    if ($is_dom) $tags_classes[] = 'domicile';
                    if ($is_annulee) $tags_classes[] = 'annulee';
                    if ($is_absent) $tags_classes[] = 'absent';
                    if ($is_new) $tags_classes[] = 'new';
                    $texte_cellule = '<details>';
                    $texte_cellule .= '<summary'. (sizeof($tags_classes) > 0 ? ' class="'.implode(" ",$tags_classes).'"' : ''). '>';
                    $texte_cellule .= $prepend_heure;                           // Heure
                    $texte_cellule .= ucwords(strtoupper($row[3])).' ';         // Nom
                    $texte_cellule .= ucwords(strtolower($row[5]));             // Prénom
                    $texte_cellule .= ' <span class="num_seance">'.$row[30].'</span>' ; // Num Séance
                    $texte_cellule .= '</summary>';
                    $texte_cellule .= '<br>'.ucwords(strtolower($row[6]));      // Adr
                    $texte_cellule .= ' '.ucwords(strtolower($row[7]));         // AdrCpl 
                    $texte_cellule .= '<br>'.ucwords(strtolower($row[8]));      // CP
                    $texte_cellule .= ' '.ucwords(strtolower($row[9]));         // Ville
                    $texte_cellule .= '<br>☎️ '.ucwords(strtolower($row[10]));   // Tel1
                    $texte_cellule .= '<br>'.ucwords(strtolower($row[11]));     // Tel2
                    $texte_cellule .= '</details>';

                    if (isset($ligne) && array_key_exists($d, $ligne))
                        $ligne[$d] .= $texte_cellule;
                    else
                        $ligne[$d] = $texte_cellule;
                        
                    if (isset($ligne_annulee) && array_key_exists($d, $ligne_annulee)) {
                        if ($ligne_annulee[$d] == false && $is_annulee == true) {
                            $ligne_annulee[$d] = false;
                        } else {
                            $ligne_annulee[$d] = $is_annulee;
                        }
                    } else {
                        $ligne_annulee[$d] = $is_annulee;
                    }

                    if (isset($ligne_absent) && array_key_exists($d, $ligne_absent)) {
                        if ($ligne_absent[$d] == false && $is_absent == true) {
                            $ligne_absent[$d] = false;
                        } else {
                            $ligne_absent[$d] = $is_absent;
                        }
                    } else {
                        $ligne_absent[$d] = $is_absent;
                    }
                    
                }
            }
        }
        
        // Produit le code HTML de chaque colonne
        if ($est_affichable) {
            foreach ($dates as $d) {
                if (empty($ligne[$d]) || $ligne_annulee[$d]) {
                    echo '<td class="checkbox">';
                } else {
                    echo '<td>';
                }

                if (!empty($ligne[$d]))
                    echo $ligne[$d];

                if (empty($ligne[$d]) || $ligne_annulee[$d]) {
                    if (! isset($_POST['submit_button']) ) {
                        // Forms has not yet been submitted, by default we check all checkboxes
                        $checked_text="checked";
                    } else if ( isset($_POST['seance'][$d][$heure_rdv_hhmm]) ) {
                        $checked_text="checked";
                    } else {
                        $checked_text="";
                    }
                    if ($heure_rdv_min >= $heure_debut && $heure_rdv_min < $heure_fin)
                        // N'afficher de checkbox que si on est dans les horaires de rdv
                        echo '<input type="checkbox" name="seance['."${d}][$heure_rdv_hhmm".']" value="'."${d}_$heure_rdv_hhmm".'" '. $checked_text . ' >';
                } 
                echo '</td>';
            }
        }

        // Cloture la ligne
        if ($est_affichable)
            echo '</tr>';

    }

    echo '</table>';

//} // Fin (if (sizeof($weeks) > 0))

//    ___             _                            _                     _   
//   | __|_ ___ _____(_) __ _____ _ _ ___  _____ _| |_ _ _ __ _ _ _  ___| |_ 
//   | _|| ' \ V / _ \ | \ V / -_) '_(_-< / -_) \ /  _| '_/ _` | ' \/ -_)  _|
//   |___|_||_\_/\___/_|  \_/\___|_| /__/ \___/_\_\\__|_| \__,_|_||_\___|\__|
//                                                                           
// Call API
if (isset($_POST['submit_button'])) {
    
    // Supprime les seances qui sont dans la base distante (pour la semaine sélectionnée)
    $arr = array( 'therapeute' => "$therapeute", 'jourDebut' => $jourLundi, 'jourFin' => $jourDimanche);
    $data = json_encode($arr);
    $result = CallAPI('POST', $configs['extranet_base_url'].'/api/seance/delete.php', $data);
    //echo $result;

    // Ajoute des séances dans la base distante (soit individuellement, ou en collection, selon la config)
    $envoi_collection = $configs['envoi_collection'];
    $arr = array();
    if (isset($_POST['seance'])) {
        foreach($_POST['seance'] as $jour => $seance) {
            //echo "jour: $jour";
            foreach($seance as $heure => $seance2) {
                if ($envoi_collection) {
                    // On fait un seul envoi vers l'extranet contenant toutes les séances
                    array_push($arr, array( 'therapeute' => "$therapeute",
                                'jour' => $jour,
                                'heure' => $heure,
                                'status' => 0));
                } else {
                    // On fait autant d'envois vers l'extranet qu'il y a de séances
                    //echo "heure: $heure";
                    $arr = array( 'therapeute' => "$therapeute",
                                'jour' => $jour,
                                'heure' => $heure,
                                'status' => 0);
                    //echo $arr;
                    $data = json_encode($arr);
                    //echo $data;
                    $result = CallAPI('POST', $configs['extranet_base_url'].'/api/seance/create.php', $data);
                    //echo $result;
                }

            }
        }
        if ($envoi_collection) {
            $data = json_encode($arr);
            //echo($data);
            $result = CallAPI('POST', $configs['extranet_base_url'].'/api/seance/create_multiple.php', $data);
            //echo $result;
        }
    }

    // Supprime les séances avant aujourd'hui (jusqu'à hier inclus)
    $suppr_seance_avant_auj = $configs['suppr_seance_avant_auj'];
    if ($suppr_seance_avant_auj) {
        // Supprime toutes les seances qui sont avant aujourd'hui
        $hier = getYesterdayYYYYMMAA();
        $arr = array( 'therapeute' => "$therapeute", 'jourDebut' => '2019-01-01', 'jourFin' => $hier);
        $data = json_encode($arr);
        $result = CallAPI('POST', $configs['extranet_base_url'].'/api/seance/delete.php', $data);
        //echo $result;
    }
}


?>
</form>
    <p>&nbsp;</p>
    <hr>
    <p>&nbsp;</p>
    <footer>
        <p><small>Dernière mise à jour des données le : <?php echo date ("Y-m-d à H:i:s.", filemtime($csv_file)) ?></small></p>
        <p><a href="..">Accueil</a></p>
    </footer>
</body>

</html>
