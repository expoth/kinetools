<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */

function check_auth($my_data) {

    $configs = include($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/config/config.php');
    
    $enable_auth_check = $configs['enable_auth_check'];
    $secret = $configs['secret'];
    $algo = $configs['algo'];
    $timestamp_file_location = $configs['timestamp_file_location'];
    
    // Si on n'active pas la vérification de l'authentification, 
    // alors on retourne "true", cad on continue comme l'authentification était ok.
    if (!$enable_auth_check)
        return true;
    
    // HMAC Authentication (inspiré de http://restcookbook.com/Basics/loggingin/)
    
    $client_hash = '';
    $timestamp = 0;
    $nonce = '';

    // Récupère les valeurs des Headers qui contiennent les informations pour vérifier l'Authentication.
    foreach (getallheaders() as $name => $value) {
        //echo "$name: $value\n";
        if (strtolower($name) === strtolower('X-Auth-HMAC-hash'))
            $client_hash = $value;
        if (strtolower($name) === strtolower('X-Auth-HMAC-timestamp'))
            $timestamp = base64_decode($value);
        if (strtolower($name) === strtolower('X-Auth-HMAC-nonce'))
            $nonce = base64_decode($value);
    }

    //echo "client_hash: $client_hash";
    //var_dump($my_data);
    
    $hash = hash_hmac($algo, json_encode($my_data).'+'.$timestamp.'+'.$nonce, $secret);
    
    if (!file_exists($timestamp_file_location))
        touch($timestamp_file_location);
    
    // Vérifie si le timestamp est postérieur au timestamp de la dernière requête.
    // Sinon, on arrête le traitement.
    if ((int)file_get_contents($timestamp_file_location) >= $timestamp) {
        http_response_code(401);
        echo json_encode(array("message" => "Unauthorized. Bad Timestamp (in the past)"));
        return false;
    }
    
    if ($client_hash === $hash) {
        file_put_contents($timestamp_file_location, $timestamp, LOCK_EX);
        return true;
    } else {
        http_response_code(401);
        echo json_encode(array("message" => "Unauthorized."));
        return false;
    }
}

function check_auth_sub($my_data, $client_hash, $timestamp, $nonce, $check_timestamp = true) {

    $configs = include($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/config/config.php');
    
    $enable_auth_check = $configs['enable_auth_check'];
    $secret = $configs['secret'];
    $algo = $configs['algo'];
    $timestamp_file_location = $configs['timestamp_file_location'];
    
    // Si on n'active pas la vérification de l'authentification, 
    // alors on retourne "true", cad on continue comme l'authentification était ok.
    if (!$enable_auth_check)
        return true;
    
    //echo "client_hash: $client_hash";
    //var_dump($my_data);
    
    $hash = hash_hmac($algo, json_encode($my_data).'+'.$timestamp.'+'.$nonce, $secret);
    
    if ($check_timestamp) {
        if (!file_exists($timestamp_file_location))
            touch($timestamp_file_location);
        
        // Vérifie si le timestamp est postérieur au timestamp de la dernière requête.
        // Sinon, on arrête le traitement.
        if ((int)file_get_contents($timestamp_file_location) >= $timestamp) {
            return false;
        }
    }
    
    if ($client_hash === $hash) {
        if ($check_timestamp) {
            file_put_contents($timestamp_file_location, $timestamp, LOCK_EX);
        }
        return true;
    } else {
        return false;
    }

}

function get_hash($my_data, $timestamp, $nonce) {

    $configs = include($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/config/config.php');
    
    $secret = $configs['secret'];
    $algo = $configs['algo'];
    $timestamp_file_location = $configs['timestamp_file_location'];
    
    //var_dump($my_data);
    
    $hash = hash_hmac($algo, json_encode($my_data).'+'.$timestamp.'+'.$nonce, $secret);
    return array($hash, $timestamp ,$nonce);
}

?>
