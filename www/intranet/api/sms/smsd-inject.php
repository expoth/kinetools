<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */


/*
  En entrée
    - Message (Array PHP contenant numéro mobile et texte à envoyer)
    - Hash
    - Nonce
    - Timestamp
*/

include_once __DIR__.'/../shared/auth.php';
$configs = include(__DIR__.'/../../config/config.php');
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Envoi d'un SMS de confirmation de rdv</title>
    <meta charset="UTF-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.6.0.min.js"></script>
<script type="text/javascript">

	function testSameOrigin(url) {
		var loc = window.location,
			a = document.createElement('a');

		a.href = url;

		return a.hostname == loc.hostname &&
			a.port == loc.port &&
			a.protocol == loc.protocol;
	}

	var myXHR = new XMLHttpRequest();

	function update_csrf_hash() {
		// Get the CSRF Hash
		$.ajax({
			url: '<?php echo $configs['kalkun_base_url']; ?>/index.php/kalkun/get_csrf_hash',
			type: "get",
			async: false,
			xhr: function() {
				return myXHR; // Needed to be able to use: myXHR.responseURL
			},
			success: function(data, textStatus, jqXHR) {
				if (myXHR.responseURL == '<?php echo $configs['kalkun_base_url']; ?>/index.php/kalkun/get_csrf_hash') {
					if (textStatus == "error")
					{
						alert("Error getting the CSRF Hash.");
						return;
					}
					if (data == null) {
						// data == null when CSRF is disabled in Kalkun
						$("#kalkun_csrf_tkn").val("null");
					} else {
						$("#kalkun_csrf_tkn").val(data);
					}
				} else {
					// Display the iframe with the login page
					document.getElementById("kalkun_iframe").src="<?php echo $configs['kalkun_base_url']; ?>";
					$("#kalkun_iframe").show();
				}
			}
		});
 	}

	$(document).ready(function() {

		if ( ! testSameOrigin("<?php echo $configs['kalkun_base_url']; ?>"))
		{
			var msg = "Kalkun & KineTools don't have the 'same origin'. This is supported only when CSRF is disabled in Kalkun.";
			//alert (msg);
			console.warn(msg);
		}
		else
		{
			// Add CSRF field to the form (used when CSRF is enabled in Kalkun)
			$('#send_sms_form').append('<input type="hidden" id="kalkun_csrf_tkn" name="kalkun_csrf_tkn" value="" />');
		}

		function submitFormWithCsrf(event) {
			event.preventDefault(); //prevent default form submit
			update_csrf_hash();
			if ($("#kalkun_csrf_tkn").val().length != 0) {
				$("#send_sms_form").unbind('submit').submit();
			} else {
				//alert("Please login to Kalkun.");
			}
		}

		$('#send_sms_form').submit(function (event) {
			if (testSameOrigin("<?php echo $configs['kalkun_base_url']; ?>")) {
				submitFormWithCsrf(event);
			}
		});

		$("#kalkun_iframe").on("load", function(event) {
			if (testSameOrigin("<?php echo $configs['kalkun_base_url']; ?>")) {
				var src = this.contentWindow.location.toString();
				if (src.includes("/login")) {
					// Let the iframe open
				} else {
					$("#kalkun_iframe").hide();
					//alert("Logged in!");
					submitFormWithCsrf(event);
				}
			}
		});
	});

</script>

    </head>

<body>
<h1>Envoi d'un SMS de confirmation de rdv</h1>

<?php if ($configs['smsd-inject-config-mode']) { 

    $secret = $configs['secret'];
    $algo = $configs['algo'];

    if (!isset($_POST['send_sms'])) {
        $timestamp = microtime(true);
        if (version_compare(phpversion(), '7.0.0', ">=")) {
            $nonce = random_int(-99999999,99999999); // nécessite php 7
        } else {
            trigger_error("TODO: php est <php7. Fix Nonce !",E_USER_WARNING);
            $nonce = rand();
        }
        $message = '';
        $tel = '';
        $hash = '';
        $decoded_pwd = '';
    } else {
        $decoded_message = array (
            "num" =>  $_POST['tel'],
            "message" => $_POST['message'], // La colonne 1 contient le message pour les SMS
            );
        $decoded_pwd = json_decode($_POST['pwd']);
        $tel = $_POST['tel'];
        $message = $_POST['message'];
        $timestamp = $_POST['ts'];
        $nonce = $_POST['nonce'];
        //$hash = hash_hmac($algo, $data.'+'.$timestamp.'+'.$nonce, $secret);
        $hash_url = hash_hmac($algo, json_encode($decoded_message).'+'.$timestamp.'+'.$nonce, $secret);
        $hash_pwd = hash_hmac($algo, json_encode($decoded_pwd).'+'.$timestamp.'+'.$nonce, $secret);
        
        $query = array(
            'message' => urlencode(base64_encode(json_encode($decoded_message))),
            'hash' => $hash_url,
            'nonce' => $nonce,
            'ts' => $timestamp
        );
        $url_q = http_build_query($query);
        $url_to_this_page = "/api/sms/smsd-inject.php?".$url_q;
    }
    
    
?>
<div style="background-color:lightgreen">
<p>Préparation des paramètres à utiliser dans la config.</p>
<p> D'abord, mettre un mot de passe dans config.php, ligne : 'secret' => "A_RENSEIGNER",
<p>Renseigner ici un modèle de message (qui peut rester vide) et le code qui sera utilisé pour vérifier le droit d'envoi de SMS</p>
<p>Valider le formulaire, ce qui affichera les paramètres à renseigner dans config.php et l'URL à mettre dans index.php
<form name="send_sms_form" action="smsd-inject.php" method="POST">
<fieldset>
<p><label>Message:</label><br>
<textarea cols="50" rows="7" type="text" name="message"><?php echo $message ?></textarea></p>
<p><label>Tél:</label>
<input type="text" name="tel" size="20" style="font-family:monospace" value="<?php echo $tel ?>"><p>
<p><label>Code:</label>
<input type="password" name="pwd" size="4" maxlength="4" style="font-family:monospace" value="<?php echo $decoded_pwd ?>">
<input type="submit" name="send_sms" value="Afficher les paramètres à utiliser dans la config"></p>
</fieldset>
<fieldset style="color:grey; font-size:70%;">
<p>nonce:<input type="text" name="nonce" readonly value="<?php echo $nonce ?>">
ts:<input type="text" name="ts" readonly value="<?php echo $timestamp ?>"></p>
</fieldset>
</form>
<?php if (isset($_POST['send_sms'])) { ?>
<p>Paramètres à mettre dans config.php (correspondant au code renseigné):</p>
<pre>
    'smsd-inject-config-mode' => false,
    'smsd-inject-ts' => '<?php echo $timestamp ?>',
    'smsd-inject-nonce' => '<?php echo $nonce ?>',
    'smsd-inject-hash' => '<?php echo $hash_pwd ?>',
    'smsd-inject-query_string' => '<?php echo $url_q ?>',
</pre>
<?php } ?>
</div>

<?php
exit;
}
?>

<?php
if (!isset($_POST['send_sms'])) {
    
    if (!isset($_GET['message']) || !isset($_GET['hash']) || !isset($_GET['nonce']) || !isset($_GET['ts'])) {
        die("Paramètre(s) manquants.");
    }
    $decoded_message = json_decode(base64_decode(urldecode($_GET['message'])));
    $hash = $_GET['hash'];
    $nonce = $_GET['nonce'];
    $ts = $_GET['ts'];
    
    if (!check_auth_sub($decoded_message, $hash, $ts, $nonce, false)) {
        echo "Message corrumpu ou authentification incorrecte. Abandon.";
        exit;
    }
    
    $tel = $decoded_message->num;
    $message = $decoded_message->message;
    
?>

<hr/>

<?php if ($configs['smsd-inject-agent'] === 'gammu-smsd-inject')
{ ?>
<form name="send_sms_form" action="smsd-inject.php" method="POST">
<fieldset>
<p><label>Message:</label><br>
<textarea cols="50" rows="7" type="text" name="message"><?php echo $message ?></textarea></p>
<p><label>Tél:</label>
<input type="text" name="tel" size="20" style="font-family:monospace" value="<?php echo $tel ?>"><p>
<p><label>Code:</label>
<input type="password" name="pwd" size="4" maxlength="4" style="font-family:monospace" value="">
<input type="submit" name="send_sms" value="Envoyer SMS via gammu-smsd-inject"></p>
</fieldset>
<fieldset style="color:grey; font-size:70%; display:none">
<p>hash:<input type="text" name="hash" size="40" style="font-family:monospace" readonly value="<?php echo $hash ?>">
nonce:<input type="text" name="nonce" readonly value="<?php echo $nonce ?>">
ts:<input type="text" name="ts" readonly value="<?php echo $ts ?>"></p>
</fieldset>
</form>

<p>&nbsp;</p>
<?php
// End $configs['smsd-inject-agent'] === 'gammu-smsd-inject'
}

if ($configs['smsd-inject-agent'] === 'kalkun')  {
// Envoie un message via Kalkun. Ce formulaire va préremplir le formulaire d'envoi dans l'interface
// de Kalkun. La validation de l'envoi se fait dans l'interface de Kalkun.

?>
<div style="display:flex; flex-wrap: wrap;">
<iframe src="about:blank" height="500" width="400" id="kalkun_iframe" style="display: none;"></iframe>

<form name="send_sms_form" id="send_sms_form" action="<?php echo $configs['kalkun_base_url']; ?>/" method="post">

<fieldset>
<input type="hidden" name="action" value="compose">
<input type="hidden" name="type" value="prefill">

<p><label>Message:</label><br>
<textarea cols="50" rows="7" type="text" name="msg"><?php echo $message ?></textarea></p>
<p><label>Tél:</label>
<input type="text" name="phone" size="20" style="font-family:monospace" value="<?php echo $tel ?>">
<input type="submit" name="send_sms" value="Envoyer SMS via Kalkun API PHP">
</fieldset>
<!--
<p>hash:<input type="text" name="hash" size="40" style="font-family:monospace" readonly value="<?php echo $hash ?>">
nonce:<input type="text" name="nonce" readonly value="<?php echo $nonce ?>">
ts:<input type="text" name="ts" readonly value="<?php echo $ts ?>"></p>
-->
</form>

</div>


<?php
// End $configs['smsd-inject-agent'] === 'kalkun'
} ?>
<hr/>
<p>&nbsp;</p>
<?php


}

if (isset($_POST['send_sms'])) {

    // D'abord on vérifie si le code renseigné dans le formulaire est correct.
    
    // Pas très sûr comme méthode de vérification du code, car on utilise toujours 
    // le même nonce et timestamp. Donc c'est juste comme si on stockait un mot de passe sous forme cryptée...
    
    // Décommenter ces deux lignes si on veut connaitre le hash du code renseigné, 
    // afin de le mettre dans $configs['smsd-inject-hash']
    
    //$pwd_hash = get_hash (json_decode($_POST['pwd']), $configs['smsd-inject-ts'], $configs['smsd-inject-nonce'] );
    //echo 'Hash pour le code renseigné: "'.$pwd_hash[0].'"';
    
    if (check_auth_sub(json_decode($_POST['pwd']), 
                    $configs['smsd-inject-hash'], 
                    $configs['smsd-inject-ts'], 
                    $configs['smsd-inject-nonce'], 
                    false)) {
        
        // Ici procéder à l'appel de smsd-inject

        // Setlocale et Putenv nécessaire pour que le message ne soit pas tronqué dès qu'il y a 
        // un caractère accentué.
        setlocale(LC_ALL, "fr_FR.UTF-8");
        putenv('LC_ALL=fr_FR.UTF-8');    

        $cmd = "/usr/bin/gammu-smsd-inject TEXT '".$_POST['tel']."' -autolen 2000 -text ".escapeshellarg($_POST['message']);
        unset($cmd_output);
        unset($cmd_return_var);
        $ret = exec($cmd, $cmd_output, $cmd_return_var );

        $query = array(
                    'tel' => urlencode($_POST['tel']),
                    'message' => urlencode($_POST['message']),
                    'cmd' => $cmd,
                    'cmd_output' => $cmd_output,
                    'cmd_return_var' => $cmd_return_var,
                    'ret' => $ret,
                );
        $url_q = http_build_query($query);
        
        $envoi_ok = ($cmd_return_var == 0);

        if ($envoi_ok) {
            // Si l'envoi a réussi, on redirige vers la page de confirmation
            header('Location: smsd-inject_confirm.php?'.$url_q);
            exit;
        } else {
            // Si l'envoi a échoué on redirige vers la page d'échec
            header('Location: smsd-inject_ko.php?'.$url_q);
            exit;
        }
        
    } else {
        // Ici le code renseigné est faux, donc envoi refusé.
        header('Location: smsd-inject_ko_code_faux.php');
        exit;
    }

}


?>


</body>
</html>
