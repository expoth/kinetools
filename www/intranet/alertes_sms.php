<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Paramétrage Alertes SMS</title>
    <script>
        function disp_hide_element(callerTag, css_selector) {
            // Get the element to hide/unhide
            var elt = document.querySelectorAll(css_selector);

            // If the checkbox is checked, display the output text
            if (callerTag.checked == true) {
                for (var i=0; i<elt.length; i++)
                    elt[i].style.display = "none";
            } else {
                for (var i=0; i<elt.length; i++)
                    elt[i].style.display = "block";
            }
        }
    </script>
    <style>
        /*@import url(<?php echo dirname($_SERVER['REQUEST_URI']) ?>/css/main-intranet.css);*/
        form input[type="submit"] { display:inline; color:darkblue}

        .domicile { background-color: AntiqueWhite; }
        details { font-family:sans-serif; font-size:80%; }
        summary { display: inline; }
        details summary::-webkit-details-marker {display: none}

        .num_seance {font-size:60%; color:grey; vertical-align:middle;}
        
        .form-err-message { background-color:orange; border-radius:0.8em; padding:0.8em;}
        .form-ok-message { background-color:lightgreen; border-radius:0.8em; padding:0.8em;}
        
        div.boites_th {position:relative; display: flex; flex-wrap: wrap;}
        .boite_th {position:relative; display: inline; flex:0 1 auto; background-color:beige; }
    </style>
</head>

<body>
    <h1>Paramétrage alertes SMS</h1>

<?php 
// Affiche valeur du formulaire
// echo '<pre>';
// var_dump($_POST);
// echo '</pre>';

include_once $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/tools/th_csv.php';
include_once $_SERVER['CONTEXT_DOCUMENT_ROOT'].'/tools/tools.php';
$configs = include($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/config/config.php');

if ($_POST && (isset($_POST['update_ini']) || isset($_POST['add_th']) ) && isset($_POST['ini'])) {
    $ini_array = $_POST['ini'];
    foreach ($_POST['form'] as $fk => $fv) {
        if (substr( $fk, 0, 3 ) === "th_") {
            if (isset($_POST['form'][$fk]['msg_rappel_standard']) && (bool) $_POST['form'][$fk]['msg_rappel_standard'] === true ||
                isset($ini_array[$fk]['msg_rappel']) && $ini_array[$fk]['msg_rappel'] === $ini_array['msg']['rappel']) {
                $ini_array[$fk]['msg_rappel'] = "";
            }
            if (isset($_POST['form'][$fk]['msg_annul_standard']) && (bool) $_POST['form'][$fk]['msg_annul_standard'] === true ||
                isset($ini_array[$fk]['msg_annul']) && $ini_array[$fk]['msg_annul'] === $ini_array['msg']['annul']) {
                $ini_array[$fk]['msg_annul'] = "";
            }
        }
    }
    
    // Ajoute le nouveau thérapeute
    $new_key_in_form = array_values(array_diff(array_keys($_POST['form']), array_keys($_POST['ini'])));
    if (sizeof($new_key_in_form) > 0) {
        //var_dump($new_key_in_form);
        if ( isset($_POST['form'][$new_key_in_form[0]]['code_th']) && $_POST['form'][$new_key_in_form[0]]['code_th'] !== '') {
            $ini_array[$new_key_in_form[0]] = $_POST['form'][$new_key_in_form[0]];
            echo '<p class="form-err-message">Nouveau thérapeute ajouté, veuillez le paramétrer puis enregistrer.</p>';
        }
    }

    // Fixe le type de "activer_envoie" à numérique/bool
    foreach ($ini_array as $key => $value) {
        if(isset($ini_array[$key]['activer_envoi'])) {
            $ini_array[$key]['activer_envoi'] = (int)$ini_array[$key]['activer_envoi'];
        }
    }

    try {
        $ret = write_ini_file($configs['alertes_sms_ini_location'], $ini_array);
        if (!$ret) {
            echo '<p class="form-err-message">Erreur lors de la sauvegarde du paramétrage</p>';
            die;
        }
        echo '<p class="form-ok-message">Changements enregistrés.</p>';
    } catch (Exception $e) {
        echo '<p class="form-err-message">Erreur (exception) lors de la sauvegarde du paramétrage</p>';
        echo '<br><strong>'.$e->getMessage().'</strong>';
        die;
    }
} else {
    $ini_array = parse_ini_file($configs['alertes_sms_ini_location'], true, INI_SCANNER_TYPED);
}
// echo '<pre>';
// var_dump($ini_array);
// echo '</pre>';

?>

<form name="paramètres" action="alertes_sms.php  " method="POST">
<h2>Messages standards</h2>
<fieldset>
<p><label for="rdv_kine_message">Message de rappel</label><br>
<textarea cols="80" rows="2" id="rdv_kine_message" name="ini[msg][rappel]" placeholder="Message de rappel.">
<?php if (isset($ini_array['msg']['rappel'])) echo $ini_array['msg']['rappel'] ?>
</textarea>
</p>

<p><label for="rdv_kine_message">Message d'annulation</label><br>
<textarea cols="80" rows="2" id="rdv_kine_message" name="ini[msg][annul]" placeholder="Message d'annulation.">
<?php if (isset($ini_array['msg']['annul'])) echo $ini_array['msg']['annul'] ?>
</textarea>
</p>
<p>Variables utilisables:<br>
<!--⋅ @TH@ ("Nom Prénom" du thérapeute tel que défini dans therapeutes.csv)<br>-->
⋅ @DATE@ (date rendez-vous)<br>
⋅ @HEURE@ (heure rendez-vous)<br>
⋅ @ID_PROCH_RDV@ (n° identifiant le patient pour le fichier des prochains_rdv)<br>
⋅ @EXRANET_BASE_URL@ (adresse de base du site internet au format: https://monsite.com)
</p>
</fieldset>
<h2>Configuration de chaque thérapeute</h2>
<div class="boites_th">
<?php
    $ths_obj = new therapeutes();
    $ths = $ths_obj->get_therapeutes();
    
    $codes_th = array();
    $code_th_present_in_ini = array();
    
    foreach ($ini_array as $key => $value) {
        if (isset($ini_array[$key]['code_th'])) {
            $code_th_present_in_ini[] = $ini_array[$key]['code_th'];
        }
    }
    
    // Ajoute ceux du therapeutes.csv
    $code_th_present_in_csv = array();
    foreach ($ths as $th) {
        $codes_th[] = $th['code'];
        foreach ($ini_array as $key => $value) {
            if (isset($ini_array[$key]['code_th']) && $ini_array[$key]['code_th'] === $th['code']) {
                $code_th_present_in_csv[] = $ini_array[$key]['code_th'];
            }
        }
    }
    $code_th_present_in_csv_and_ini=array_merge($code_th_present_in_ini, $code_th_present_in_csv);
    
    $code_th_missing_in_ini = array_diff($codes_th, $code_th_present_in_csv);
    
//     var_dump($codes_th);
//     var_dump($code_th_present_in_ini);
//     var_dump($code_th_missing_in_ini);
    
    $th_key_id = array();
    foreach ($ini_array as $key => $value) {
        if (substr( $key, 0, 3 ) === "th_") {
            $th_key_id[] = (int)substr( $key, 3);
?>

<p>
<fieldset class="boite_th">
<?php 
            if (isset($ini_array[$key]['code_th']) && in_array($ini_array[$key]['code_th'],$code_th_present_in_csv_and_ini)) {
                echo '<select name="ini['.$key.'][code_th]">';
                echo '  <option value="'.$ini_array[$key]['code_th'].'" selected>'.$ini_array[$key]['code_th'].' ('.$ths_obj->get_therapeute_nom_prenom($ini_array[$key]['code_th']).')</option>';
                echo '</select>'."\n\n";
            } else {
                echo '<select name="ini['.$key.'][code_th]">';
                echo '<option value=""></option>';
                foreach ($code_th_missing_in_ini as $th) {
                    if (isset($ini_array[$key]['code_th']) && in_array($ini_array[$key]['code_th'],$code_th_present_in_ini))
                        echo '  <option value="'.$th.'" selected>'.$th.' ('.$ths_obj->get_therapeute_nom_prenom($th).')</option>';
                    else
                        echo '  <option value="'.$th.'">'.$th.' ('.$ths_obj->get_therapeute_nom_prenom($th).')</option>';
                }
                echo '</select>'."\n\n";
            }
?>
<p>Activer l'envoi<br>
<input type="radio" id="ini[<?php echo $key; ?>][activer_envoi]_1" name="ini[<?php echo $key; ?>][activer_envoi]" value="1" <?php if(isset($ini_array[$key]['activer_envoi']) && (bool)$ini_array[$key]['activer_envoi'] === true) echo 'checked' ?>>
<label for="ini[<?php echo $key; ?>][activer_envoi]_1">oui</label>
<input type="radio" id="ini[<?php echo $key; ?>][activer_envoi]_0" name="ini[<?php echo $key; ?>][activer_envoi]" value="0" <?php if(!isset($ini_array[$key]['activer_envoi']) || (isset($ini_array[$key]['activer_envoi']) && (bool)$ini_array[$key]['activer_envoi'] === false)) echo 'checked' ?>>
<label for="ini[<?php echo $key; ?>][activer_envoi]_0">non</label>
</p>

<p>Type de message<br>
<input type="radio" id="ini[<?php echo $key; ?>][type_message]_rappel" name="ini[<?php echo $key; ?>][type_message]" value="rappel" <?php if(!isset($ini_array[$key]['type_message']) || (isset($ini_array[$key]['type_message']) && $ini_array[$key]['type_message'] === "rappel")) echo 'checked' ?>>
<label for="ini[<?php echo $key; ?>][type_message]_rappel">Rappel de rdv</label>
<input type="radio" id="ini[<?php echo $key; ?>][type_message]_annul" name="ini[<?php echo $key; ?>][type_message]" value="annul" <?php if(isset($ini_array[$key]['type_message']) && $ini_array[$key]['type_message'] === "annul") echo 'checked' ?>>
<label for="ini[<?php echo $key; ?>][type_message]_annul">Annulation de rdv</label>
</p>

<p><label for="ini[<?php echo $key; ?>][nb_max_envois_par_rdv_typemsg]">Nb max de SMS envoyés<br> par couple 'rdv+Type'</label><br>
<input type="number" id="ini[<?php echo $key; ?>][nb_max_envois_par_rdv_typemsg]" name="ini[<?php echo $key; ?>][nb_max_envois_par_rdv_typemsg]" value="<?php if (isset($ini_array[$key]['nb_max_envois_par_rdv_typemsg']) && $ini_array[$key]['nb_max_envois_par_rdv_typemsg'] != '') echo $ini_array[$key]['nb_max_envois_par_rdv_typemsg']; else  echo '1'; ?>" min=0 max=100 size="2">
</p>

<p><label for="rdv_kine_message">Message de rappel</label><br>

<?php $check_me = (!(isset($ini_array[$key]['msg_rappel']) && $ini_array[$key]['msg_rappel'] !== '')) ?>
<input type="checkbox" 
    id="form[<?php echo $key; ?>][msg_rappel_standard]"
    name="form[<?php echo $key; ?>][msg_rappel_standard]" 
    <?php if ($check_me) echo 'checked' ?>
    onclick="disp_hide_element(this,'#msg_rappel_standard_<?php echo $key; ?>')"
    >
<label for="form[<?php echo $key; ?>][msg_rappel_standard]">Utiliser le message standard.</label><br>

<textarea cols="80" rows="2" 
    id="msg_rappel_standard_<?php echo $key; ?>" 
    name="ini[<?php echo $key; ?>][msg_rappel]" 
    placeholder="Message de rappel."
    style="<?php if ($check_me) echo 'display:none'; ?>"
    >
<?php if (!isset($ini_array[$key]['msg_rappel']) || !$ini_array[$key]['msg_rappel']) echo $ini_array['msg']['rappel']; else echo $ini_array[$key]['msg_rappel']; ?>
</textarea>
</p>

<p><label for="rdv_kine_message">Message d'annulation</label><br>

<?php $check_me = (!(isset($ini_array[$key]['msg_annul']) && $ini_array[$key]['msg_annul'] !== '')) ?>
<input type="checkbox"
    id="form[<?php echo $key; ?>][msg_annul_standard]"
    name="form[<?php echo $key; ?>][msg_annul_standard]" 
    <?php if ($check_me) echo 'checked' ?>
    onclick="disp_hide_element(this,'#msg_annul_standard_<?php echo $key; ?>')"
    >
<label for="form[<?php echo $key; ?>][msg_annul_standard]">Utiliser le message standard.</label><br>

<textarea cols="80" rows="2" 
    id="msg_annul_standard_<?php echo $key; ?>" 
    name="ini[<?php echo $key; ?>][msg_annul]" 
    placeholder="Message d'annulation."
    style="<?php if ($check_me) echo 'display:none'; ?>"
    >
<?php if (!isset($ini_array[$key]['msg_annul']) || !$ini_array[$key]['msg_annul']) echo $ini_array['msg']['annul']; else echo $ini_array[$key]['msg_annul']; ?>
</textarea>
</p>

</fieldset>
<?php   }
    }


echo '</div><p>';    
    // Menu déroulant pour le choix d'ajout d'un nouveau thérapeute.
    echo '<select name="form[th_'.(max($th_key_id)+1).'][code_th]">';
    echo '<option value=""></option>';
    foreach ($code_th_missing_in_ini as $th) {
        echo '  <option value="'.$th.'">'.$th.' ('.$ths_obj->get_therapeute_nom_prenom($th).')</option>';
    }
    echo '</select>'."\n\n";
    echo '<input type="submit" name="add_th" value="Ajouter un nouveau thérapeute">';
?>

<p><input type="reset" value="Réinitialiser">
<input type="submit" name="update_ini" value="Enregistrer">
</p>
</form>

<p>
    <p>&nbsp;</p>
    <hr>
    <p>&nbsp;</p>
    <footer>
        <p><small>Dernière mise à jour du paramétrage: <?php echo date ("Y-m-d à H:i:s.", filemtime($configs['alertes_sms_ini_location'])) ?></small></p>
        <p><a href="..">Accueil</a></p>
    </footer>
</body>

</html>
