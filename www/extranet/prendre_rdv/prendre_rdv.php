<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rendez-vous disponibles - Cabinet de kinésithérapie</title>
    <script src="/css/script.js"></script>
    <style>
        @import url(/css/main.css);
        /*header {position:fixed; height:3rem; top:0}*/
        /*#contenu {position:relative; top:10rem; background:lightgrey}*/
        /*.liste_th a { display : block }*/
        .liste_th { text-align: center}
    </style>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/nav.php'; ?>
    <h2 id="therapeutes">Rendez-vous disponibles</h2>

<?php

    if (isset ($_GET['th']))
        $th_get = base64_decode(urldecode($_GET['th']));
    //$th_get = $_GET['th'];
    //echo $th_get;
    
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/tools.php';
    // include database and object files
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/database.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/th_csv.php';
    
    $ths_obj = new therapeutes();

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // REQUETE DES THÉRAPEUTES
    $query_th = "SELECT distinct(therapeute) as therapeute
                FROM seances
                ORDER BY therapeute asc";
    // var_dump($query_th);
    // prepare query statement
    $stmt_th = $db->prepare($query_th);
    // execute query
    $stmt_th->execute();

    // AFFICHE LES THÉRAPEUTES
    if (!isset ($_GET['th']))
        echo '<p>Sélectionnez le thérapeute dont vous souhaitez afficher les disponibilités.</p>';

    echo '<p class="liste_th"><a href="?th='.urlencode(base64_encode("tous")).'#'.urlencode("tous").'">Tous</a>';
    while ($row = $stmt_th->fetch(PDO::FETCH_ASSOC)) {
        extract($row); // produit $therapeute
        $th_np = $ths_obj->get_therapeute_civil_nom_prenom($therapeute);
        echo ' | <a class="th" href="?th='.urlencode(base64_encode($therapeute)).'#'.urlencode($th_np).'">'.$th_np."</a>";
    }
    echo '</p><hr>';

    $rdv_counter = 0;
    if ( (isset($_GET['th']) && $th_get === "tous") ||
        !isset($_GET['th'])) {
    
    // AFFICHE LA LISTE DES RDV GROUPÉ PAR DATE
    // REQUETE DES DATES
    $query_jours = "SELECT distinct(jour) as jour
                    FROM (SELECT jour, jour || '_' || heure AS jour_heure
                        FROM seances 
                        WHERE jour_heure >= '".rdv_futurs_uniqt_now_JH()."')
                    ORDER BY jour asc";
    // var_dump($query_jours);
    // prepare query statement
    $stmt_jours = $db->prepare($query_jours);
    $stmt_jours->execute();

    echo '<p id="tous">Affichage par jour (<a href="#therapeutes">choisir un thérapeute</a>)</p>';

    // AFFICHE LA LISTE DES RDV GROUPÉ PAR DATE
    while ($row = $stmt_jours->fetch(PDO::FETCH_ASSOC)) {
        extract($row); // Produit $jour

        $query_heure = "SELECT distinct(heure) as heure
            FROM (SELECT heure, jour || '_' || heure AS jour_heure
                FROM seances
                WHERE jour = '$jour' and jour_heure >='".rdv_futurs_uniqt_now_JH()."')
            ORDER BY heure asc";
        $stmt_heure = $db->prepare($query_heure);
        $stmt_heure->execute();
        
        echo '<ul><li>';
        echo formate($jour);
        echo "<ul>";
        
        while ($row_heure = $stmt_heure->fetch(PDO::FETCH_ASSOC)) {
            extract($row_heure);  // Produit $heure
            
            $seance_count_sql = "SELECT COUNT(*) 
                                FROM seances 
                                WHERE jour = '$jour' and heure = '$heure'";
            if ($res = $db->query($seance_count_sql)) {
                if ($res->fetchColumn() > 1) {
                    $more_than_one_result_for_this_date_time = true;
                } else {
                    $more_than_one_result_for_this_date_time = false;
                }
            }
            
            $query_seance = "SELECT status, therapeute
                FROM seances
                where jour = '$jour' and heure = '$heure'
                order by jour, heure, therapeute asc";
            $stmt_seance = $db->prepare($query_seance);
            $stmt_seance->execute();

            echo "<li>";
            if ($more_than_one_result_for_this_date_time) {
                echo $heure;
                echo "<ul>";
            }
            
            while ($row_seance = $stmt_seance->fetch(PDO::FETCH_ASSOC)) {
                extract($row_seance); // Produit $status et $therapeute
                $th_np = $ths_obj->get_therapeute_nom_prenom($therapeute);
                
                if ($more_than_one_result_for_this_date_time) {
                    echo '<li>';
                    echo_form_rdv($therapeute, $th_np, $jour, $heure, $status, true, false);
                    $rdv_counter++;
                    echo '</li>';
                } else {
                    echo_form_rdv($therapeute, $th_np, $jour, $heure, $status, true, true);
                    $rdv_counter++;
                }
            }
            if ($more_than_one_result_for_this_date_time) {
                echo '</ul>';
            }
            echo '</li>';
            
        }
        echo "</ul>";
        echo "</li></ul>";
    }
    }

    
    
    if (isset($_GET['th']) && $th_get !== "tous") {

    // AFFICHE LA LISTE DES RDV CHAQUE THERAPEUTE A LA SUITE
    //$stmt_th->execute();
    //while ($row = $stmt_th->fetch(PDO::FETCH_ASSOC)) {
    //    extract($row); // produit $therapeute
    $therapeute = $th_get;
        $th_np = $ths_obj->get_therapeute_civil_nom_prenom($therapeute);
        echo '<p><a class="th" id="'.urlencode($th_np).'">'.$th_np.'</a>';
        echo ' (<a href="#therapeutes">changer de thérapeute</a>';
        echo ' | <a href="?th='.urlencode(base64_encode("tous")).'#'.urlencode("tous").'">tout afficher</a>)</p>';

        if ($ths_obj->therapeute_utilise_site_rdv_externe($therapeute)) {
            echo '<div>⋅ Prendre rendez-vous avec <a href="'.$ths_obj->get_therapeute_site_rdv_url($therapeute).'"><img src="/css/'.$ths_obj->get_therapeute_site_rdv_nom($therapeute).'.png" alt="'.$ths_obj->get_therapeute_site_rdv_nom($therapeute).'" style="vertical-align: text-bottom"></a></div>';
        } else {

            $query_jour = "SELECT distinct(jour) as jour
                FROM (SELECT jour, jour || '_' || heure as jour_heure
                    FROM seances
                    WHERE therapeute = '$therapeute'
                    AND jour_heure >= '".rdv_futurs_uniqt_now_JH()."')
                ORDER BY jour asc";
            $stmt_jour = $db->prepare($query_jour);
            $stmt_jour->execute();
            echo "<ul>";
            $th_np = $ths_obj->get_therapeute_nom_prenom($therapeute);
            while ($row_jour = $stmt_jour->fetch(PDO::FETCH_ASSOC)) {
                $jour = $row_jour['jour'];
                echo "<li>";
                echo formate($jour);

                $query_seance = "SELECT heure, status
                    FROM (SELECT heure, status, jour, jour || '_' || heure as jour_heure
                        FROM seances
                        WHERE therapeute = '$therapeute'
                        AND jour = '$jour' AND jour_heure >= '".rdv_futurs_uniqt_now_JH()."')
                    ORDER BY jour asc, heure asc";
                $stmt_seance = $db->prepare($query_seance);
                $stmt_seance->execute();

                echo "<ul>";
                while ($row_seance = $stmt_seance->fetch(PDO::FETCH_ASSOC)) {
                    //$jour = $row_seance['jour'];
                    $heure = $row_seance['heure'];
                    $status = $row_seance['status'];
                    //extract($row_seance);
                    echo '<li>';
                    echo_form_rdv($therapeute, $th_np, $jour, $heure, $status, false);
                    $rdv_counter++;
                    echo '</li>';
                }
                echo '</ul></li>';
            }
            echo "</ul>";
        }
    //}
    
    }
    if ($rdv_counter === 0 && isset($_GET['th'])) {
        echo '<p> ❗ Aucun rendez-vous disponible. Veuillez ressayer ultérieurement.</p>';
    }
?>


<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'; ?>
</body>

</html>
