<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Prendre rendez-vous - Cabinet de kinésithérapie</title>
    <script src="/css/script.js"></script>
    <script>
        function set_email_input_required(callerTag, css_selector) {
            // Get selected value of radio button group
            var selected_radio = document.querySelector('input[name="'+callerTag+'"]:checked').value;
            // console.log(selected_radio);
            
            // Depending on selected radio button make field required:
            if (selected_radio === "email_tel" || selected_radio === "email") {
                document.querySelector(css_selector).required = true;
                //console.log("r: "+document.querySelectorAll(css_selector)[0].required);
            } else {
                document.querySelector(css_selector).required = false;
                //console.log("r: "+document.querySelectorAll(css_selector)[0].required);
            }
        }
        
    </script>
    <style>
        @import url(/css/main.css);
        
        .form-err-message { background-color:orange; border-radius:0.8em; padding:0.8em;}
        .form-err-message strong { font-weight:normal;}
        
        input#rdv_kine_nom_prenom, textarea#rdv_kine_message, input#email {
            width:100%;
            box-sizing: border-box; /* Fixes: https://bugzilla.mozilla.org/show_bug.cgi?id=1612271 */
        }
        input#email_field {
            /* Piège à spammeur */
            display:none;
            background:orange;
            color:blue;
        }
    </style>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/nav.php'; ?>
    <h2>Prendre rendez-vous</h2>

<?php
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/tools.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/tools_mail.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/myGnuPG.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/database.php';
    
    $configs = include($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

// Vérifie que le préremplissage du thérapeute, date et jour
// en provenance de la page d'avant, sont bien renseignés.
// Sinon arrêter là.
    if ( !isset($_POST['th_code']) || $_POST['th_code'] == '' ||
        !isset($_POST['therapeute']) || $_POST['therapeute'] == '' ||
        !isset($_POST['jour']) || $_POST['jour'] == '' ||
        !isset($_POST['heure']) || $_POST['heure'] == '' ||
        !isset($_POST['status']) || $_POST['status'] == '') {

        echo '<p class="form-err-message"><strong>Il y a eu un problème. Thérapeute, jour et/ou heure du rendez-vous n\'ont pu être pré-renseignés.</strong></p>';
        echo '<p>Veuillez recommencer depuis la <a href="prendre_rdv.php">liste des rendez-vous</a>. Vous allez être redirigé vers celle ci.</p>';

        header( "Refresh:7; url=prendre_rdv.php", true, 303);
        die;
    }

// Traitement de la soumission du formulaire de cette page.
if (isset($_POST['submit_rdv'])) {
    $is_tel_ok = false;
    $is_mail_ok = false;
    $is_timestamp_ok = false;

    //Vérifier que le n° de tél est saisi
    if ( $_POST['rdv_kine_telephone'] != '' ) {
        $is_tel_ok = true;
    } else {
        echo '<p class="form-err-message"><strong>Veuillez renseigner le numéro de téléphone.</strong></p>';
        $is_tel_ok = false;
    }
    
    // On ne vérifie la présence d'un mail que si l'utilisateur a demandé une confirmation par mail.
    if ( $_POST['mode_confirm'] === "email_tel" || $_POST['mode_confirm'] === "email") {
        // $re = '/[a-z0-9._%+-]+@[a-z0-9.-]+/';
        // Source : http://emailregex.com/
        $re = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
        $ret = preg_match($re, $_POST['email'], $matches);
        if ($ret === 0) {
            $is_mail_ok = false;
            echo '<p class="form-err-message"><strong>Veuillez renseigner une adresse courriel valide.</strong></p>';
        } else {
            $is_mail_ok = true;
        }
    } else {
        $is_mail_ok = true;
    }

    if ( $_POST['email_field'] != '') {
        header( "Location: prendre_rdv_confirm3.php");
        exit;
    }
        
    // Si cela fait moins de 9 secondes entre le moment où la page a été chargée et où le patient la valide,
    // C'est trop rapide, on demande d'attendre.
    if ( $_POST['timestamp'] > time() - 9) {
        echo '<p class="form-err-message"><strong>Vous avez rempli le formulaire un peu trop vite.</strong><br> Veuillez attendre encore quelques secondes avant de valider à nouveau.<br> Merci.</p>';
        $is_timestamp_ok = false;
    } else {
        $is_timestamp_ok = true;
    }

    if ($is_mail_ok && $is_tel_ok && $is_timestamp_ok) {

        // Envoyer un mail
        // https://www.vulgarisation-informatique.com/mail.php
        $mail_configs = include($_SERVER['DOCUMENT_ROOT'].'/config/mail_config.php');
        
        // Construit $to (destinataire du mail, ici le thérapeute)
        $to = $mail_configs['to_complet'];
        
        // Construit $subject
        $subject = "Demande Rdv: ";
        $subject .= $_POST['therapeute'] . " "; 
        $subject .= $_POST['jour'] . " ";
        $subject .= $_POST['heure'];
        if ( $_POST['status'] == 0 )
            $subject .= ' ✅';
        else
            $subject .= ' ⚠️';
        $subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
        
        // Construit le texte affiché dans le mail pour le thérapeute
        $msg_plaintext  = 'Thérapeute: '.$_POST['therapeute']."\n";
        $msg_plaintext .= 'Jour: '.$_POST['jour'].' ['.formate($_POST['jour'])."]\n";
        $msg_plaintext .= 'Heure: '.$_POST['heure']."\n";
        $msg_plaintext .= "\n";
        $msg_plaintext .= 'Nom: '.$_POST['rdv_kine_nom_prenom']."\n";
        $msg_plaintext .= 'Date Naissance: '.$_POST['rdv_kine_date_naissance']."\n";
        $msg_plaintext .= "\n";
        $msg_plaintext .= 'Téléphone: '.$_POST['rdv_kine_telephone']." [".tel_sans_caract_speciaux($_POST['rdv_kine_telephone'])."]\n";
        $msg_plaintext .= 'Email: '.$_POST['email']."\n";
        $msg_plaintext .= 'Confirmation par: '.$_POST['mode_confirm']."\n";
        $msg_plaintext .= "\n";
        $msg_plaintext .= 'Message:'."\n\n".$_POST['rdv_kine_message']."\n";
        
        $notif_messages = array(
            "confirmation" => array(
                "✅ Confirmation de rendez-vous",
                "✅ Bonjour votre rendez-vous avec ".$_POST['therapeute']." le ".formate($_POST['jour'])." à ".$_POST['heure']." est confirmé.",
                "Bonjour,\n\nVotre rendez-vous avec ".$_POST['therapeute']." le ".formate($_POST['jour'])." à ".$_POST['heure']." est confirmé.\n\nCordialement.\n"),
            "annulation" => array(
                "❗ Annulation de rendez-vous",
                "❗ Bonjour, votre demande de rendez-vous avec ".$_POST['therapeute']." le ".formate($_POST['jour'])." à ".$_POST['heure']." n'a pu être satisfaite et est annulée.",
                "Bonjour,\n\nVotre demande de rendez-vous avec ".$_POST['therapeute']." le ".formate($_POST['jour'])." à ".$_POST['heure']." n'a pu être satisfaite et est annulée.\n\nCordialement.\n"),
        );

        $msg_plaintext .=    "\n====\n";

        // Ajoute les URL pour notifier le patient par SMS.
        if (tel_est_mobile($_POST['rdv_kine_telephone'])) {
            $msg_plaintext .= "\n";
            $msg_plaintext .= build_url_notif_par_SMS(tel_sans_caract_speciaux($_POST['rdv_kine_telephone']), $notif_messages);
        }

        // Ajoute les URL pour notifier le patient par email.
        $msg_plaintext .= "\n";
        $msg_plaintext .= build_url_notif_par_mail(isset($_POST['email']) ? $_POST['email']: '', $notif_messages);
        
        // Start building header (From, Reply-To...)
        $headers ='From: '.$mail_configs['from']."\n";
        //$headers .='Reply-To: '.$mail_configs['reply_to']."\n";
        
        $do_encrypted_mail = $mail_configs['encrypt_mail'];
        if ($do_encrypted_mail) {
            $mess_array = build_encrypted_message($headers, $msg_plaintext);
        } else {
            $mess_array = build_plaintext_message($headers, $msg_plaintext);
        }
        $headers = $mess_array['headers'];
        $message = $mess_array['body'];
        
        // DEBUG
        /*
        echo "<pre>";
        echo htmlspecialchars($to);
        echo "\n";
        echo htmlspecialchars($subject);
        echo "\n";
        echo htmlspecialchars($headers);
        echo "\n";
        echo htmlspecialchars($message);
        echo "\n";
        echo "</pre>";
        */
        
        // Envoie la demande de rdv au thérapeute
        $mail_ret = mail($to, $subject, $message, $headers);
        unset ($to, $subject, $message, $headers);
        
        if ($mail_ret) {
            //echo 'Le message a bien été envoyé';
            
            // Mettre le rdv au status "1" dans la base de données.
            // instantiate database
            $database = new Database();
            $db = $database->getConnection();

            $query = "UPDATE seances
                    SET status = 1
                    WHERE therapeute = ?
                    and jour = ?
                    and heure = ?";

            //var_dump($query);

            // prepare query statement
            $stmt = $db->prepare($query);
            
            $stmt->bindParam(1, $_POST['th_code']);
            $stmt->bindParam(2, $_POST['jour']);
            $stmt->bindParam(3, $_POST['heure']);
            
            // execute query
            $stmt->execute();
            
            // Envoyer copie de la demande de rdv au patient
            if (isset($_POST['email']) && $_POST['email'] != '' &&
                isset($_POST['copie_par_mail']) && $_POST['copie_par_mail'] != '') {
                $to = $_POST['email'];
                
                $subject = 'Demande de rendez-vous envoyée';
                $subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
                
                $headers ='From: '.$mail_configs['from']."\n";
                // Copie cachée au thérapeute (pour vérifier que les utilisateurs ne se font pas spammer...)
                $headers .= 'Bcc: '.$mail_configs['to_complet']."\n";
                
                $msg_plaintext = "Bonjour,\n\nVotre demande de rendez-vous avec ".$_POST['therapeute']." le ".formate($_POST['jour'])." à ".$_POST['heure']." a bien été envoyée.\nAttention, votre rendez-vous n'est pas confirmé pour le moment.\n\nCordialement.\n";
                $mess_array = build_plaintext_message($headers, $msg_plaintext);
                $headers = $mess_array['headers'];
                $message = $mess_array['body'];
                
                $mail_ret2 = mail($to, $subject, $message, $headers);
            }
            
            // Redirection vers la page de confirmation d'envoi.
            header( "Location: prendre_rdv_confirm1.php");
            //header( "Refresh:10; url=prendre_rdv.php", true, 303);
            exit;
        } else {
            //echo 'Le message n\'a pu être envoyé';
            header( "Location: prendre_rdv_confirm2.php");
            exit;
        }
    }
}
?>

<form name="prendre_rdv" action="prendre_rdv2.php" method="POST">

<p>Veuillez remplir le formulaire pré-complété ci-dessous et le valider avec le bouton au bas de la page.</p>

<fieldset>
<p>Demande de rendez-vous avec 

<input type="hidden" name="therapeute" autocomplete="off" value="<?php echo $_POST['therapeute']; ?>">
<em class="th"><?php echo $_POST['therapeute'];?></em>

 le 
<input type="hidden" name="jour" autocomplete="off" value="<?php echo $_POST['jour']; ?>">
<?php echo formate($_POST['jour']);?>

 à 
<input type="hidden" name="heure" autocomplete="off" value="<?php echo $_POST['heure']; ?>">
<?php echo $_POST['heure'];?>.

<input type="hidden" name="status" autocomplete="off" value="<?php echo $_POST['status']; ?>">
<?php //echo $_POST['status'];?>
<input type="hidden" name="th_code" autocomplete="off" value="<?php echo $_POST['th_code']; ?>">
</p>

<p><label for="rdv_kine_telephone">Numéro de téléphone pour confirmer le rendez-vous <strong>(obligatoire)</strong></label><br>
<input type="tel" id="rdv_kine_telephone" name="rdv_kine_telephone" autocomplete="off" placeholder="06 12 34 56 78" pattern="\+?[0-9\s\./,-]+" size=25 maxlength="40" required value="<?php if(isset($_POST['rdv_kine_telephone'])) echo $_POST['rdv_kine_telephone']; ?>"></p>

<p><label for="rdv_kine_nom_prenom">NOM &amp; Prénom</label><br>
<input type="text" id="rdv_kine_nom_prenom" name="rdv_kine_nom_prenom" autocomplete="off" placeholder="NOM Prénom" size=50 maxlength="50" value="<?php if(isset($_POST['rdv_kine_nom_prenom'])) echo $_POST['rdv_kine_nom_prenom']; ?>">
</p>

<p><label for="rdv_kine_date_naissance">Date de naissance</label><br>
<input type="text" id="rdv_kine_date_naissance" name="rdv_kine_date_naissance" autocomplete="off" placeholder="JJ/MM/AAAA" pattern="[0-9]{1,2}[\./-][0-9]{1,2}[\./-][0-9]{2,4}" size=10 maxlength="10" value="<?php if(isset($_POST['rdv_kine_date_naissance'])) echo $_POST['rdv_kine_date_naissance']; ?>">
</p>

<p><label for="rdv_kine_message">Votre message</label><br>
<textarea cols="40" rows="7" id="rdv_kine_message" name="rdv_kine_message" placeholder="Si vous souhaitez ajouter un message, veuillez le faire ici.">
<?php if(isset($_POST['rdv_kine_message'])) echo $_POST['rdv_kine_message']; ?>
</textarea>
</p>

<p><label for="email">Courriel</label><br>
<input type="email" id="email" name="email" placeholder="courriel@domaine.tld" size=40 maxlength="50" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
</p>

<p>Recevoir la confirmation du rendez-vous par<br>
<input type="radio" id="mode_confirm_email" name="mode_confirm" value="email" <?php if(isset($_POST['mode_confirm']) && $_POST['mode_confirm']=='email') echo 'checked' ?> onclick="set_email_input_required('mode_confirm','#email')">
<label for="mode_confirm_email">courriel</label><br>
<input type="radio" id="mode_confirm_tel" name="mode_confirm" value="tel" <?php if(!isset($_POST['mode_confirm']) || $_POST['mode_confirm']=='tel') echo 'checked' ?> onclick="set_email_input_required('mode_confirm','#email')">
<label for="mode_confirm_tel">téléphone (SMS*)</label><br>
<input type="radio" id="mode_confirm_email_tel" name="mode_confirm" value="email_tel" <?php if(isset($_POST['mode_confirm']) && $_POST['mode_confirm']=='email_tel') echo 'checked' ?> onclick="set_email_input_required('mode_confirm','#email')">
<label for="mode_confirm_email_tel">courriel &amp; téléphone (SMS*)</label>
</p><?php 

    if (isset($configs['activer_copie_demande_rdv_par_mail']) && $configs['activer_copie_demande_rdv_par_mail']) { 
?><p><input type="checkbox" id="copie_par_mail" name="copie_par_mail" <?php if(isset($_POST['copie_par_mail'])) echo 'checked' ?>>
<label for="copie_par_mail">Recevoir une copie de ma demande par courriel.<sup><small>✻</small></sup></label></p><?php }

?><input type="hidden" name="timestamp" autocomplete="off" value="<?php 
if (!isset( $_POST['timestamp'])) echo time(); else echo $_POST['timestamp']; 
?>">

<p style="display: none"><label for="email_field">Veuillez laisser ce champ vide</label><br><input type="text" id="email_field" name="email_field" autocomplete="off" placeholder="Ne pas remplir" value="<?php if(isset($_POST['email_field'])) echo $_POST['email_field']; ?>"></p>

</fieldset>

<p><small>Les données personnelles de ce formulaire sont transmises sous forme chiffrée et ne peuvent être déchiffrées que par votre thérapeute.
<br>Aucune information à caractère personnel n'est stockée sur ce site.</small></p>

<p><input type="submit" name="submit_rdv" value="Valider ma demande">
</p>
</form>
<p><small>* Si votre demande ne nécessite pas de traitement particulier et que vous avez renseigné un n° de téléphone mobile, vous recevrez plutôt un SMS.</small></p>
<?php if (isset($configs['activer_copie_demande_rdv_par_mail']) && $configs['activer_copie_demande_rdv_par_mail']) {  ?>
<p><small><sup><small>✻</small></sup> Si vous avez demandé une copie par courriel, votre copie n'est pas chiffrée. Seuls y figurent le nom du thérapeute, le jour et l'heure de rendez-vous.</small></p>
<?php } ?>
    <p>&nbsp;</p>

<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'; ?>
</body>

</html>
