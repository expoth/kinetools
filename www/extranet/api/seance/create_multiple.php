<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once $_SERVER['DOCUMENT_ROOT'].'/tools/database.php';

// instantiate seance object
include_once $_SERVER['DOCUMENT_ROOT'].'/api/objects/seance.php';

include_once $_SERVER['DOCUMENT_ROOT'].'/api/shared/auth.php';

$database = new Database();
$db = $database->getConnection();
 
$seance = new Seance($db);

// get posted data
$all_data = json_decode(file_get_contents("php://input"));

if (!check_auth($all_data))
    exit;

//var_dump($all_data);
//echo "aa: ".sizeof($all_data);
$counter=0;
// make sure data is not empty
if( sizeof($all_data) > 0) {
    foreach ($all_data as $data) {
        if( 
        !empty($data->therapeute) &&
        !empty($data->jour) &&
        !empty($data->heure) &&
        isset($data->status)
        ) {

        
            // set product property values
            $seance->therapeute = $data->therapeute;
            $seance->jour = $data->jour;
            $seance->heure = $data->heure;
            $seance->status = $data->status;
            //$seance->created = date('Y-m-d H:i:s');
        
            // create the product
            if($seance->create()){
                $counter++;
                // set response code - 201 created
                //http_response_code(201);
        
                // tell the user
                //echo json_encode(array("message" => "Seance was created."));
            }
        
            // if unable to create the product, tell the user
            else{
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                echo json_encode(array("message" => "Unable to create at least one seance. Stopping here. ".$counter." seances were created /".sizeof($all_data)."."));
                break;
            }
        } else {
            http_response_code(400);
            echo json_encode(array("message" => "Unable to create at least one seance. Data is incomplete. Stopping here. ".$counter. " seances were created /".sizeof($all_data)."."));
            break; 
        }

    }
    if ($counter == sizeof($all_data)) {
        // set response code - 201 created
        http_response_code(201);
        
        // tell the user
        echo json_encode(array("message" => $counter." seances were created /".sizeof($all_data)."."));
    }
} else {
    // set response code - 400 bad request
    http_response_code(400);
        
    // tell the user
    echo json_encode(array("message" => "Unable to create seance. No data submitted."));
}
 
// tell the user data is incomplete


/* Exemple JSON:
[{"therapeute":"THERAP1","jour":"2019-12-03","heure":"10:00","status":0},{"therapeute":"THERAP1","jour":"2019-12-03","heure":"10:00","status":0}]
*/
?>
