<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
class Seance{
 
    // database connection and table name
    private $conn;
    private $table_name = "seances";
 
    // object properties
    public $therapeute;
    public $jour;
    public $heure;
    public $status;
    public $jourDebut;
    public $jourFin;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    // read products
    function read(){
    
        // select all query
        $query = "SELECT therapeute, jour, heure, status FROM
                    " . $this->table_name . " ";

        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }
    
    // create product
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "(therapeute, jour, heure, status)
                VALUES
                    (:therapeute, :jour, :heure, :status)";

        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->therapeute=htmlspecialchars(strip_tags($this->therapeute));
        $this->jour=htmlspecialchars(strip_tags($this->jour));
        $this->heure=htmlspecialchars(strip_tags($this->heure));
        $this->status=htmlspecialchars(strip_tags($this->status));
    
        // bind values
        $stmt->bindParam(":therapeute", $this->therapeute);
        $stmt->bindParam(":jour", $this->jour);
        $stmt->bindParam(":heure", $this->heure);
        $stmt->bindParam(":status", $this->status);

        // execute query
        // Essai d'utilsier les transactions, mais n'a pas améliorer les performances/lock
//         $this->conn->beginTransaction();
//         $ret = $stmt->execute();
//         $this->conn->commit();
//         if($ret){
//             return true;
//         }   

        // Ancienne méthode (sans les transactions)
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    // delete the product
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE therapeute = ? and jour >= ? and jour <= ?";

        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->therapeute=htmlspecialchars(strip_tags($this->therapeute));
        $this->jourDebut=htmlspecialchars(strip_tags($this->jourDebut));
        $this->jourFin=htmlspecialchars(strip_tags($this->jourFin));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->therapeute);
        $stmt->bindParam(2, $this->jourDebut);
        $stmt->bindParam(3, $this->jourFin);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }
}
?>
