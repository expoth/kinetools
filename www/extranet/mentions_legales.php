<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mentions légales</title>
    <script src="/css/script.js"></script>
    <style>
        @import url(/css/main.css);
    </style>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/nav.php'; ?>
<?php $coordonnees = include($_SERVER['DOCUMENT_ROOT'].'/config/coordonnees.php'); ?>
    <h2>Mentions légales</h2>

    <h3>Identification</h3>
    <p><?php echo $coordonnees['raison_sociale']; ?><br>
    <?php echo $coordonnees['adresse1'].' - '.$coordonnees['adresse2']; ?> - France<br>
    <?php echo $coordonnees['tel']; ?></p>
    <?php // Rajouter éventuellement un mail ?>
    <?php //Forme juridique de la société (SA, SARL, SNC, SAS, etc.)
        //Montant du capital social ?>  
    <p>Directeur⋅rice⋅s de la publication: <a href="/#therapeutes">les masseurs-kinésithérapeutes</a> du cabinet.</p>
    <p>Hébergeur: <?php echo $coordonnees['hebergeur']; ?></p>

    <h3>Activité</h3>
<?php  /*
    Pour une activité réglementée, le site internet doit mentionner les informations suivantes :
    Référence aux règles professionnelles applicables
    Indication du titre professionnel
    Nom de l’État de l'Union européenne dans lequel le titre professionnel a été octroyé
*/ ?>
    <p>Les masseurs-kinésithérapeutes sont inscrits auprès de l'ordre des masseurs-kinésithérapeutes.</p>
    <h3>Utilisation de cookies</h3>
    <p>Ce site ne fait pas usage de cookies.</p>
    
    <h3>Utilisation de données personnelles</h3>
    <p>Les informations que vous transmettez dans le formulaire de prise de rendez-vous 
    ont pour finalité de vous identifier et de vous recontacter afin de confirmer ou non
    le rendez-vous demandé. Le cas échéant elles pourront servir à mettre à jour vos 
    coordonnées dans notre logiciel métier. Vous disposez d'un droit d'opposition, 
    d'interrogation, d'accès et de rectification de vos données. Vous pouvez exercer
    ce droit auprès des thérapeutes du cabinet. Vous disposez aussi du droit d'introduire
    une réclamation auprès de la Cnil.</p>
    
<?php 
    $inc_file = $_SERVER['DOCUMENT_ROOT'].'/inc/mentions_legales_contenu_tiers.php';
    if (file_exists ($inc_file)) include $inc_file;
?>
    
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'; ?>
</body>

</html>
 
