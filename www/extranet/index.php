<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cabinet de masso-kinésithérapie</title>
<?php 
    $favicon_inc_file = $_SERVER['DOCUMENT_ROOT'].'/inc/favicon.php';
    if (file_exists ($favicon_inc_file)) include $favicon_inc_file;
?>
    <script src="/css/script.js"></script>
    <style>
        @import url(/css/main.css);
        form, form input[type=submit] { display:inline; }
        form input[type=submit] { display:inline; color:darkblue}
    </style>
</head>
<?php
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/tools.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/th_csv.php';
    $coordonnees = include($_SERVER['DOCUMENT_ROOT'].'/config/coordonnees.php');
    
?>
<body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/nav.php'; ?>


<aside>
    <h2>Bloc-notes</h2>
    <h4><span class="emoji tel">📞 </span><a href="tel:<?php echo tel_to_intl($coordonnees['tel']); ?>"><?php echo $coordonnees['tel']; ?></a></h4>
    <h4><span class="emoji">📆 </span><a href="/prendre_rdv/prendre_rdv.php">Prendre rendez-vous</a></h4>
</aside>

<div id="main-content">
<section>
    <h2 id="coordonnees">Coordon<wbr>nées</h2>
    <strong><span class="emoji">📇 </span>Adresse</strong><address> <?php echo $coordonnees['adresse1'].'<br>'.$coordonnees['adresse2']; ?></address>
    <p><strong><span class="emoji tel">📞 </span>Téléphone</strong><br><a href="tel:<?php echo tel_to_intl($coordonnees['tel']); ?>"><?php echo $coordonnees['tel']; ?></a></p>
</section>
<section>
    <h2 id="therapeutes">Les thérapeutes<span class="emoji th"> 👩‍⚕️ 👨‍⚕️</span></h2>
    <ul>
    
<?php
    // instantiate database
    include_once $_SERVER['DOCUMENT_ROOT'].'/tools/database.php';
    $database = new Database();
    $db = $database->getConnection();

    $ths_obj = new therapeutes();
    $ths = $ths_obj->get_therapeutes();
    foreach ($ths as $th) {

        // REQUETE DES THÉRAPEUTES
        $query = 'SELECT jour, heure, jour || "_" || heure as jour_heure 
                    FROM seances
                    WHERE therapeute = "'.$th['code'].'"
                    and status = 0
                    and jour_heure >= "'.rdv_futurs_uniqt_now_JH().'"
                    ORDER BY jour, heure 
                    LIMIT 1';
        //var_dump($query_th);
        // prepare query statement
        $stmt = $db->prepare($query);
        // execute query
        $stmt->execute();
        $rdv_prochain = "";
        unset ($rdv_jour);
        unset ($rdv_heure);
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $rdv_prochain = formate($row['jour']).' à '.$row['heure'];
            $rdv_jour=$row['jour'];
            $rdv_heure=$row['heure'];
            break;
        }

        echo '<li class="th">';

        $url_th = therapeutes::url_page_indiv_therapeute($th['url']);
        if ($url_th) {
            echo '<a href="'.$url_th.'">';
        }

        echo '<em>'.therapeutes::get_civilite($th['sexe'])." ".$th['nom']." ".$th['prenom'].'</em>';
        echo ', <small>'.$th['titre'].'</small>';
        
        if ($url_th) {
            echo '</a>';
        }

        $th_np = therapeutes::get_therapeute_nom_prenom_str($th['nom'],$th['prenom']);
        $url_rdv_therapeute = urlencode($th_np);
        $url_rdv_therapeute_q = urlencode(base64_encode($th['code']));
        

        //$rdv_prochain = get_prochain_rdv($th['code']);
        //$rdv_prochain = "mar 01.01.01 à 13h50";
        if ($th['site_rdv_nom'] == "integre" || $th['site_rdv_nom'] == "") {
            if ($rdv_prochain) {
                echo '<div>⋅ Prochaine disponibilité: '.$rdv_prochain.' ';
                echo '[';
                echo_form_rdv($th['code'], $th_np, $rdv_jour, $rdv_heure, 0, true, true, true);
                echo ' | ';
                echo '...<a href="/prendre_rdv/prendre_rdv.php?th='.$url_rdv_therapeute_q.'#'.$url_rdv_therapeute.'">voir les suivants</a>]</div>';
            }
        } else {
            if ($th['site_rdv_nom'] != "" && $th['site_rdv_url'] != "") {
                    echo '<div>⋅ Prendre rendez-vous avec <a href="'.$th['site_rdv_url'].'"><img src="css/'.$th['site_rdv_nom'].'.png" alt="'.$th['site_rdv_nom'].'" style="vertical-align: text-bottom"></a></div>';
            }
        }
        echo '<details>';
        echo '<br class="hide"><summary><span class="hide">⋅ </span>Horaires de rendez-vous</summary>';
        echo '<p>';
        $i = 0;
        foreach (
            array(  'lun' => 'Lundi',
                    'mar' => 'Mardi',
                    'mer' => 'Mercredi',
                    'jeu' => 'Jeudi',
                    'ven' => 'Vendredi',
                    'sam' => 'Samedi') as $k => $jour) {
            if ($th[$k]) {
                if ($i > 0) echo "<br>";
                $i++;
                //echo "$jour: ".$th[$k];
                echo "&nbsp;- $jour: ".$ths_obj->get_horaire_jour_texte($th['code'],$k);
            }
        }
        echo '</p>';
        echo '</details>';
        echo '</li>';
    }
?>
    </ul>
</section>

</div>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'; ?>
</body>
</html>
