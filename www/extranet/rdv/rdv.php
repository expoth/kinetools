<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
?>
<?php 

include_once $_SERVER['DOCUMENT_ROOT'].'/tools/tools.php';
$configs = include($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

$csv_file = $configs['prochains_rdv_csv_location'];

// Mettre à true si on veut convertir l'id dans la "url_query" de base36 -> base10
// Car le fichier stocke en base 10, mais dans l'URL on met base36 pour raccourcir et cacher
$convert_url_query = false;


if (isset($_POST) && isset($_POST['id']) && $_POST['id'] != '') {
    $id_in_file = $_POST['id'];
} else {
    $id_in_file = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
}


if ($convert_url_query) {
    //Version avec id caché : ie. stocké en base10 dans le fichier mais en base36 dans l'URL
    $id_in_file = base_convert($id_in_file,36,10);
}

// Ouvre le fichier CSV
//$fileHandle = fopen($csv_file, "r");
$csv_in_memory = readCSV($csv_file);

if (isset($_POST) && isset($_POST['get_ics'])) {
    /****************************************************************************************/
    // Build ICS
    include $_SERVER['DOCUMENT_ROOT'].'/tools-3rdparty/ICS.php';

    //header('Content-Type: text/calendar; charset=utf-8');
    // https://www.ietf.org/rfc/rfc5545.txt cf p149 pour l'ajout de "method=PUBLISH; component=VEVENT"
    header('Content-Type: text/calendar; charset=utf-8; method=PUBLISH; component=VEVENT');
    header('Content-Disposition: attachment; filename=rdv.ics');

    foreach ($csv_in_memory as $row) {
        //Dump out the row for the sake of clarity.
        if ($id_in_file == $row[0] && 
            $_POST['date'] == $row[1] && 
            $_POST['heure'] == $row[2]) {

            $dtstart =  DateTime::createFromFormat("Y-m-d H:i",$row[1]." ".$row[2]);
            //var_dump($dtstart);
            $dtend = DateTime::createFromFormat("Y-m-d H:i",$row[1]." ".$row[2]);
            $duree_soin = 30;
            $dtend->add(new DateInterval( 'PT' . $duree_soin . 'M' ) );
            //var_dump($dtend);
            $ics = new ICS(array(
                //'location' => '',
                'description' => $configs['ICS_description'],
                //'dtstart' => $row[1]." ".$row[2],
                //'dtstart' => strtotime($dtstart),
                'dtstart' => $dtstart,
                'dtend' => $dtend,
                'alarm' => '15M', // Alarme 15min avant le début
                'summary' => $configs['ICS_summary'],
                'url' => $configs['extranet_base_url'].$_SERVER['REQUEST_URI'].'?'.$_POST['id'],
                'organizer' => $configs['ICS_organizer'],
                    ));
        }
    }

    echo $ics->to_string();
    exit;
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vos prochains rendez-vous</title>
    <script src="/css/script.js"></script>
    <style>
        @import url(/css/main.css);
    </style>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/header.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/nav.php'; ?>

    <h2>Vos prochains rendez-vous</h2>
    <ul>
<?php

/****************************************************************************************/
// Affiche les rdv ligne par ligne 
//Loop through the CSV rows.
foreach ($csv_in_memory as $row) {
    //Dump out the row for the sake of clarity.
    if ($id_in_file == $row[0]) {
    echo '<li>';
    echo '<time datetime="'.$row[1].'T'.$row[2].'">' . formate($row[1])  . ' à ' . $row[2] . '</time>';
    echo '<form name="form_ics" action="rdv.php" method="POST" style="display:inline">';
    echo '<input type="hidden" name="id" value="'.parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY).'">';
    echo '<input type="hidden" name="date" value="'.$row[1].'">';
    echo '<input type="hidden" name="heure" value="'.$row[2].'">';
    echo ' <input type="submit" name="get_ics" value="💾📆 Télécharger (ics)">';
    echo '</form>';
    echo '</li>';
    }
}
?>
    </ul>
    <p>En téléchargeant les rendez-vous dans votre calendrier électronique, vous avez la possibilité de paramétrer un rappel, par exemple 15&nbsp;minutes avant le début de la séance.</p>
    <p>Cette page est personnelle, veuillez ne pas la partager.</p>
    <p>En cas d'erreur, veuillez prendre contact avec votre thérapeute.</p>
    <hr>
    <p><small>Dernière mise à jour des données le : <?php echo date ("Y-m-d à H:i:s.", filemtime($csv_file)) ?></small></p>
<?php include $_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'; ?>
</body>
</html>
