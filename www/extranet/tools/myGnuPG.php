<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
class myGnuPG {

    private $my_gpg;
    private $gnupghome, $fingerprint, $pubkey;

    public function __construct(){
        $gnupg_configs = include($_SERVER['DOCUMENT_ROOT'].'/config/gnupg_config.php');

        $this->gnupghome = $gnupg_configs['gnupghome'];

        $this->fingerprint = trim(file_get_contents($gnupg_configs['pubkey_fingerprint_file']));

        $this->pubkey = file_get_contents($gnupg_configs['pubkey_aex_file']);

        if ( ! $this->createPath($this->gnupghome)) {
            exit;
        }

        putenv("GNUPGHOME=$this->gnupghome");
        $this->my_gpg = new gnupg();
    }

    function createPath($path) {
        if (is_dir($path)) return true;
        $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
        $return = $this->createPath($prev_path);
        return ($return && is_writable($prev_path)) ? mkdir($path) : false;
    }

    public function import_pubkey() {
        if (! $this->my_gpg->keyinfo($this->fingerprint)) {
            $rtv = $this->my_gpg->import($this->pubkey);
            if (!$rtv) throw new Exception("Erreur de chiffrement n°3",3);
            //echo "gnupg_import RTV = <br/><pre>\n";
            //var_dump($rtv);
            //echo "</pre>\n";
        }
    }

    public function encrypt($message) {
        //putenv("GNUPGHOME=$this->gnupghome");
        $enc = (null);
        $this->import_pubkey();

        $rtv = $this->my_gpg->addencryptkey($this->fingerprint);
        if (!$rtv) throw new Exception("Erreur de chiffrement n°1",1);
        //echo "gnupg_addencryptkey RTV = <br /><pre>\n";
        //var_dump($rtv);
        //echo "</pre>\n";
        
        $enc = $this->my_gpg->encrypt($message);
        if (!$enc) throw new Exception("Erreur de chiffrement n°2",2);
        
        //echo "Encrypted Data: " . $enc . "<br/>";
        return $enc;
    }

}

?>
