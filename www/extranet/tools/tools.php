<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */

// Lun:1 Mar:2...
function jour_de_semaine($date)
{
setlocale(LC_ALL,'fr_FR.UTF-8');

//Convert the date string into a unix timestamp.
$unixTimestamp = strtotime($date);
 
//Get the day of the week using PHP's date function.
// https://www.php.net/manual/fr/function.date.php
# 1 (pour Lundi) à 7 (pour Dimanche)
$dayOfWeek = strftime("%u", $unixTimestamp);

    //Print out the day that our date fell on.
    return $dayOfWeek;
}

// Retourne: lun. mar. mer. ...
function jour_de_semaine_lun($date)
{
setlocale(LC_ALL,'fr_FR.UTF-8');

//Convert the date string into a unix timestamp.
$unixTimestamp = strtotime($date);
 
//Get the day of the week using PHP's date function.
// https://www.php.net/manual/fr/function.date.php

$dayOfWeek = strftime("%a", $unixTimestamp);

    //Print out the day that our date fell on.
    return $dayOfWeek;
}

// Retourne: lun. mar. mer. ...
function formate($date)
{
setlocale(LC_ALL,'fr_FR.UTF-8');

//Convert the date string into a unix timestamp.
$unixTimestamp = strtotime($date);
 
//Get the day of the week using PHP's date function.
// https://www.php.net/manual/fr/function.date.php

$dayOfWeek = strftime("%a %d %b %Y", $unixTimestamp);

    //Print out the day that our date fell on.
    return $dayOfWeek;
}

function getStartAndEndDate($year, $week)
{
   return [
      (new DateTime())->setISODate($year, $week)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week, 7)->format('Y-m-d') //end date
   ];
}

function getLunVenDates($year, $week)
{
   return [
      (new DateTime())->setISODate($year, $week)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,2)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,3)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,4)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,5)->format('Y-m-d') //start date
   ];
}

function getLunSamDates($year, $week)
{
   return [
      (new DateTime())->setISODate($year, $week)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,2)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,3)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,4)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,5)->format('Y-m-d'), //start date
      (new DateTime())->setISODate($year, $week,6)->format('Y-m-d') //start date
   ];
}

function getTodayYYYYMMAA()
{
   return (new DateTime())->format('Y-m-d');
}

function getYesterdayYYYYMMAA()
{
   return date('Y-m-d', time() - 60 * 60 * 24);
}

function getYYYYWWplusDays ($days_shift = 0) {
    return date('Y-W', time() + $days_shift * 60 * 60 * 24);
}

function getYYYYWWofTimestamp ($ts) {
    return date('Y-W', $ts);
}

// Retourne maintenant au format YYYY-MM-AA_HH:MM
// en ajoutant $addition (min) si renseigné.
function getNowYYYYMMAA_HHMM($addition_minutes = 0)
{
    if ($addition_minutes != 0)
        return (new DateTime())->format('Y-m-d_H:i');
    else
        return date('Y-m-d_H:i', time() + $addition_minutes * 60);
}

// Retourne maintenant au format YYYY-MM-AA_HH:MM
function getNowHHMM()
{
   return (new DateTime())->format('H:i');
}

function tel_est_mobile($num) {
    $tmp_num = tel_sans_caract_speciaux($num);
    //echo $tmp_num;
    if ( (strlen($tmp_num) == 10 and preg_match('/^0[6|7]/',$tmp_num) ) or
         (strlen($tmp_num) == 12 and preg_match('/^\+33[6|7]/',$tmp_num) ) or 
         (strlen($tmp_num) == 13 and preg_match('/^0033[6|7]/',$tmp_num) ) ) {
        //echo "\n$num est mobile ($tmp_num)\n";
        return true;
    } else {
        //echo "\n$num est PAS mobile ($tmp_num)\n";
        return false;
    }
}

function tel_sans_caract_speciaux($str) {
    return preg_replace('/[^0-9\+]/', '', $str);
}

function tel_to_intl($str) {
    $t = tel_sans_caract_speciaux($str);
    $ret = preg_match('/^0([0-9\+]{9})$/', $t, $matches);
    if ($ret === false)
        return $t;
    else
        return '+33'.$matches[1];
}

function display_cabinet() {
    if (file_exists($_SERVER['DOCUMENT_ROOT'].'/cabinet.php'))
        return true;
    else
        return false;
}

function rdv_futurs_uniqt() {
    $configs = include($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
    return ($configs['rdv_futurs_uniqt']);
}

// Retourne auj au format 2019-01-01_08:00 si on ne veut que les rdv futurs, et une autre date passée sinon.
function rdv_futurs_uniqt_now_JH() {
    $configs = include($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
    if (rdv_futurs_uniqt()) {
        return getNowYYYYMMAA_HHMM($configs['cacher_rdv_x_min_avant_debut']);
    } else {
        return "2019-12-02_08:00";
    }
}

// Fonction qui affiche le formulaire en HTML
function echo_form_rdv($th_code, $th_np, $jour, $heure, $status, $preciser_th = false, $preciser_h = true, $juste_bouton = false) {
?>
    <form name="prendre_rdv" action="/prendre_rdv/prendre_rdv2.php" method="POST">
    <?php if (!$juste_bouton) { ?>
    <label><?php if ($preciser_h) echo $heure.' ';
                if ($status == 0) echo '<span class="emoji check">✅</span> Libre';
                else echo '<span class="emoji cross">❌</span> Déjà réservé';
?>
    </label>
<?php } ?>
    <input type="hidden" name="therapeute" value="<?php echo $th_np ?>">
    <input type="hidden" name="jour" value="<?php echo $jour ?>">
    <input type="hidden" name="heure" value="<?php echo $heure ?>">
    <input type="hidden" name="status" value="<?php echo $status ?>">
    <input type="hidden" name="th_code" value="<?php echo $th_code ?>">
    <input type="submit" value="<?php
        if ($status == 0) echo 'le réserver';
        else echo 'Réserver malgré tout'; ?>">
<?php
    if (!$juste_bouton) { 
        if ($preciser_th === true) echo ' <small> avec <em class="th">'.$th_np.'</em></small>';
    }
?>
    </form>
<?php
} // Fin function echo_form_rdv

function readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    if ($file_handle == false) {
        die ("<p><br>Le fichier n'a pas pu être ouvert: ".$csvFile);
    }
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 0, "\t", "\"");
    }
    fclose($file_handle);
    return $line_of_text;
}

?>
