<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */

class therapeutes {
    
    private $csv_therapeutes;
    public $therapeutes = array();
    
    function __construct() {
        $this->csv_therapeutes = $_SERVER['DOCUMENT_ROOT'].'/config/therapeutes.csv';
        if (!file_exists($this->csv_therapeutes))
            die ("Fichier nécessaire manquant:".$this->csv_therapeutes);
        $fileHandle = fopen($this->csv_therapeutes, "r");
        
        while (($row = fgetcsv($fileHandle, 0, ";", "\"")) !== FALSE) {
            //Dump out the row for the sake of clarity.
            
            if (strtolower($row[0]) === strtolower("actif")) {
                $th = array(    'code' => "$row[1]",
                                'nom' => "$row[2]",
                                'prenom' => "$row[3]",
                                'sexe' => "$row[4]",
                                'titre' => "$row[5]",
                                'url' => "$row[6]",
                                'lun' => "$row[7]",
                                'mar' => "$row[8]",
                                'mer' => "$row[9]",
                                'jeu' => "$row[10]",
                                'ven' => "$row[11]",
                                'sam' => "$row[12]",
                                'hor_agenda' => "$row[13]",
                                'duree_rdv' => "$row[14]",
                );
                $th['site_rdv_nom'] = (isset($row[15])) ? $row[15] : "";
                $th['site_rdv_url'] = (isset($row[16])) ? $row[16] : "";
                array_push( $this->therapeutes, $th);
                unset ($th);
            }
        }
    }
    
    // Retourne le tableau contenant tous les thérapeutes
    // avec leurs informations.
    function get_therapeutes() {
        return $this->therapeutes;
    }
    
    static function get_civilite($sexe) {
        switch (strtolower($sexe)) {
            case 'm':
                return 'M.';
            case 'f':
                return 'Mme';
            default:
                return '';
        }
    }

    function get_therapeute_civil_nom_prenom($code_th) {
        $result = "inconnu";
        foreach ($this->therapeutes as $th) {
            if (strtolower($th['code']) === (strtolower($code_th))) {
                $result = $this->get_civilite($th['sexe'])." ".$th['nom']." ".$th['prenom'];
                break;
            }
        }
        return $result;
    }

    // En entrée le code thérapeute comme dans le logiciel de gestion $th['code']
    function get_therapeute_nom_prenom($code_th) {
        $result = "inconnu";
        foreach ($this->therapeutes as $th) {
            if (strtolower($th['code']) === (strtolower($code_th))) {
                $result = $th['nom']." ".$th['prenom'];
                break;
            }
        }
        return $result;
    }

    // En entrée le code thérapeute comme dans le logiciel de gestion $th['code']
    function therapeute_utilise_site_rdv_externe($code_th) {
        foreach ($this->therapeutes as $th) {
            if (strtolower($th['code']) === (strtolower($code_th)) && strtolower($th['site_rdv_nom']) != "" && strtolower($th['site_rdv_nom']) != "integre" && strtolower($th['site_rdv_url']) != "") {
                return TRUE;
            }
        }
        return FALSE;
    }

    function get_therapeute_site_rdv_nom($code_th) {
        foreach ($this->therapeutes as $th) {
            if (strtolower($th['code']) === (strtolower($code_th))) {
                return $th['site_rdv_nom'];
            }
        }
        return "";
    }

    function get_therapeute_site_rdv_url($code_th) {
        foreach ($this->therapeutes as $th) {
            if (strtolower($th['code']) === (strtolower($code_th))) {
                return $th['site_rdv_url'];
            }
        }
        return "";
    }

    // En entrée le nom et le prénom
    static function get_therapeute_nom_prenom_str($nom, $prenom) {
        return $nom." ".$prenom;
    }


    static function url_page_indiv_therapeute($th) {
        $url = '/therapeute/'.strtolower($th);
        if ($th && file_exists($_SERVER['DOCUMENT_ROOT'].$url))
            return $url;
        else
            return false;
    }
    
    function get_horaire_jour($code_th, $jour) {
        $h = "--";
        foreach ($this->therapeutes as $th) {
            if (strtolower($th['code']) === (strtolower($code_th))) {
                //var_dump($th);
                switch (strtolower($jour)) {
                    case "lun":
                        $h = $th['lun'];
                        break;
                    case "mar":
                        $h = $th['mar'];
                        break;
                    case "mer":
                        $h = $th['mer'];
                        break;
                    case "jeu":
                        $h = $th['jeu'];
                        break;
                    case "ven":
                        $h = $th['ven'];
                        break;
                    case "sam":
                        $h = $th['sam'];
                        break;
                    case "dim":
                        $h = $th['dim'];
                        break;
                    case "hor_agenda":
                        $h = $th['hor_agenda'];
                        break;
                    default;
                        $h = "--";
                        break;
                }
            }
        }
        
        $reg_part = '([0-9]{1,2})[:h]([0-9]{1,2})[\s–\-]{0,3}([0-9]{1,2})[:h]([0-9]{1,2})';
        
        // Cherche les horaires de type 08:00-12:00,14:00-18:00
        $ret = preg_match('/^'.$reg_part.'[\s,]{1,2}'.$reg_part.'$/', $h, $matches, PREG_UNMATCHED_AS_NULL);
        if ($ret) {
            //var_dump($matches);
            $c = sprintf('%02d', (int)$matches[1]).':'.
                    sprintf('%02d', (int)$matches[2]).' - '.
                    sprintf('%02d', (int)$matches[3]).':'.
                    sprintf('%02d', (int)$matches[4]).' ; '.
                    sprintf('%02d', (int)$matches[5]).':'.
                    sprintf('%02d', (int)$matches[6]).' - '.
                    sprintf('%02d', (int)$matches[7]).':'.
                    sprintf('%02d', (int)$matches[8]);
            return array (
                'complet' => $c,
                'matin_deb_hh' => intval($matches[1],10),
                'matin_deb_mm' => intval($matches[2],10),
                'matin_fin_hh' => intval($matches[3],10),
                'matin_fin_mm' => intval($matches[4],10),
                'aprem_deb_hh' => intval($matches[5],10),
                'aprem_deb_mm' => intval($matches[6],10),
                'aprem_fin_hh' => intval($matches[7],10),
                'aprem_fin_mm' => intval($matches[8],10),
            );
        }
        
        // Cherche les horaires de type 08:00-18:00
        $ret = preg_match('/^'.$reg_part.'$/', $h, $matches, PREG_UNMATCHED_AS_NULL);
        if ($ret) {
            return array(
                'complet' => sprintf('%02d', (int)$matches[1]).':'.
                    sprintf('%02d', (int)$matches[2]).' - '.
                    sprintf('%02d', (int)$matches[3]).':'.
                    sprintf('%02d', (int)$matches[4]),
                'matin_deb_hh' => intval($matches[1],10),
                'matin_deb_mm' => intval($matches[2],10),
                'matin_fin_hh' => '--',
                'matin_fin_mm' => '--',
                'aprem_deb_hh' => '--',
                'aprem_deb_mm' => '--',
                'aprem_fin_hh' => intval($matches[3],10),
                'aprem_fin_mm' => intval($matches[4],10),
            );
        } 

        return array(
                'complet' => '-',
                'matin_deb_hh' => '--',
                'matin_deb_mm' => '--',
                'matin_fin_hh' => '--',
                'matin_fin_mm' => '--',
                'aprem_deb_hh' => '--',
                'aprem_deb_mm' => '--',
                'aprem_fin_hh' => '--',
                'aprem_fin_mm' => '--',);
    }
    
    function get_horaire_debut_matin_min($code_th, $jour) {
        $h = $this->get_horaire_jour($code_th, $jour);
        if ($h['matin_deb_hh'] === '--')
            // Valeur par défaut
            return 0;
        else
            return ($h['matin_deb_hh']*60 + $h['matin_deb_mm']);
    }
    function get_horaire_fin_matin_min($code_th, $jour) {
        $h = $this->get_horaire_jour($code_th, $jour);
        return ($h['matin_fin_hh']*60 + $h['matin_fin_mm']);
    }
    
    function get_horaire_debut_aprem_min($code_th, $jour) {
        $h = $this->get_horaire_jour($code_th, $jour);
        return ($h['aprem_deb_hh']*60 + $h['aprem_deb_mm']);
    }
    function get_horaire_fin_aprem_min($code_th, $jour) {
        $h = $this->get_horaire_jour($code_th, $jour);
        if ($h['aprem_fin_hh'] == '--')
            // Valeur par défaut
            return 24*60;
        else
            return ($h['aprem_fin_hh']*60 + $h['aprem_fin_mm']);
    }
    
    function get_horaire_jour_texte ($code_th, $jour) {
        return ($this->get_horaire_jour($code_th, $jour))['complet'];
    }
    
    function get_duree_rdv($code_th) {
        foreach ($this->therapeutes as $th) {
            $val = $th['duree_rdv'];
            if (strtolower($th['code']) === (strtolower($code_th))) {
                if (($val) != "") {
                    //if (60 % $val != 0) {
                    //    throw new Exception("la durée $val n'est pas un multiple de 60 (reste ".(60 % $val)."). Cela posera des problèmes dans la vue agenda (les heures de rdv ne commenceront pas à l'heure du 1er rdv)", 2);
                    //}
                    return (int)$val;
                }
            }
        }
        throw new Exception("Durée des rdv non configurée dans ".$this->csv_therapeutes,1);
    }
}

?>
