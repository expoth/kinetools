<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */
class Database {

    private $configs;

    // specify your own database credentials
    private $db_type;
    private $sqlite3_db;
    private $host, $db_name, $username, $password;

    public $conn;

    public function __construct(){
        $this->configs = include($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

        $this->db_type = $this->configs['db_type'];

        $this->sqlite3_db = $this->configs['sqlite3_db'];

        $this->host = $this->configs['host'];
        $this->db_name = $this->configs['db_name'];
        $this->username = $this->configs['username'];
        $this->password = $this->configs['password'];
    }

    function __destruct() {
        if ($this->conn != null) {
            try {
                if (isset($this->configs['sqlite3_vacuum_timestamp_file_location']) && $this->configs['sqlite3_vacuum_timestamp_file_location'] !== '') {
                    $ts_file = $this->configs['sqlite3_vacuum_timestamp_file_location'];
                    $timestamp = time();

                    if (!file_exists($ts_file))
                        touch($ts_file);

                    // Vacuum 1x par semaine
                    if ((int)file_get_contents($ts_file) < $timestamp - 7*24*60*60) {
                        $this->conn->exec("VACUUM");
                        file_put_contents($ts_file, $timestamp, LOCK_EX);
                    }
                }
            } catch (PDOException $exception) {
                echo "Error: " . $exception->getMessage();
            }
        }
    }

    // get the database connection
    public function getConnection() {

        $this->conn = null;

        try {
            if ($this->db_type == "mysql") {
                $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->conn->exec("set names utf8");
            } else if ($this->db_type == "sqlite3") {
                $db_file_exists = file_exists($this->sqlite3_db);
                $this->conn = new PDO("sqlite:$this->sqlite3_db");
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // Ceci va accelerer les insertions (et donc éviter que la base soit trop 
                //  longtemps inaccessible pour des select lorsqu'on fait des insert en masse
                if (SQLite3::version()['versionNumber'] >= 3007000) {
                    // Besoin SQLite version 3.7.0 (2010-07-21) or later pour utiliser WAL
                    $this->conn->exec("PRAGMA journal_mode=WAL"); 
                } else {
                    $this->conn->exec("PRAGMA journal_mode=MEMORY");
                }

                // Créer la table si elle n'existe pas
                if (!$db_file_exists) {
                    // Rem: si on souhaite vérifier si la table existe:
                    // https://stackoverflow.com/a/1604121
                    
                    $query = "CREATE TABLE IF NOT EXISTS seances(therapeute TEXT, jour TEXT, heure TEXT, status NUMERIC)";
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute();
                }
            }

        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
            die;
        }

        return $this->conn;
    }
}
?>
