<?php
/**
 * Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
 * 
 * Ce fichier fait partie du logiciel KineTools Suite.
 * 
 * Ce logiciel est un programme informatique servant à extraire des données
 * de logiciels métiers pour kinésithérapeutes dans le but de faciliter
 * certaines tâches de gestion. 
 * 
 * Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
 * termes.
 */

function build_encrypted_message($headers, $msg_plaintext) {
    //Construit un message crypté gnuGP
    $boundary = md5(uniqid(mt_rand()));

    $headers .= 'MIME-Version: 1.0'."\n";
    $headers .= 'Content-Type: multipart/encrypted; boundary="'.$boundary.'"; protocol="application/pgp-encrypted"'; 
    
    $body = "\n";
    $body .= '--'.$boundary."\n";
    $body .= 'Content-Type: application/pgp-encrypted'."\n";
    $body .= 'Content-Disposition: attachment'."\n";
    $body .= 'Content-Transfer-Encoding: 7Bit'."\n\n";
    $body .= "Version: 1\n";
    
    $body .= "\n";
    $body .= '--'.$boundary."\n";
    $body .= 'Content-Type: application/octet-stream'."\n";
    $body .= 'Content-Disposition: inline; filename="msg.asc"'."\n";
    $body .= 'Content-Transfer-Encoding: 7Bit'."\n\n";
    
    // Ici encrypter le contenu du message:
    
    // La partie qui est cryptée doit contenir les deux headers ci-dessous et le corps du message
    // Pour que l'affichage en UTF-8 se fasse bien.
    $gpg = new myGnuPG();
    
    $message_text_header = 'Content-Type: text/plain; charset="utf-8"'."\n";
    $message_text_header .= 'Content-Transfer-Encoding: 8bit'."\n\n"; 
    //echo htmlspecialchars($message_text_header.$msg_plaintext);
    try {
        $body .= $gpg->encrypt($message_text_header.$msg_plaintext);
    } catch (Exception $e) {
        echo '<br>Le serveur a rencontré une erreur lors du chiffrement de votre demande.';
        echo '<br>Celle-ci ne peut aboutir. Si cela persiste, veuillez ';
        echo 'contacter le cabinet en faisant référence au message suivant :<br>';
        echo '<strong>'.$e->getMessage().'</strong>';
        //echo '<pre>';
        //var_dump($e);
        //echo '</pre>';
        die;
    }
    
    $body .= "\n";
    $body .= '--'.$boundary."--\n";
    
    return array( 'headers' => $headers, 'body' => $body);
}

function build_plaintext_message($headers, $msg_plaintext) {
    $headers .='Content-Type: text/plain; charset="utf-8"'."\n";
    $headers .='Content-Transfer-Encoding: 8bit'; 
    //Construit un message texte simple
    $body = $msg_plaintext;
    return array( 'headers' => $headers, 'body' => $body);
}

function build_url_notif_par_SMS($tel, $notif_messages) {
    // Rajouter une URL au bas du mail pour confirmer par SMS
    $configs = include($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

    $secret = $configs['secret'];
    $algo = $configs['algo'];
    $intranet_base_url = $configs['intranet_base_url'];
    
    $timestamp = microtime(true);
    if (version_compare(phpversion(), '7.0.0', ">=")) {
        $nonce = random_bytes(32); // nécessite php 7
    } else {
        trigger_error("TODO: php est <php7. Fix Nonce !",E_USER_WARNING);
        $nonce = rand();
    }
    
    $contenu = '';
    
    foreach ($notif_messages as $key => $value) {
        $sms_message = array (
            "num" => $tel,
            "message" => $value[1], // La colonne 1 contient le message pour les SMS
            );

        /*
        $sms_message = array (
            "num" => "",
            "message" => ""
        );
        */

        $hash = hash_hmac($algo, json_encode($sms_message).'+'.$timestamp.'+'.$nonce, $secret);
        
        $query = array(
            'message' => urlencode(base64_encode(json_encode($sms_message))),
            'hash' => $hash,
            'nonce' => $nonce,
            'ts' => $timestamp
        );
        $url_q = http_build_query($query);
        $url_sms = $intranet_base_url."/api/sms/smsd-inject.php?".$url_q;
        
        switch ($key) {
            case "confirmation":
                $contenu .= '⋅ Confirmer par SMS: '."$url_sms\n";
                break;
            case "annulation":
                $contenu .= '⋅ Annuler par SMS: '."$url_sms\n";
                break;
        }
    }
    return ($contenu);
}

function build_url_notif_par_mail($mail_patient, $notif_messages) {
    // Ajoute la notification par email.

    $contenu = '';

    foreach ($notif_messages as $key => $value) {
        // Paramètres pour mailto: http://www.faqs.org/rfcs/rfc2368.html
        // Concernant les paramètres from & preselectid ils ne sont pas supportés dans TB.
        // https://bugzilla.mozilla.org/show_bug.cgi?id=1613342 (demande d'évolution)
        // http://kb.mozillazine.org/Command_line_arguments_%28Thunderbird%29
        $query = array(
            'to' => $mail_patient,
            'subject' => $value[0],
            'body' => $value[2],
            //'preselectid' => 'id1' // Paramètre pour sélectionner l'identité de Thunderbird < 52
            //'from' => 'user@domain.tld' // Paramètre pour sélectionner l'identité de Thunderbird > 52+
        );
        
        $url_mailto = 'mailto:?'.http_build_query($query, null, ini_get('arg_separator.output'), PHP_QUERY_RFC3986);
        
        switch ($key) {
            case "confirmation":
                $contenu .= '⋅ Confirmer par e-mail: '."$url_mailto\n";
                break;
            case "annulation":
                $contenu .= '⋅ Annuler par e-mail: '."$url_mailto\n";
                break;
        }
    }
    return ($contenu);
}
?>
