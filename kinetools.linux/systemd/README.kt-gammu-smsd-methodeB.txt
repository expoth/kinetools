MethodeB = toutesMinutesOneShot

Si
    `service kt-gammu-smsd start` (manuel ou par timer),
alors
    arrête gammu-smsd
    arrête le timer de démarrage de gammu-smsd
    lance le timer d'arret de gammu-smsd

si
    `service kt-gammu-smsd-stopper start` (manuel ou par timer),
alors
    arrête gammu-smsd
    arrête le timer d'arret de gammu-smsd
    lance le timer de démarrage de gammu-smsd

Si
    `service gammu-smsd start`,
alors
    démarre gammu-smsd
    ne change rien aux timers

si
    `service gammu-smsd stop`
alors
    arrête gammu-smsd
    ne change rien aux timers

Si on est en journée (entre 7h-20h)
    le timer de démarrage de gammu-smsd est activé toutes les minutes tant qu'il n'a pas fait
    "service kt-gammu-smsd start", (ce qui démarre gammu-smsd [par la directive Wants])
    et s'arrête dès que c'est fait (par la directive Conflicts de kt-gammu-smsd.service)
    il démarre le timer 'kt-gammu-smsd-stopper.timer' de la nuit (par la directive Wants)

Si on est en nuit (20:02-23:59 / 0:00-6:58)
    le timer d'arret de gammu-smsd est activé toutes les minutes tant qu'il n'a pas fait
    "service kt-gammu-smsd-stopper start", (ce qui arrête gammu-smsd [par la directive Conflicts])
    et s'arrête dès que c'est fait (par la directive Conflicts de kt-gammu-smsd-stopper.service)
    il démarre le timer 'kt-gammu-smsd.timer' de la journée (par la directive Wants)


ATTENTION:
----------
Si à un moment ou à une autre gammu-smsd plante en journée, il est possible qu'il ne soit pas redémarré tout seul
Pour qu'il redémarre tout seul, on va créer un fichier drop-in avec ces lignes dans /lib/systemd/system/gammu-smsd.service.d/
(on pourrait aussi ajouter dans "gammu-smsd.service" ces deux directives)
On met: on-failure pour qu'avec un "kill -KILL" ca redémarre, mais pas "kill -TERM"

[Service]
RestartSec=60
Restart=on-failure



Pour que les timers soient lancés au démarrage:
-----------------------------------------------
systemctl enable kt-gammu-smsd-stopper.timer
systemctl enable kt-gammu-smsd.timer


Pour relancer les timers:
-------------------------
systemctl daemon-reload
systemctl restart kt-gammu-smsd-stopper.timer
systemctl restart kt-gammu-smsd.timer


Pour vérifier l'état des timers:
--------------------------------
systemctl list-timers --all
