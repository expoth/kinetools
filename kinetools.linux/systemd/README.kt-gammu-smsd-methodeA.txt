MethodeA = toutes5minTimerEnPermanence

 si
    `service kt-gammu-smsd-stopper start` (manuel ou par timer),
alors
    arrête gammu-smsd

Si
    `service gammu-smsd start`,
alors
    démarre gammu-smsd
    ne change rien aux timers

si
    `service gammu-smsd stop`
alors
    arrête gammu-smsd
    ne change rien aux timers

Si on est en journée (entre 7h-20h)
    le timer de démarrage de gammu-smsd est activé toutes les 5 minutes tant qu'il n'a pas fait
    "service gammu-smsd start",
    il continue de tourner toutes les 5 minutes toute la journée pour lancer gammu-smsd s'il ne tourne plus

Si on est en nuit (20:02-23:59 / 0:00-6:58)
    le timer d'arret de gammu-smsd est activé toutes les 5 minutes tant qu'il n'a pas fait
    "service kt-gammu-smsd-stopper start",
    il continue de tourner toutes les 5 minutes toute la nuit pour arrêter gammu-smsd s'il a démarré


ATTENTION:
----------
Si à un moment ou à une autre gammu-smsd plante en journée, il est possible qu'il ne soit pas redémarré tout seul
Pour qu'il redémarre tout seul, on peut ajouter dans "gammu-smsd.service" ces deux directives.
On met: on-failure pour qu'avec un "kill -KILL" ca redémarre, mais pas "kill -TERM"

RestartSec=5
Restart=on-failure


Pour que les timers soient lancés au démarrage:
-----------------------------------------------
systemctl enable kt-gammu-smsd-stopper.timer
systemctl enable kt-gammu-smsd.timer


Pour relancer les timers:
-------------------------
systemctl daemon-reload
systemctl restart kt-gammu-smsd-stopper.timer
systemctl restart kt-gammu-smsd.timer


Pour vérifier l'état des timers:
--------------------------------
systemctl list-timers --all
