#!/bin/bash
# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

FTP_INCOMING_DIR=/var/ftp/incoming/
# Les scripts à lancer sont dans le répertoire où est ce fichier
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Mettre à 0 si on a besoin de débugguer
DELETE_INCOMING_FILES=1
DELETE_TMP_FILES=1

function remove_extension_from_file {
  local extension BASENAME input
  input="$1"
  extension="$(echo "$input" | awk -F. '{print $NF}')"
  BASENAME=$(basename "${input%.${extension}}")
  echo "${BASENAME}"
  #echo ${input} | sed "s/.${extension}//gI";
}


function convert_certutil_to_md5sum() {
    local orig_file
    orig_file="$1"
  
    # Convertir le fichier Certutil en dos2unix pour que la creation du fichier .md5sum ne buggue pas
    dos2unix -q -n "$orig_file.certutil.md5" "$orig_file.certutil.md5.2unix" &&

    # Créer un fichier .md5sum (sur la base du hash présent le fichier Certutil)
    # contre lequel on pourra lancer md5sum -c
    echo "$(sed -n '2p' "$orig_file.certutil.md5.2unix")  $orig_file" > "$orig_file.md5sum"
    rm "$orig_file.certutil.md5.2unix"
}

function delete_files_from_dir() {
    local dir
    dir="$1"
    if [ "a$dir" = "a" ]; then
        delete_file "$file"
        delete_file "$FILENAME_ORIG"
        delete_file "$FILENAME_MD5"    
    else
        delete_file "$dir/$file"
        delete_file "$dir/$FILENAME_ORIG"
        delete_file "$dir/$FILENAME_MD5"    
    fi
}

function delete_file() {
    if [ -f "$1" ]; then
        rm "$1"
    fi
}

function update_clock() {
    /usr/bin/sntp -S time.windows.com && hwclock -w
}

inotifywait -m -e close_write "$FTP_INCOMING_DIR" |
    while read -r path action file; do
        case "$file" in
            "rdv_du_jour_pour_alerte_sms.csv.certutil.md5" | \
            "rdv_du_lendemain_pour_alerte_sms.csv.certutil.md5" | \
            "agenda.csv.certutil.md5" | \
            "intranet_config.tar.xz.certutil.md5" | \
            "intranet_base.tar.xz.certutil.md5" | \
            kinetools-intranet_*_all.deb | \
            kalkun-devel_*_all.deb | \
            kalkun_*_all.deb | \
            kinetools-linux-updater_*_all.deb)

                mkdir -p /tmp/incoming_monitor
                if [ "$file" == "rdv_du_jour_pour_alerte_sms.csv.certutil.md5" ]; then
                    update_clock
                fi
                if [ "$file" == "rdv_du_lendemain_pour_alerte_sms.csv.certutil.md5" ]; then
                    update_clock
                fi

                echo "[$(date "+%F T %T %z")] Fichier $file détecté ayant un script correspondant."

                cp -a "$path/$file" /tmp/incoming_monitor &&
                if [[ $file = *_all.deb ]]; then
                    # cas d'un paquet debian. On va l'installer

                    # Suppression des fichiers temporaires dans FTP_INCOMING_DIR
                    if [ $DELETE_INCOMING_FILES = 1 ]; then
                        delete_files_from_dir "$FTP_INCOMING_DIR"
                    fi

                    "$SCRIPTS_DIR/on_new_deb.sh" "/tmp/incoming_monitor/" "$file"
                else
                    # Cas général où on a recu un fichier avec sa somme de contrôle MD5
                    # On vérifie que le fichier et sa somme de controle sont cohérents
                    # pour continuer, sinon on arrête
                    FILENAME_ORIG=$(remove_extension_from_file "$file")
                    FILENAME_ORIG=$(remove_extension_from_file "$FILENAME_ORIG")
                    FILENAME_MD5="$FILENAME_ORIG.md5sum"

                    if [ -e "$path/$FILENAME_ORIG" ]; then
                        cp -a  "$path/$FILENAME_ORIG" /tmp/incoming_monitor
                    else
                        echo -e "   ERREUR: fichier manquant: $path/$FILENAME_ORIG. Fin de taitement.\\n"
                        delete_files_from_dir "$FTP_INCOMING_DIR"
                        continue
                    fi

                    # Suppression des fichiers temporaires dans FTP_INCOMING_DIR
                    if [ $DELETE_INCOMING_FILES = 1 ]; then
                        delete_files_from_dir "$FTP_INCOMING_DIR"
                    fi

                    cd /tmp/incoming_monitor &&

                    # Convertir le fichier Certutil en dos2unix pour que la creation du
                    # fichier .md5sum ne buggue pas puis en output equivalent à celui de md5sum
                    convert_certutil_to_md5sum "$FILENAME_ORIG"

                    md5sum --quiet -c "$FILENAME_MD5"
                    CHECKSUM_RESULT=$?

                    # Ici est le vrai traitement du fichier reçu.
                    if [ $CHECKSUM_RESULT = 0 ]; then
                        # Vérification OK, le fichier n'est pas corrompu
                        "$SCRIPTS_DIR/on_new_${FILENAME_ORIG}.sh" "/tmp/incoming_monitor/" "$FILENAME_ORIG"
                    else
                        echo "   ERREUR: fichier $FILENAME_ORIG et Hash ne correspondent pas. Abandon." #| tee -a /var/log/inotify_script.log
                    fi
                fi

                # Suppression des fichiers temporaires dans /tmp/incoming_monitor
                if [ $DELETE_TMP_FILES = 1 ]; then
                    delete_files_from_dir /tmp/incoming_monitor
                fi
                ;;

            *)
                echo -e "[$(date "+%F T %T %z")] Fichier '$file' détecté mais pas de traitement correspondant (appeared in directory '$path' via '$action')"
            ;;
        esac
        echo ""
    done
