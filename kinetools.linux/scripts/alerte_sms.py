#!/usr/bin/python3
# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

#
# Usage : alerte_sms.py "fichier.csv" "dry-run"
# 

import csv
import os
import sys
import configparser
import shlex
import datetime
import re
import sqlite3

#######################
# Fonctions:
#######################
def msg_template_for_th(key, type_msg, config):
  if unescape_ini_value(config[key]['msg_'+type_msg]):
    return unescape_ini_value(config[key]['msg_'+type_msg])
  else:
    return unescape_ini_value(config['msg'][type_msg])
  return

def unescape_ini_value(val):
  # D'autres méthodes sont ici : https://stackoverflow.com/questions/1885181/how-to-un-escape-a-backslash-escaped-string
  # Méthode1 : 
  #     ma version initiale. Mais ne remplace pas les \' s'il y en a.
  #     A utiliser si dans l'export INI dans php on met addcslashes($sval,'"')
  # Méthode2 :
  #     avec ast.literal_eval -> https://stackoverflow.com/a/1885211. 
  #     Pose parfois des des pb si la valeur du ini n'set pas entre guillemets alors que c'est du texte
  # Méthode3 : 
  #     avec encode/decode -> https://stackoverflow.com/a/57192592. La mieux ?
  #     Marche aussi bien avec  addcslashes($sval,'"') que addslashes($sval) (cad si " et ' sont tous deux escaped)

  #print ("eval" + val + "\ntype" + repr(val))
  
  method_to_use = 1
  if (method_to_use == 1):
    return val.strip('"\'').replace('\\"','"')
  elif (method_to_use == 2):
    import ast
    try:
      new_val = ast.literal_eval(val)
    except ValueError:
      print ("erreur dans literal_eval. La valeur n'est pas un literal. Val = " + val)
      new_val = val
    return new_val
  elif (method_to_use == 3):
    from codecs import encode, decode
    if isinstance(val, str):
      return decode(encode(val.strip('"\''), 'latin-1', 'backslashreplace'), 'unicode-escape')
    else:
      return val

def tel_en_blacklist(rows, tel):
    for row in rows:
        if row[0] == tel:
            return True
    return False

def ajouter_indicatif_pays(tel):
    #print ("Tel Before:"+tel)
    tel = re.sub(r'^0([67])([0-9]*)$', r'+33\1\2', tel)
    #print ("Tel After:"+tel)
    return tel




def open_db_envois_faits():
    conn_sqlite3 = sqlite3.connect('/var/opt/kinetools/lib/alerte_sms.sqlite3.db')
    conn_sqlite3.execute('''CREATE TABLE IF NOT EXISTS envois_faits(type_message char(20), tel CHAR(30), date CHAR(10), heure CHAR(5));''');
    return conn_sqlite3

def close_db_envois_faits(conn_sqlite3):
    conn_sqlite3.close()

def nb_envois_faits(conn_sqlite3, type_message, tel, date, heure):
    cur = conn_sqlite3.cursor()
    cur.execute('''select count(*) 
        FROM envois_faits 
        WHERE type_message = ?
        AND tel = ?
        AND date = ?
        AND heure = ?''', (type_message, tel, date, heure))
    row = cur.fetchone()
    return row[0]

def insert_table_envois_faits(conn_sqlite3, type_message, tel, date, heure):
    cur = conn_sqlite3.cursor()
    cur.execute('INSERT INTO envois_faits (type_message, tel, date, heure) VALUES (?, ?, ?, ?)',
                (type_message, tel, date, heure))
    #conn_sqlite3.commit()

def commit_and_cleanup_db_envois_faits(conn_sqlite3):
    cur = conn_sqlite3.cursor()
    if envoyer_meme_si_rdv_dans_le_passe != "1":
        #print ('delete all before ' + datetime.date.today().strftime('%Y-%m-%d'))    
        cur.execute('DELETE FROM envois_faits WHERE date < ?', (datetime.date.today().strftime('%Y-%m-%d'),))
    #print ('vacuum')
    conn_sqlite3.commit()
    cur.execute('VACUUM;')
    #conn_sqlite3.commit()




def open_db_timing_rappel():
    conn_sqlite3 = sqlite3.connect('/var/opt/kinetools/lib/www-data/timing_rappel.sqlite3.db')
    conn_sqlite3.execute('''CREATE TABLE IF NOT EXISTS timing_rappel(
    tel TEXT NOT NULL PRIMARY KEY,
    minutes INTEGER);''');
    return conn_sqlite3

def duree_timing_rappel(conn_sqlite3, tel):
    cur = conn_sqlite3.cursor()
    cur.execute('''select minutes
        FROM timing_rappel
        WHERE tel = ?''', (tel,))
    row = cur.fetchone()
    if row == None:
        return None
    else:
        return row[0]

def close_db_timing_rappel(conn_sqlite3):
    conn_sqlite3.close()

#######################
# Programme
#######################
print (" * Début du script alerte_sms.py")

silent = True

if (len(sys.argv) > 1 ):
  csv_filename=sys.argv[1]
  if (not os.path.isfile(csv_filename)):
    print (csv_filename + " n'est pas un fichier / n'existe pas. Aborting...")
    exit()
else:
  print ("Il faut renseigner le nom du fichier CSV en paramètre. Aborting...")
  exit()

dry_run = False
if (len(sys.argv) > 2 ):
  if (sys.argv[2] == "dry-run"):
    dry_run = True

# Read ini "config.ini"
print (" * Lecture du fichier de configuration")
config_base = configparser.ConfigParser(allow_no_value=True)
config_base.read(os.path.dirname(os.path.realpath(__file__)) + '/config.ini')
config_file_path = unescape_ini_value(config_base['alerte_sms']['config_file_path'])
envoyer_meme_si_rdv_dans_le_passe = unescape_ini_value(config_base['alerte_sms']['envoyer_meme_si_rdv_dans_le_passe'])
extranet_base_url = unescape_ini_value(config_base['alerte_sms']['extranet_base_url'])
kalkun_has_stop_enabled = unescape_ini_value(config_base['alerte_sms']['kalkun_has_stop_enabled'])
if config_base.has_option('alerte_sms', 'sync_time'):
  sync_time = unescape_ini_value(config_base['alerte_sms']['sync_time'])
else:
  sync_time = "1"

# Read ini "alertes_sms.ini"
config = configparser.ConfigParser(allow_no_value=True)
config.read(config_file_path + '/alertes_sms.ini')

if not dry_run and sync_time == "1" :
  os.system("/usr/bin/sntp -S time.windows.com && hwclock -w")

ths_envoi_actif = []
ths_cle_code = {}
types_msg = []
for key in config:
  if key.startswith("th_"):
    ths_cle_code[unescape_ini_value(config[key]['code_th'])] = key
    if unescape_ini_value(config[key]['activer_envoi']) == "1":
      ths_envoi_actif.append(unescape_ini_value(config[key]['code_th']))
      types_msg.append(unescape_ini_value(config[key]['type_message']))

#print (ths_cle_code)
print ("    Thérapeutes pour lesquels l'envoi est activé: ")
print (ths_envoi_actif)
types_msg = set(types_msg)
print (types_msg)
#print(unescape_ini_value(config["msg"]['rappel']))
#exit()

# Read Database
if kalkun_has_stop_enabled == "1" :
    # Get database connection info from kalkun
    from subprocess import check_output
    import json
    import psycopg2
    kalkun_conf = check_output(['php', '-r', 'define("BASEPATH",""); include "/var/www/kalkun/application/config/database.php"; echo json_encode($db);'])
    kalkun_conf = json.loads(kalkun_conf)
    #kalkun_conf['default']['hostname']
    #kalkun_conf['default']['database']
    #kalkun_conf['default']['username']
    #kalkun_conf['default']['password']
    conn = psycopg2.connect(host=kalkun_conf['default']['hostname'], 
                            database=kalkun_conf['default']['database'],
                            user=kalkun_conf['default']['username'],
                            password=kalkun_conf['default']['password'])
    cur = conn.cursor()

    blacklist_rows = {}
    for type_message in types_msg:
        sql = "select distinct(destination_number) from plugin_stop_manager where stop_type ilike %s"
        cur.execute(sql, (type_message,))
        blacklist_rows[type_message] = cur.fetchall()
        
    #for row in blacklist_rows[type_message]:
    #    print(row)

    cur.close()
    conn.close()

conn_sqlite3 = open_db_envois_faits()
conn_sqlite3_timing = open_db_timing_rappel()

# Read CSV containing infos for SMS
print (" * Lecture du CSV")

with open(csv_filename, 'r') as csv_file:
  reader = csv.reader(csv_file, delimiter='\t', quotechar='"', doublequote=True, lineterminator='\n', strict=True)
  for row in reader:
    therapeute = row[15]
    #print (therapeute)

    # Si le thérapeute pour cette ligne a l'envoi activé dans le ini
    if therapeute in ths_envoi_actif:
      tel = ajouter_indicatif_pays(row[9])
      date = row[10]
      heure = row[11]
      id_proch_rdv = row[1]

      # Type message: "rappel", "annul"
      section = ths_cle_code[therapeute]
      type_message = unescape_ini_value(config[section]['type_message'])

      # Vérification des conditions requises pour un envoi.
      if not tel:
          print ("N° de tel non renseigné")
          continue

      if kalkun_has_stop_enabled == "1" and tel_en_blacklist(blacklist_rows[type_message], tel):
          print ("N° de tel dans la base STOP")
          continue

      nb_max_envois_par_rdv_typemsg = int(unescape_ini_value(config[section]['nb_max_envois_par_rdv_typemsg']))
      if nb_envois_faits(conn_sqlite3, type_message, tel, date, heure) >= nb_max_envois_par_rdv_typemsg:
          print ("nb envois >= nb_max_envois_par_rdv_typemsg: "+ str(nb_envois_faits(conn_sqlite3, type_message, tel, date, heure)) + " >= " + str(nb_max_envois_par_rdv_typemsg))
          continue

      dt_now = datetime.datetime.now()
      dt_rdv = datetime.datetime.fromisoformat(date + " " + heure);

      timing = duree_timing_rappel(conn_sqlite3_timing, tel)
      if (timing != None) and (type_message == "rappel"):
        # On enleve 3 minutes de facon arbitraire (temps que gammu-smsd réagisse, temps de lancement du cron kinetools-linux)
        dt_rappel = datetime.datetime.fromisoformat(date + " " + heure) - datetime.timedelta(minutes = int(timing) + 3);

        if (dt_now < dt_rappel):
          print ("Rappel demandé à partir de "+ dt_rappel.strftime('%Y-%m-%d %H:%M') + " il est " + dt_now.strftime('%Y-%m-%d %H:%M'))
          continue

      if envoyer_meme_si_rdv_dans_le_passe == "0" and dt_rdv <= dt_now:
          print ("Condition d'horodatage non remplie. Rdv dans le passé ("+dt_rdv.strftime('%Y-%m-%d %H:%M')+')')
          continue

      message = msg_template_for_th(section, type_message, config)

      message = message.replace("@DATE@", date)
      message = message.replace("@HEURE@", heure)
      message = message.replace("@EXRANET_BASE_URL@", extranet_base_url)
      message = message.replace("@ID_PROCH_RDV@", id_proch_rdv)
      message = message.replace("@TH@",'TODO')

      if not message:
          print ("message vide")
          continue

      # Procéder à l'envoi (appelle "gammu-smsd-inject" & insert en BDD)
      command = "/usr/bin/gammu-smsd-inject TEXT " + shlex.quote(tel) + " -autolen 2000 -text " + shlex.quote(message)
      if (dry_run):
          #1==1
          print(command)
      else:
          print(command)
          insert_table_envois_faits(conn_sqlite3, type_message, tel, date, heure)
          shell_return = os.system(command)
          print(shell_return)

    # Si le thérapeute n'a pas l'envoi activé
    else:
      # Ne rien faire
      if not silent:
        print ("Ligne ignorée");


commit_and_cleanup_db_envois_faits(conn_sqlite3)

close_db_timing_rappel(conn_sqlite3_timing)
close_db_envois_faits(conn_sqlite3)

print (" * Fin du script alerte_sms.py")
