#!/bin/bash
# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

TEST=false
FILEPATH="$1"
FILENAME="$2"
FILE="$1$2"
EXTRANET_DOCUMENT_ROOT=/var/www/kinetools-intranet
DOCUMENT_ROOT=$EXTRANET_DOCUMENT_ROOT
#OPTS="$3"

echo -e "  Décompresse le fichier intranet_base.tar.xz dans l'arborescence web Intranet"
echo -e "   ⋅ Début de traitement: $FILE"
if [ a$TEST = "atrue" ]; then
    # Liste les commandes qui seront executées.
    # TODO
	echo ""
else
    #
    # CECI execute la commande !!

    # D'abord fait un backup
    TODAY=$(date +%F)
    if [ ! -e /var/www/kinetools-intranet_$TODAY.tar.xz ]; then 
        tar -C /var/www/ -cvJf kinetools-intranet_$TODAY.tar.xz intranet
    fi

    # Puis décompresse
    tar -xvJf "$FILE" -C $DOCUMENT_ROOT --strip-components=1 --no-same-permissions --no-same-owner
    #tar -xvJf intranet_config.tar.xz -C $DOCUMENT_ROOT --strip-components=1 --no-same-permissions --no-same-owner
    
    # Puis enleve les permissions executable
    find $DOCUMENT_ROOT -type f -exec chmod a-x {} +

fi

echo -e "   ⋅ Fin de traitement: $FILE"
