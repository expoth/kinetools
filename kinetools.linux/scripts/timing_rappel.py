#!/usr/bin/python3
#
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
#
# Ce fichier fait partie du logiciel KineTools Suite.
#
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion.
#
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site "http://www.cecill.info".
#
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
#
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
#
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
#

#
# Usage : timing_rappel.py "numéro de téléphone" "timing en minutes"
#

import os
import sys
import configparser
import shlex
import datetime
import re
import sqlite3

#######################
# Fonctions:
#######################
def open_db_timing_rappel():
    conn_sqlite3 = sqlite3.connect('/var/opt/kinetools/lib/www-data/timing_rappel.sqlite3.db')
    conn_sqlite3.execute('''CREATE TABLE IF NOT EXISTS timing_rappel(
    tel TEXT NOT NULL PRIMARY KEY,
    minutes INTEGER);''');
    return conn_sqlite3

def close_db_timing_rappel(conn_sqlite3):
    conn_sqlite3.close()

def insert_table_timing_rappel(conn_sqlite3, tel, minutes):
    cur = conn_sqlite3.cursor()
    cur.execute('INSERT OR IGNORE INTO timing_rappel (tel, minutes) VALUES (?, ?);',
                (tel, minutes))
    cur.execute('UPDATE timing_rappel SET minutes = ? WHERE tel = ?;',
                (minutes, tel))
    #conn_sqlite3.commit()

def commit_and_cleanup_db_envois_faits(conn_sqlite3):
    cur = conn_sqlite3.cursor()
    #print ('vacuum')
    conn_sqlite3.commit()
    cur.execute('VACUUM;')
    #conn_sqlite3.commit()

#######################
# Programme
#######################
print (" * Début du script timing_rappel.py")

silent = True
print (sys.argv)
if (len(sys.argv) == 3):
  tel=sys.argv[1]
  minutes=int(sys.argv[2])
  print (tel)
  print (minutes)
else:
  print ("Erreur de paramètres. Il faut renseigner le numéro de tél, et les minutes. Aborting...")
  exit()

# Read ini "config.ini"
print (" * Lecture du fichier de configuration")


conn_sqlite3_timing = open_db_timing_rappel()

insert_table_timing_rappel(conn_sqlite3_timing, tel, minutes)
commit_and_cleanup_db_envois_faits(conn_sqlite3_timing)
close_db_timing_rappel(conn_sqlite3_timing)

print (" * Fin du script timing_rappel.py")
