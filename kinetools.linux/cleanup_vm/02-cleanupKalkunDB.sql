--
-- Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
-- 
-- Ce fichier fait partie du logiciel KineTools Suite.
-- 
-- Ce logiciel est un programme informatique servant à extraire des données
-- de logiciels métiers pour kinésithérapeutes dans le but de faciliter
-- certaines tâches de gestion. 
-- 
-- Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
-- respectant les principes de diffusion des logiciels libres. Vous pouvez
-- utiliser, modifier et/ou redistribuer ce programme sous les conditions
-- de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
-- sur le site "http://www.cecill.info".
-- 
-- En contrepartie de l'accessibilité au code source et des droits de copie,
-- de modification et de redistribution accordés par cette licence, il n'est
-- offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
-- seule une responsabilité restreinte pèse sur l'auteur du programme,  le
-- titulaire des droits patrimoniaux et les concédants successifs.
-- 
-- A cet égard  l'attention de l'utilisateur est attirée sur les risques
-- associés au chargement,  à l'utilisation,  à la modification et/ou au
-- développement et à la reproduction du logiciel par l'utilisateur étant 
-- donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
-- manipuler et qui le réserve donc à des développeurs et des professionnels
-- avertis possédant  des  connaissances  informatiques approfondies.  Les
-- utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
-- logiciel à leurs besoins dans des conditions permettant d'assurer la
-- sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
-- à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
-- 
-- Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
-- pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
-- termes.
--

-- Pour nettoyer les BDD + Reinitialiser les compteurs dans PostgreSQL
-- TRUNCATE TABLE someTable RESTART IDENTITY;
-- sinon voir ici: https://stackoverflow.com/questions/5342440/reset-auto-increment-counter-in-postgres


-- Tables Crées par Gammu pouvant être vidées
--   inbox
--   outbox
--   outbox_multipart 
--   phones
--   sentitems
-- Tables anciennement crées par gammu mais ayant du être rajoutées manuellement car plus crées
--   pbk
--   pbk_groups

-- Tables ci_session pour gammu avec ci3
--   ci_sessions

-- Tables pour le plugin stop_manager que j'ai créé
--   plugin stop_manager
--   plugin_stop_manager

TRUNCATE TABLE inbox RESTART IDENTITY;
TRUNCATE TABLE outbox RESTART IDENTITY;
TRUNCATE TABLE outbox_multipart RESTART IDENTITY;
TRUNCATE TABLE phones RESTART IDENTITY;
TRUNCATE TABLE sentitems RESTART IDENTITY;
TRUNCATE TABLE pbk RESTART IDENTITY;
TRUNCATE TABLE pbk_groups RESTART IDENTITY;
TRUNCATE TABLE ci_sessions RESTART IDENTITY;
TRUNCATE TABLE plugin_stop_manager RESTART IDENTITY;


-- Tables crées par kalkun
TRUNCATE TABLE user_outbox  RESTART IDENTITY;
TRUNCATE TABLE user_inbox  RESTART IDENTITY;
TRUNCATE TABLE user_sentitems  RESTART IDENTITY;
TRUNCATE TABLE sms_used  RESTART IDENTITY;
TRUNCATE TABLE user_group  RESTART IDENTITY;
TRUNCATE TABLE kalkun  RESTART IDENTITY;
TRUNCATE TABLE user_templates  RESTART IDENTITY;
TRUNCATE TABLE plugins  RESTART IDENTITY;
TRUNCATE TABLE user_forgot_password  RESTART IDENTITY;
TRUNCATE TABLE user_filters  RESTART IDENTITY;

-- Attention, les tables suivantes sont précomplétées à la création par kalkun
-- On leur remet leurs valeurs
--   b8_wordlist
--   user_folders
--   user
--   user_settings

TRUNCATE TABLE b8_wordlist RESTART IDENTITY;
TRUNCATE TABLE user_folders RESTART IDENTITY;
TRUNCATE TABLE "user" RESTART IDENTITY;
TRUNCATE TABLE user_settings RESTART IDENTITY;

-- Insertions Recopiées depuis /var/www/kalkun/media/db/pgsql_kalkun.sql (fourni par kalkun)
INSERT INTO "user" VALUES(1, 'kalkun', 'Kalkun SMS', 'f0af18413d1c9e0366d8d1273160f55d5efeddfe', '123456789', 'admin');
INSERT INTO "user_settings" VALUES (1, 'green', 'false;Put your signature here', 'false', 20, 'true;background.jpg', 'default', 'english', 'asc');
INSERT INTO "user_folders" VALUES(1, 'inbox', 0), (2, 'outbox', 0), (3, 'sent_items', 0), (4, 'draft', 0), (5, 'Trash', 0), (6, 'Spam', 0);
INSERT INTO "b8_wordlist" VALUES('bayes*dbversion', '2');
INSERT INTO "b8_wordlist" VALUES('bayes*texts.ham', '0');
INSERT INTO "b8_wordlist" VALUES('bayes*texts.spam', '0');

-- Pour info: celles-ci, créées par gammu, sont modifiées par kalkun (ajout de colonnes)
--   inbox
--   sentitems
--   pbk
--   pbk_groups


-- Eventuellement supprimer les tables créées par les plugins.
DROP TABLE IF EXISTS keys;
DROP TABLE IF EXISTS limits;
DROP TABLE IF EXISTS logs;
DROP TABLE IF EXISTS plugin_blacklist_number;
DROP TABLE IF EXISTS plugin_remote_access;
DROP TABLE IF EXISTS plugin_server_alert;
DROP TABLE IF EXISTS plugin_sms_credit;
DROP TABLE IF EXISTS plugin_sms_credit_template;
DROP TABLE IF EXISTS plugin_sms_member;
DROP TABLE IF EXISTS plugin_sms_to_email;
DROP TABLE IF EXISTS plugin_sms_to_twitter;
DROP TABLE IF EXISTS plugin_sms_to_wordpress;
DROP TABLE IF EXISTS plugin_sms_to_xmpp;
DROP TABLE IF EXISTS plugin_stop_manager;
DROP TABLE IF EXISTS plugin_whitelist_number;

-- Vacuum (Effectué par le script Bash qui appelle ce script SQL
-- Vacuum;
