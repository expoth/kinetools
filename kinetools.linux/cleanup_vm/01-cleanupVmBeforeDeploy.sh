#!/bin/bash
# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

if [ $UID -ne 0 ]; then
echo "To be run as root. Exiting..."
exit;
fi

function delete_file() {
    if [ -f "$1" ]; then
        echo "Supprime: $1"
        rm "$1"
    fi
}
if id vmuser >/dev/null 2>&1; then
    # Si l'utilisateur vmuser existe (c'est qu'on est dans la vm)
    ROOT_DIR=/
elif [ -e /mnt/home/vmuser ]; then
    # Sinon
    ROOT_DIR=/mnt/
elif [ -e /mnt/vdi/nbd0p1/home/vmuser ]; then
    # Sinon
    ROOT_DIR=/mnt/vdi/nbd0p1/
else
    ROOT_DIR=/mnt/vm
fi

full_path=$(realpath "$0")
dir_path=$(dirname "$full_path")

for udir in home/vmuser root; do
    delete_file ${ROOT_DIR}${udir}/.bash_history
    if [ -d "${ROOT_DIR}${udir}/.config" ]; then
        rm -r ${ROOT_DIR}${udir}/.config
    fi
    delete_file ${ROOT_DIR}${udir}/.lesshst
    if [ -d "${ROOT_DIR}${udir}/.ssh" ]; then
        rm -r ${ROOT_DIR}${udir}/.ssh
    fi
    if [ -d "${ROOT_DIR}${udir}/.vim" ]; then
        rm -r ${ROOT_DIR}${udir}/.vim
    fi
    delete_file ${ROOT_DIR}${udir}/.viminfo
done

# Crée les scripts pour supprimer le fichier .bash_history
# au moment de la déconnexion
if [ a${ROOT_DIR} = a/ ]; then
cat >> ~/.bash_logout << EOF
source ~/.bash_delete_history.sh
EOF
cat > ~/.bash_delete_history.sh << EOF
sed -i '/source ~\/.bash_delete_history.sh/d' ~/.bash_logout
if [ ! -s ~/.bash_logout ]; then
    rm ~/.bash_logout
fi
history -c
history -w
rm ~/.bash_history
rm ~/.bash_delete_history.sh
EOF
fi

# eventuellement aussi 
delete_file ${ROOT_DIR}home/vmuser/.gammurc

delete_file ${ROOT_DIR}etc/resolv.conf
echo "" > ${ROOT_DIR}etc/machine-id # Sera renseigné avec var/lib/dbus/machine-id au prochain démarrage
#echo "" > ${ROOT_DIR}var/lib/dbus/machine-id #https://www.freedesktop.org/software/systemd/man/machine-id.html
delete_file ${ROOT_DIR}var/lib/dbus/machine-id
dbus-uuidgen --ensure=${ROOT_DIR}var/lib/dbus/machine-id #Crée un nouveau /var/lib/dbus/machine-id s'il n'existe pas

if [ -d "${ROOT_DIR}tmp/incoming_monitor" ]; then
    rm -r "${ROOT_DIR}tmp/incoming_monitor"
fi
delete_file ${ROOT_DIR}data #un reliquat de fichier de données qui ne devrait pas être là !
rm -f ${ROOT_DIR}var/ftp/incoming/*
rm -f ${ROOT_DIR}var/lib/dhcp/*
rm -f ${ROOT_DIR}var/lib/systemd/coredump/*

# Il faut s'occuper du fichier /var/lib/systemd/random-seed
# cf https://www.freedesktop.org/software/systemd/man/systemd-random-seed.service.html
if [ a${ROOT_DIR} = a/ ]; then
    systemctl stop systemd-random-seed.service
fi
delete_file ${ROOT_DIR}var/lib/systemd/random-seed
if [ a${ROOT_DIR} = a/ ]; then
    systemctl start systemd-random-seed.service
fi

# Kalkun place les fichiers de session dans /var/lib/php/sessions/
rm -f ${ROOT_DIR}var/lib/php/sessions/*

# kinetools place des fichiers (csv alertes rdv, logs, timing rappel...) dans /var/opt/kinetools/*
rm -f ${ROOT_DIR}var/opt/kinetools/lib/*
rm -f ${ROOT_DIR}var/opt/kinetools/lib/www-data/*
touch ${ROOT_DIR}var/opt/kinetools/lib/www-data/timing_rappel.sqlite3.db
chown --reference=${ROOT_DIR}var/opt/kinetools/lib/www-data ${ROOT_DIR}var/opt/kinetools/lib/www-data/timing_rappel.sqlite3.db
rm -f ${ROOT_DIR}var/opt/kinetools/log/*
rm -f ${ROOT_DIR}var/opt/kinetools/spool/*

# TRAITEMENT DES FICHIER DANS /var/log
# https://www.thegeekstuff.com/2011/08/linux-var-log-files/
# Dans /var/log: on peut garder ou enlever:
# - /var/log/alternatives.log* # Information by the update-alternatives are logged into this log file. On Ubuntu, update-alternatives maintains symbolic links determining default commands.
# - /var/log/apt (dossier)
# - /var/log/aptitude.log*
# - /var/log/dpkg.log
# - /var/log/installer (mais si on garde il faut enlever au moins /var/log/installer/syslog)

cd ${ROOT_DIR}var/log/ &&

for file in apache2/*.log auth.log btmp daemon.log debug faillog kern.log lastlog messages postgresql/*.log syslog user.log vsftpd.log wtmp; do
    delete_file ${ROOT_DIR}var/log/$file.*
    truncate -s 0 ${ROOT_DIR}var/log/$file
done

delete_file ${ROOT_DIR}var/log/installer/syslog
for dir in private ntpstats sysstat; do
    if [ -n "$(ls -A ${ROOT_DIR}var/log/$dir 2>/dev/null)" ]; then
        rm -r ${ROOT_DIR}var/log/$dir/*
    fi
done

# NETTOYER LES BDD + REINITIALISER LES COMPTEURS
if [ a${ROOT_DIR} = a/ ]; then
    # Dans ce cas, le script est réellement lancé depuis la
    # la machine, virtuelle, on peut donc le lancer automatiquement
    echo ""
    echo ""
    echo "Nettoyer la base de donnée et réinitialiser les compteurs:"
    echo ""
    chmod +x "$dir_path/02-cleanupKalkunDB.sh"
    source "$dir_path/02-cleanupKalkunDB.sh"
else
    echo ""
    echo ""
    echo "Pour nettoyer la base de donnée et réinitialiser les compteurs,"
    echo "lancer depuis l'intérieur de la machine virtuelle"
    echo "  $dir_path/02-cleanupKalkunDB.sh"
    echo ""
fi

echo ""
echo "TODO: Si on a renseigné un mdp dans /usr/share/kalkun/scripts/cURL/example.php"
echo "TODO: -> L'enlever"
echo ""
echo "TODO: Enlever toutes infos privées dans /opt/kinetools"
echo "TODO: + surement d'autres choses"
cd ${ROOT_DIR}opt/kinetools/scripts &&
for file in *.in; do
    bn=$(basename "$file" .in)
    cp -a "$file" "$bn"
done

echo ""
echo "TODO: - Mettre à jour login/mdp dans kalkun (application/config.php application/database.php"
echo "   \$config['encryption_key']"
echo "   \$db['default']['password']..."
echo ""
echo "Supprime les logs de kalkun"
rm -f ${ROOT_DIR}var/www/kalkun-devel/application/logs/*
rm -f ${ROOT_DIR}var/www/kalkun/application/logs/*
rm -f ${ROOT_DIR}var/log/kalkun/*

echo ""
echo "TODO: - Enlever données de l'intranet (/var/www/kinetools-intranet/data .../config .../config_local)"
rm -f ${ROOT_DIR}var/www/kinetools-intranet/data/*
for dir in ${ROOT_DIR}var/www/kinetools-intranet/config ${ROOT_DIR}var/www/kinetools-intranet/config_local; do
    cd $dir &&
    for file in *.in; do
        bn=$(basename "$file" .in)
        cp -a "$file" "$bn"
    done
done

echo ""
# ${ROOT_DIR}var/lib/postgresql/11 contient le cluster de base de données
echo "Cluster de base de données Postgresql dans ${ROOT_DIR}var/lib/postgresql/11"
echo ""
echo ""
