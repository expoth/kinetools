#!/bin/bash
# 
# Copyright ou © Fab Stz <fabstz-it@yahoo.fr>, (2020)
# 
# Ce fichier fait partie du logiciel KineTools Suite.
# 
# Ce logiciel est un programme informatique servant à extraire des données
# de logiciels métiers pour kinésithérapeutes dans le but de faciliter
# certaines tâches de gestion. 
# 
# Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
# sur le site "http://www.cecill.info".
# 
# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.
# 
# A cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant 
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
# 
# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
# pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
# termes.
# 

if [ $UID -ne 0 ]; then
echo "To be run as root. Exiting..."
exit;
fi

DBNAME=smsd
USER=smsd

full_path=$(realpath $0)
dir_path=$(dirname $full_path)

echo " == Arrête le daemon gammu-smsd"
systemctl stop gammu-smsd

echo " == Nettoye la base de donnée '$DBNAME' et réinitialise les compteurs"
su -c "psql -d $DBNAME -f $dir_path/02-cleanupKalkunDB.sql" postgres

echo " == Fait un backup complet du cluster"
su -c 'pg_dumpall > /tmp/postgresql_dumpall.sql' postgres

echo " == Supprime le cluster puis le crée à nouveau"
pg_dropcluster 11 main --stop
pg_createcluster 11 main --start

echo " == Restaure le backup complet du cluster"
su -c 'psql -f /tmp/postgresql_dumpall.sql' postgres

su -c "echo 'vacuum;' | psql -d $DBNAME" postgres
su -c "echo 'vacuum;' | psql " postgres

echo " == Démarre le daemon gammu-smsd"
systemctl start gammu-smsd

rm /tmp/postgresql_dumpall.sql

echo " -- Fin du script $(basename "$0")"
